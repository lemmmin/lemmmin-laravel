<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\User;
use App\Event;
use App\Organizer;
use App\Ticket;
use Faker\Generator;


$factory->define(User::class, function (Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt('password'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Organizer::class, function (Generator $faker) {
    return [
        'name' => $faker->name,
        'about' => $faker->paragraph(),
        // 'owner_id' => factory(User::class)->create()->id,
    ];
});

$factory->define(Event::class, function (Generator $faker) {

    $event_title = $faker->sentence(6, true);
    $end_time    = $faker->dateTimeThisYear();
    $privacies   = ['public', 'private'];

    return [
        'title'       => $event_title,
        'slug'        => str_slug( $event_title ),
        'start_time'  => $faker->dateTimeThisYear( $end_time ),
        'end_time'    => $end_time,
        'owner_id'    => function() {
            return factory(User::class)->create()->id;
        },
        'organizer_id'=> function(array $event) {
            return factory(Organizer::class)->create([
                'owner_id' => $event['owner_id'],
            ])->id;
        },
        'summary'     => $faker->paragraph(3),
        'description' => $faker->text(),
        'privacy'     => $privacies[array_rand($privacies)]
    ];
});

$factory->define(Ticket::class, function (Generator $faker) {
    new Faker\Provider\DateTime($faker);

    return [
        'name' => $faker->name(),
        'description' => $faker->text(),
        'price' => $faker->randomFloat(),
        'currency' => $faker->randomElement(['BDT', 'USD', 'GBP', 'EUR']),
        'end_time' => $faker->dateTime(),
        'start_time' => function($ticket) use ($faker) {
            return $faker->dateTime($ticket['end_time']->modify('-1 sec'));
        },
        'quantity' => $faker->numberBetween(1, 10000),
        'issued' => function($ticket) use ($faker) {
            return $faker->numberBetween(1, $ticket['quantity']);
        },
        'privacy' => $faker->randomElement(['public', 'private']),
    ];
});
