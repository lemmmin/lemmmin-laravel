<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOrderTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_ticket', function (Blueprint $table) {
            Schema::dropIfExists('order_ticket');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('order_ticket', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('ticket_id');
            $table->timestamps();
        });
    }
}
