<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissionables', function (Blueprint $table) {
            $table->integer('user_id');
            $table->integer('permissionable_id');
            $table->string('permissionable_type');
            $table->string('permission');
            $table->boolean('granted')->default(true);

            $table->primary([
                'user_id',
                'permissionable_id',
                'permissionable_type',
                'permission',
            ], 'index');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissionables');
    }
}
