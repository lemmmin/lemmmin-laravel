<?php
if(!class_exists('LemmminHelper')):
class LemmminHelper {

    // Duration of time given to a user before it expires (in minutes).
    public static $order_reserve_duration = 30;

    // Maximum number of tickets that can be purchased in a single order.
    public static $max_ticket_per_order = 100;

    // Minimum payment
    public static $minimum_payment = 1;

    // Percentage of processing fee.
    public static $processing_fee_percent = 0;

    public static $currencies = [
        'BDT' => [
            'symbol' => '৳',
            'name' => 'Bangladeshi Taka',
        ],
    ];
}
endif;
