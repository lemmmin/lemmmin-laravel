var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');
    mix.sass('print.scss');

    mix.copy('node_modules/jquery/dist/jquery.min.js', 'public/js/');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.min.js', 'public/js/');
    mix.copy('node_modules/ajaxchimp/jquery.ajaxchimp.min.js', 'public/js/');
    mix.copy('node_modules/bootstrap/dist/fonts', 'public/fonts');
    mix.copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'public/css/');

    mix.scripts([
        'jquery.simple-dtpicker.js',
        'map-style.js',
        'repeatable-fields.js',
        'app.js',
    ], 'public/js/app.js');

    mix.scripts('jquery.uploadPreview.min.js', 'public/js/jquery.uploadPreview.min.js');

    mix.styles([
        'jquery.simple-dtpicker.css'
    ]);

    mix.version([
        'public/js/app.js',
        'public/css/app.css',
        'public/css/print.css',
        'public/css/all.css',
    ]);
});
