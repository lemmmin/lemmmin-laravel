jQuery(function($){
    // Add organizer form
    var toggleCreateOrganizer =  function() {
      var target = $('#organizer').attr('data-target');
      var organizer = $('#organizer').val();
      if(organizer =="new") {
          $(target).slideDown('fast');
      }
      else {
          $(target).slideUp('fast');
      }
    }

    var linkChangeText = function() {
        var showLess = $("#sin-evn-des-collapse").attr("aria-expanded");
            if(showLess =="true") {
                $(".link-to-expand").html("Show Less...");
            }
            else {
                $(".link-to-expand").html("Read More...");
            }

    }

    var fixDatePickers = function() {
        // For single ticket date picker
        $('.ticket-start-time').appendDtpicker();
        $('.ticket-end-time').appendDtpicker();

        $('.ticket-end-time').change(function() {
            $('.ticket-start-time').appendDtpicker({
                maxDate: $('.ticket-end-time').val() // when the end time changes, update the maxDate on the start field
            });
        });
        $('.ticket-start-time').change(function() {
            $('.ticket-end-time').appendDtpicker({
                minDate: $('.ticket-start-time').val() // when the start time changes, update the minDate on the end field
            });
        });

        // For sub event date picker
        $('.sub-event-start-time').appendDtpicker();
        $('.sub-event-end-time').appendDtpicker();

        $('.sub-event-end-time').change(function() {
            $('.sub-event-start-time').appendDtpicker({
                maxDate: $('.sub-event-end-time').val() // when the end time changes, update the maxDate on the start field
            });
        });
        $('.sub-event-start-time').change(function() {
            $('.sub-event-end-time').appendDtpicker({
                minDate: $('.sub-event-start-time').val() // when the start time changes, update the minDate on the end field
            });
        });
    }

    // for event datepicker
    var setStartDatePicker = function(){
        $('#start-time').appendDtpicker({
            maxDate: $('#end-time').val(), // when the end time changes, update the maxDate on the start field
            closeOnSelected: true
        });
    }
    var setEndDatePicker = function(){
        $('#end-time').appendDtpicker({
            minDate: $('#start-time').val(), // when the start time changes, update the minDate on the end field
            closeOnSelected: true
        });
    }

    var setMapVisibility = function() {
        if ($("#online-event:checkbox:checked").length > 0) {
            $("#loc-offline").slideUp(500);
        }
        else {
            $("#loc-offline").slideDown(500);
        }
    }

    var setNavBarType = function() {
        var windowWidth = $( window ).width();

        if( windowWidth <768 ) {
            $("nav").removeClass('navbar-fixed-top');
            $("nav").addClass('navbar-static-top');
        }
        else {
            $("nav").addClass('navbar-fixed-top');

        };
    };


    $(window).resize(setNavBarType);


    $(document).ready(function() {

        $('[data-toggle="popover"]').popover();

        $('#end-time').change(setStartDatePicker);
        $('#start-time').change(setEndDatePicker);

        setStartDatePicker();
        setEndDatePicker();

        fixDatePickers();

        toggleCreateOrganizer();
        linkChangeText();

        var open = false;
        $(".sin-evn-loc-info-close").on('click',function () {
            if(!open) {
                $(".sin-evn-loc-info-close i").css({
                    transform: 'rotate(225deg)'
                });
                open = true;
            }
            else {
                $(".sin-evn-loc-info-close i").css({
                    transform: 'rotate(45deg)'
                });
                open = false;
            }
            $(".sin-evn-loc-info-box").slideToggle(700);

        })

        // Sub Event custom js
        $(".sub-event-button").on('click',function(){
            $("#sub-event-section").toggle();
        });
        $(".add-speaker-button").on('click',function(){
            $("#add-speaker-section").show();
        });

        $('.relation-repeatable').each(function() {
            var el = this;
            $(this).hasClass(".ticket-decrease")
            $(this).repeatable_fields({
                wrapper: '.relation-wrapper',
                container: '.relation-container',
                template: '.relation-template',
                row: '.relation-row',
                add: '.add-relation',
                row_count_placeholder: '{{key}}',
                after_add: function(container, new_row){
                    var row_count = $(container).attr('data-rf-row-count');
                    var settings = this;

        			row_count++;

        			$('*', new_row).each(function() {
        				$.each(this.attributes, function(index, element) {
        					this.value = this.value.replace(settings.row_count_placeholder, row_count - 1);
        				});
        			});

        			$(container).attr('data-rf-row-count', row_count);

                    new_row.find('.relation-deleted').val('0');

                    var template = $(el).find('.relation-template');
                    $(el).find(this.container).append(template);

                    fixDatePickers();
                }
            });

            $(el).delegate('.remove-relation', 'click', function(e){
                e.preventDefault();
                var relation = $(this).closest('.relation-row');
                if(relation.find('.relation-id').val() == '0'){
                    relation.detach();
                }
                else {
                    relation.find('.relation-deleted').val('1');
                    relation.hide();
                }
            });
        });

        var el = $('.form-fields');
        $(el).delegate('.form-field-types', 'change', function(){
            if($.inArray($(this).val(), ['select', 'radio', 'checkbox']) != -1){
                $(this).closest('.relation-row').find('.form-field-options textarea').removeAttr('disabled');
            }
            else {
                $(this).closest('.relation-row').find('.form-field-options textarea').attr('disabled', 'disabled');
            }
        });


        // Location Section custom js
        $("#online-event:checkbox").on('change', setMapVisibility);

        $('#organizer').on('change', toggleCreateOrganizer);

        $('#sin-evn-des-collapse').on('change', linkChangeText);

        if(typeof $.uploadPreview != 'undefined'){
            $.uploadPreview({
                input_field: "#image-upload",   // Default: .image-upload
                preview_box: "#cover-image-preview .flex-container",  // Default: .image-preview
                label_field: "#image-label",    // Default: .image-label
                label_default: "Choose File",   // Default: Choose File
                label_selected: "Change File",  // Default: Change File
                no_label: false,                 // Default: false
                success_callback: function(){}
            });
        }


        /**
         * =============================================
         * MAILCHIMP NEWSLETTER SUBSCRIPTION
         * =============================================
         */
        if(typeof $.fn.ajaxChimp != 'undefined'){
            $("#mailchimp-subscribe").ajaxChimp({
                callback: mailchimpCallback,
                url: "http://facebook.us14.list-manage.com/subscribe/post?u=70a8b9b8c4ba106b8eb493d49&amp;id=f5ea637665" // Replace your mailchimp post url inside double quote "".
            });
        }

        function  mailchimpCallback(resp){
            $('.home-subscription').slideUp("fast",function(){
                $(this).css({
                    display: 'block'
                });
                if (resp.result === 'success') {
                    $('.home-subscription')
                    .removeClass('home-subscription-failed')
                    .addClass('home-subscription-success')
                    .html('<i class="fa fa-check"></i>' + "&nbsp;" + resp.msg);
            }
                else if (resp.result === 'error') {
                    $('.home-subscription')
                    .removeClass('home-subscription-success')
                    .addClass('home-subscription-failed')
                    .html('<i class="fa fa-close"></i>' + "&nbsp;" + resp.msg);

                }
            });
        }

        $('.ticket.relation-row .ticket-basic').on('change', function(){
            parent = $(this).closest('.ticket.relation-row');
            var hasData = false;
            parent.find('.ticket-basic').each(function(){
                if($(this).val().trim() != ''){
                    hasData = true;
                    return true;
                }
            });

            if(hasData){
                parent.find('.relation-deleted').val("0");
            }
            else {
                parent.find('.relation-deleted').val("1");
            }
        });

        $(".ticket-quantity-value").on('change', function(d, h){
            var row = $(this).closest("tbody");
            var quantityField = row.find('.ticket-quantity-value');
            var remaining = parseInt(row.find('.ticket-remaining').text());
            var maxPurchaseField = row.find('.max-purchase');
            var maxPurchase = parseInt(maxPurchaseField.val());
            var value = parseInt(quantityField.val());
            if(isNaN(value)) value = 0;
            var max = Math.max(Math.min(remaining, maxPurchase), 0);

            $(this).val(Math.min(max, Math.max(value, 0)));
        });

        $(".ticket-quantity-change").on('click', function(){
            var change = 0;
            if($(this).hasClass("ticket-quantity-decrease")) {
                change--;
            }
            else if($(this).hasClass("ticket-quantity-increase")) {
                change++;
            }

            var row = $(this).closest("tbody");
            var quantityField = row.find('.ticket-quantity-value');
            var remaining = parseInt(row.find('.ticket-remaining').text());
            var maxPurchaseField = row.find('.max-purchase');
            var maxPurchase = parseInt(maxPurchaseField.val());
            var value = parseInt(quantityField.val());
            if(isNaN(value)) value = 0;
            var max = Math.min(remaining,maxPurchase);

            if(value+change >= 0 && value+change <= max){
                quantityField.val(value+change);

                var total_tickets_count = 0;
                $('.ticket-quantity-value').each(function(){
                  total_tickets_count += parseInt($(this).val());
                });

                var maxTotalPurchase = $('.max-total-purchase-quantity').val();
                if(total_tickets_count > 0 && total_tickets_count <= maxTotalPurchase){
                  $('#order-button').removeAttr("disabled", "");
                }
                else {
                  $('#order-button').attr("disabled", "disabled");
                }
            }
        });

        $('.link-to-expand').on('click', function(e){
            e.preventDefault();
            var showLess = $(this).attr("aria-expanded");
                if(showLess =="true") {
                    $(this).html("Read More...");
                }
                else if(showLess =="false"){
                    $(this).html("Show Less.");
                }

        });

        $('.scroll').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                console.log(target);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 900);
                    return false;
                }
            }
        });

        var lat = 0.0;
        var lng = 0.0;

        $('.home-search-loc-container input[name="q"]').on('focus', function(){
            if($('.home-search-loc-container select[name="sort"]').val() == 'nearby'){
                if(lat != 0.0 || lng != 0.0) return;

                // Set the coordinates when available
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(){
                        lat = position.coords.latitude;
                        lng = position.coords.longitude;

                        $('.home-search-loc-container input[name="latlng"]').val(lat+','+lng);
                    });
                }
            }
        });

        $('a.save_event').on('click', function(e){
            e.preventDefault();
            var el = this;

            $.get($(this).attr('href'), {}, function(data){
                if(data.success){
                    if(data.current_status == true){
                        $(el).removeClass('fa-bookmark-o').addClass('fa-bookmark');
                    }
                    else {
                        $(el).removeClass('fa-bookmark').addClass('fa-bookmark-o');
                    }
                }
            }, 'json');
        });
    });

    $(window).on('load', function(){
        setMapVisibility();
        setNavBarType();

        $('.form-fields .form-field-types').each(function(){
            if($.inArray($(this).val(), ['select', 'radio', 'checkbox']) == -1) {
                $(this).closest('.relation-row').find('.form-field-options textarea').attr('disabled', 'disabled');
            }
        });
    });
});
