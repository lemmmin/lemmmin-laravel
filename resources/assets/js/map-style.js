/**
 * =======================================
 * GOOGLE MAP
 * =======================================
 */

var placeSearch, autocomplete;
var styleMapComponent = [

    {
        featureType: "administrative",
        elementType: "all",
        stylers: [{
            visibility: "off"
        }]
    }, {
        featureType: "landscape",
        elementType: "all",
        stylers: [{
            visibility: "simplified"
        }, {
            hue: "#0066ff"
        }, {
            saturation: 74
        }, {
            lightness: 100
        }]
    }, {
        featureType: "poi",
        elementType: "all",
        stylers: [{
            visibility: "simplified"
        }]
    }, {
        featureType: "road",
        elementType: "all",
        stylers: [{
            visibility: "simplified"
        }]
    }, {
        featureType: "road.highway",
        elementType: "all",
        stylers: [{
            visibility: "off"
        }, {
            weight: 0.6
        }, {
            saturation: -85
        }, {
            lightness: 61
        }]
    }, {
        featureType: "road.highway",
        elementType: "geometry",
        stylers: [{
            visibility: "on"
        }]
    }, {
        featureType: "road.arterial",
        elementType: "all",
        stylers: [{
            visibility: "off"
        }]
    }, {
        featureType: "road.local",
        elementType: "all",
        stylers: [{
            visibility: "on"
        }]
    }, {
        featureType: "transit",
        elementType: "all",
        stylers: [{
            visibility: "simplified"
        }]
    }, {
        featureType: "water",
        elementType: "all",
        stylers: [{
            visibility: "simplified"
        }, {
            color: "#5f94ff"
        }, {
            lightness: 26
        }, {
            gamma: 5.86
        }]
    }
];

    /**
     * =======================================
     * GOOGLE MAP SHOE ON SINGLE EVENT PAGE
     * =======================================
     */


    function showMapSingle() {
        var showMap = document.getElementById('show-map');

        if(null == showMap) return;
        var showMapLat = parseFloat($("#show-map").parent().find("#map-Lat").val());
        var showMapLng = parseFloat($("#show-map").parent().find("#map-Lng").val());
        var showMapLatLngCenter = new google.maps.LatLng(showMapLat, showMapLng);

        var styledMapType = new google.maps.StyledMapType(
            styleMapComponent, {
                name: 'Styled Map'
            });
        var mapLatLng = new google.maps.Map(showMap, {
            center: showMapLatLngCenter,
            disableDefaultUI: true,
            zoom: 15,
            //    draggable: false,
            scrollwheel: false,
            zoomControl: true,
            rotateControl: true,
            fullscreenControl: true,
            mapTypeId: 'styled_map'
        });

        mapLatLng.mapTypes.set('styled_map', styledMapType);
        mapLatLng.setMapTypeId('styled_map');

        // Set the marker position
        var marker = new google.maps.Marker({
            map: mapLatLng,
            position: showMapLatLngCenter
            // animation: google.maps.Animation.BOUNCE
        });
        marker.setMap(mapLatLng);
    }






function initMap() {
    var mapElement = document.getElementById('map');

    if(null == mapElement) return;

    // Create a new StyledMapType object, passing it an array of styles,
    // and the name to be displayed on the map type control.

    var hasPreviousAddress = false;
    var myLatLng = {
        lat: 23.780676,
        lng: 90.3494174
    };

    var prevLocationName = document.getElementById('location-name').value;
    var prevLocationAddress = document.getElementById('location-address').value;
    var prevLatitude = document.getElementById('location-latitude').value;
    var prevLongitude = document.getElementById('location-longitude').value;

    if(prevLatitude != '' && prevLongitude != ''){
        myLatLng = {
            lat: parseFloat(prevLatitude),
            lng: parseFloat(prevLongitude)
        }

        hasPreviousAddress = true;
    }

    var styledMapType = new google.maps.StyledMapType(
        styleMapComponent, {
            name: 'Styled Map'
        });
    var map = new google.maps.Map(mapElement, {
        center: myLatLng,
        disableDefaultUI: true,
        zoom: hasPreviousAddress?20:13,
        //    draggable: false,
        scrollwheel: false,
        zoomControl: true,
        rotateControl: true,
        fullscreenControl: true,
        mapTypeId: 'styled_map'
    });

    //Associate the styled map with the MapTypeId and set it to display.
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    markers = [];

    // If the page already contains a previous address
    if(hasPreviousAddress){
        var marker = new google.maps.Marker({
            map: map,
            //    icon: icon,
            title: prevLocationName,
            position: myLatLng
        });

        var infowindow = new google.maps.InfoWindow({
            content: prevLocationName+'<br />'+prevLocationAddress
        });

        infowindow.open(map, marker);

        markers.push(marker);
    }

    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();
        place = places[0];

        if (places.length == 0) {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();

        if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
        }
        var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
        };

        var marker = new google.maps.Marker({
            map: map,
            //    icon: icon,
            title: place.name,
            position: place.geometry.location
        });


        var infowindow = new google.maps.InfoWindow({
            content: place.name+'<br />'+place.adr_address
        });

        infowindow.open(map, marker);

        // Create a marker for each place.
        markers.push();

        if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
        } else {
            bounds.extend(place.geometry.location);
        }

        map.fitBounds(bounds);

        fillInAddress(place);
    });

    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */
        (input), {
            types: ['geocode']
        });

    google.maps.event.addDomListener(input, 'keydown', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
        }
    });

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
}

function fillInAddress(place) {

    // Get the place details from the autocomplete object.
    $('#location-name').val(place.name);
    $('#location-address').val(place.adr_address);
    $('#location-latitude').val(place.geometry.location.lat());
    $('#location-longitude').val(place.geometry.location.lng());
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}
