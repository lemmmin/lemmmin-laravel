@extends('layouts.app')

@section('content')
<section class="container-fluid nav-gap auth_bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-3 col-sm-6 col-lg-offset-4 col-lg-4">
                <div class="auth_white_bg">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="auth-heading">
                                <h2>Reset password</h2>
                            </div>
                        </div>
                    </div>

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                        <!-- Email Row -->
                        <div class="row">
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="col-sm-12">
                                <div class="auth-input-field">
                                    <input id="email" placeholder="Email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}">

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- Password Row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="auth-input-field">
                                    <input id="password" placeholder="Password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- Confirm Password Row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="auth-input-field">
                                    <input id="password-confirm" placeholder="Confirm password" type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- Confirm Password Row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="lower-space">
                                    <button type="submit" class="btn btn-primary auth-submit-button">
                                        <i class="fa fa-btn fa-refresh"></i> Reset Password
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</section>
@endsection
