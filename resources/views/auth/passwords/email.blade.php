@extends('layouts.app')

<!-- Main Content -->
@section('content')
<section class="container-fluid nav-gap auth_bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-3 col-lg-offset-4 col-sm-6 col-lg-4">
                <div class="auth_white_bg">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="auth-heading">
                                <h2>Reset Password</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            @if (session('status'))
                            <div class="alert alert-success text-center">
                                {{ session('status') }}
                            </div>
                            @endif
                        </div>
                    </div>

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <!-- Email Row -->
                        <div class="row">
                            <div class="col-sm-12 ">
                                <div class="auth-input-field {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- Send Password Reset Link -->
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn auth-submit-button">
                                    <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                                </button>
                            </div>
                        </div>

                        <!-- Account Swich Row -->
                        <div class="row">
                            <div class = "col-sm-12">
                                <div class="swich-auth">
                                    <a role="button" class="btn forgot-password" href="{{ route('login') }}">Back to login</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
