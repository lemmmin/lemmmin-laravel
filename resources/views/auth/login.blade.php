@extends('layouts.app')

@section('content')
<section class="nav-gap auth_bg container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-3 col-sm-6 col-lg-offset-4 col-lg-4">
                <div class="auth_white_bg">

                    <div class="auth-heading">
                        <h2>Sign in</h2>
                    </div>

                    <!-- Alert Container -->
                    <div class="row">
                        <div class="col-sm-12">
                            @if(Session::has('message'))
                            <div class="alert alert-info">
                                {{ session('message') }}
                            </div>
                            @endif
                        </div>
                    </div>

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="{{ url('/social/facebook') }}"><img class="img-responsive center-block" src="{{ asset('images/fb-button.png') }}" alt=""></a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="{{ url('/social/google') }}"><img class="img-responsive center-block" src="{{ asset('images/google-dark.png')}}" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 or-text">
                                Or,
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="auth-input-field {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- Password Row -->
                        <div class="row">
                            <div class="col-sm-12 ">
                                <div class="auth-input-field {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" placeholder="Password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- Sign In Button Row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn auth-submit-button">
                                    SIGN IN
                                </button>
                            </div>
                        </div>

                        <!-- Remember & Forget Row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="remember-me">
                                            <input type="checkbox" id="squaredThree" name="remember" >
                                            <label for="squaredThree">Remember me</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" >
                                        <div class="cpassword">
                                            <a class="forgot-password" href="{{ url('/password/reset') }}">Forgot Password?</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Account Swich Row -->
                        <div class="row">
                            <div class = "col-sm-12 text-center">
                                <div class="switch-auth">
                                    Don't have an account? <a class="switch-auth" href="{{ route('register') }}"><br class="visible-xs"/><strong class="switch-auth-auth-major">Sign up</strong></a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
