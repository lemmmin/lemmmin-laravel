@extends('layouts.app')

@section('content')
<section class="container-fluid nav-gap auth_bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-3 col-sm-6 col-lg-offset-4 col-lg-4">
                <div class="auth_white_bg">
                    <div class="auth-heading effect5">
                        <h2>Sign up</h2>
                    </div>

                    <!-- Alert Container -->
                    <div class="row">
                        <div class="col-sm-12">
                            @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                            @endif
                        </div>
                    </div>

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <!-- Button Row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="{{ url('/social/facebook') }}"><img class="img-responsive center-block" src="{{ asset('images/fb-button.png') }}" alt=""></a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="{{ url('/social/google') }}"><img class="img-responsive center-block" src="{{ asset('images/google-dark.png')}}" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Or Text -->
                        <div class="row">
                            <div class="col-sm-12 or-text">
                                Or,
                            </div>
                        </div>

                        <!-- Name Row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="auth-input-field">
                                    <input id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ old('name') }}">

                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- Email Row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="auth-input-field">
                                    <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- Password Row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="auth-input-field">
                                    <input id="password" type="password" placeholder="Password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- Confirm Password Row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="auth-input-field">
                                    <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- Sign Up Button Row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn auth-submit-button">
                                    SIGN UP
                                </button>
                            </div>
                        </div>

                        <!-- Account Swich Row -->
                        <div class="row">
                            <div class = "col-sm-12">
                                <div class="switch-auth">
                                    Already have an account? <a class="switch-auth" href="{{ route('login') }}"><br class="visible-xs"/><strong class="switch-auth-auth-major">Sign in</strong></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection
