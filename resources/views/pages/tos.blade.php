@extends('layouts.app')

@section('title', "Terms of Service")

@section('content')

<section class="container nav-gap">
    <div class="row">
        <article class="col-sm-offset-2 col-sm-8">
            <h1 class="text-center">Terms of Service</h1>
            <h2>Introduction</h2>
            <p>
                These terms and condition govern your use of Lemmmin; by using Lemmmin , you accept these terms and conditions in full. If you disagree with these terms and conditions or any part of these terms and conditions, you must not use Lemmmin.
                <br><br>
                You must be at least 18 years of age to use this website. By using this website you warrant or represent that you are 18.
                <br><br>
            </p>

            <h2>License to use website</h2>
            <p>
                Unless otherwise stated, Lemmmin or its licensors own the intellectual property rights on the website. Subject to the license below, all these intellectual property rights are reserved.
                <br><br>
                You may view, download for caching purposes only, and print tickets from the Lemmmin for your own personal use, subject to the restrictions set out the below and elsewhere in these terms and conditions.
                <br><br>
            </p>

            <h3>You must not</h3>
            <ul>
                <li>Republish material from Lemmmin.</li>
                <li>Sell, rent or sub-license material from the website.</li>
                <li>Reproduce, duplicate, copy or otherwise exploit material on this website for a commercial purpose.</li>
                <li>Redistribute material from this website.</li>
            </ul>
            <br><br>

            <h3>Acceptable use</h3>
            <p>
                You must not use Lemmmin  in any way that causes, or may cause, damage to the website/App or impairment of the availability or accessibility of the website; or in any way which is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purposes.
                <br><br>
                You must not conduct any systematic or automated data collection activities  or in relation to Lemmmin  without express written consent.
                <br><br>
                You must not  copy, store, host, transmit, store, publish  any material which consists of any spyware, computer virus.
                <br><br>
            </p>

            <h3>User content</h3>
            <p>
                In these terms and conditions, “your user content” means material (including without limitation text, images, audio material, video material and audio-visual material) that you submit to this website, for whatever purpose.<br>
                <br>
                You grant to Lemmmin a worldwide, irrevocable, non-exclusive, royalty-free license to use, reproduce, adapt, publish, translate and distribute your user content in any existing or future media.  You also grant to [NAME] the right to sub-license these rights, and the right to bring an action for infringement of these rights.<br>
                <br>
                Your user content must not be illegal or unlawful, must not infringe any third party's legal rights, and must not be capable of giving rise to legal action whether against you or Lemmmin or a third party (in each case under any applicable law).<br>
                <br>
                You must not submit any user content to the website that is or has ever been the subject of any threatened or actual legal proceedings or other similar complaint.<br>
                <br>
                Lemmmin reserves the right to edit or remove any material submitted to this website, or stored on Lemmmin’s servers, or hosted or published upon this website.<br>
                <br>
                [Not withs tanding Lemmmin’s rights under these terms and conditions in relation to user content, Lemmmin does not undertake to monitor the submission of such content to, or the publication of such content on, this website.]<br>
                <br>
            </p>

            <h3>No warranties</h3>
            <p>
                Lemmmin provided “as is” without any representations or warranties, express or implied.  Lemmmin makes no representations or warranties in relation to this website or the information and tickets provided on this website.<br>
                <br>
                Without prejudice to the generality of the foregoing paragraph, Lemmmin does not warrant that:<br>
                <br>
                this website will be constantly available, or available at all; or <br>
                the information on this website is complete, true, accurate or non-misleading.<br>
                Nothing on this website constitutes, or is meant to constitute, advice of any kind.  [If you require advice in relation to any [legal, financial or medical] matter you should consult an appropriate professional.]<br>
                <br>
            </p>

            <h2>Limitations of Liability</h2>
            <p>
                Lemmmin will not be liable to you (whether under the law of contact, the law of torts or otherwise) in relation to the contents of, or use of, or otherwise in connection with, this website:
                <br>
                for any indirect, special or consequential loss; or <br>
                for any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or business relationships, loss of reputation or goodwill, or loss or corruption of information or data.<br>
                <br>
                Lemmmin does not verify any host so it is totally the user’s responsibility to know them before buying ticket from Lemmmin.<br>
                Lemmmin is never responsible if the user is not satisfied by the event, or any kind of complaint relating the event which is beyond the scope of the service of Lemmmin.<br>
                If there is any enquiry about refund  in any odd situation, it will depend on host. They may decide whether to refund or not. In such cases, Lemmmin processing fees are not refundable. However, when it is appropriate, Lemmmin may decide to process a refund when it is appropriate.<br>
                <br>
            </p>

        </article>
    </div>
</section>
@endsection
