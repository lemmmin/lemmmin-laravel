@extends('layouts.app')

@section('title', "Privacy Policy")

@section('content')

<section class="container nav-gap">
    <div class="row">
        <article class="col-sm-offset-2 col-sm-8">
            <h1 class="text-center">Privacy Policy</h1>
            <h2>What information do we collect?</h2>
            <p>
                We collect information from you when you register on Lemmmin or
                fill up our form.<br>
                <br>
                When ordering or registering on our site, as appropriate, you
                may be asked to enter your name, e-mail address, mailing address
                phone number or any personal details.<br>
                <br>
            </p>

            <h2>What do we use your information for?</h2>
            <p>
                Any of the information we collect from you may be used in one of
                the following ways:
                <ul>
                    <li>
                        To personalize your experience (your information helps
                        us to better respond to your individual needs)
                    </li>
                    <li>
                        To improve Lemmmin.(we continually strive to improve
                        Lemmmin’s offering based on the information and feedback
                        we received from you.)
                    </li>
                    <li>
                        To improve customer service.(Your information helps us
                        to more effectively respond to your customer service
                        request and support needs.)
                    </li>
                    <li>
                        To process transaction.(Your information whether public
                        or private, will not be sold, exchanged, transferred, or
                        given any other company for any reason whatsoever,
                        without your consent, other than for the express purpose
                        of delivering the purchased product or service requested
                        by the customer)
                    </li>
                    <li>
                        To hosts of events.(We share some of the basic
                        information with the host of event you participate in).
                    </li>
                    <li>To administer a contest, promotion survey or other site
                        feature.
                    </li>
                </ul>
                <br>
            </p>

            <h2>How do we protect your information?</h2>
            <p>
                We implement a variety of security measures to maintain the
                safety of your information when you access your personal
                information.<br>
                <br>
            </p>

            <h2>Do we disclose any information to outside parties?</h2>
            <p>
                We do not sell, trade, or otherwise transfer to outside parties
                your personally identifiable information.This does not include
                trusted third parties who assist us in operating Lemmmin,
                conducting our business, or serving you, so long as those
                parties agree to keep this information confidential. We may also
                release your information when we believe release is appropriate
                to comply with law, enforce our site policies, or protect ours
                or others rights, property, or safety.<br>
                <br>
            </p>

            <h2>Third party links</h2>
            <p>
                Occasionally, at our discretion, we may include or offer third
                party products or service our websites. These third party sites
                have separate and independent privacy policies. We therefore
                have no responsibility or liability for the content and
                activities of these link sites.<br>
                <br>
            </p>

            <h2>Online privacy policy only</h2>
            <p>
                This online privacy policy is applies only to information
                collected through Lemmmin.<br>
                <br>
            </p>

            <h2>Changes to our privacy policy</h2>
            <p>
                If we decide change our privacy policy we will post those changes
                on this page.<br>
                <br>
            </p>
        </article>
    </div>
</section>
@endsection
