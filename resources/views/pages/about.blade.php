@extends('layouts.app')

@section('title', "About Us")

@section('content')

<section class="container nav-gap">
    <div class="row">
        <article class="col-sm-offset-2 col-sm-8">
            <h1 class="text-center">About Us</h1>
            <p>
                <b>Lemmmin</b> is an online ticket marketplace. When you need to buy tickets, <b>Lemmmin</b> is the place to go. Working with us, you have access to tickets for any kind of event in Bangladesh.<br>
                If you want to host an event, you could do it by all other means. But, it has never been easier to collect payments than with us. We take all your hassle to collect the payments and send it to you on time.<br>
                <br>
                Our mission is to provide superior ticketing, marketing and service solutions for our clients and their customers. Lemmmin is the only online ticketing company in Bangladesh and we provide flexible and secure ticketing solutions, digital marketing services, and robust e-commerce fulfillment.<br>
                <br>
            </p>

            <h2>Our Core Values</h2>
            <dl>
                <dt>Service</dt>
                <dd>We pick up the phone when you have questions, and respond over the mail as soon as possible.</dd>

                <dt>Tenacity</dt>
                <dd>Our clients’ businesses depend on our ingenuity and persistence.</dd>

                <dt>Results</dt>
                <dd>We’re committed to getting great results for our clients.</dd>
            </dl>
        </article>
    </div>
</section>
@endsection
