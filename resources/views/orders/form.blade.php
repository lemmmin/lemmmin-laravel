@extends('layouts.dashboard')

@section('title', "Order# $order->id")

@section('content')
    <script>
    fbq('track', 'Form');
    </script>
    <section class="container">
        <div class="row">
            <div class="col-sm-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $message)
                            <li>{{$message}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <p class="alert alert-warning">
                    This order will expire on
                    {{$order->created_at
                        ->addMinutes(LemmminHelper::$order_reserve_duration)
                        ->timezone('Asia/Dhaka')
                        ->toDayDateTimeString()}}
                    unless paid.
                </p>
            </div>
        </div>

        <form class="order-form" action="{{route('orders.form_submit', $order)}}" method="post">
            @foreach($order->passes as $i => $pass)
                <div class="box">
                    <h2>Pass#: {{$pass->passNumber}}</h2>
                    <p><b>Pass Type:</b> {{$pass->ticket_name}}</p>

                    @foreach($order->event->form_fields as $j => $field)
                        <div class="field">
                            @if(in_array($field->type, ['radio', 'checkbox']))
                            <label for="">{{$field->title}}</label>
                            @else
                            <label for="field-{{$i}}-{{$j}}">{{$field->title}}</label>
                            @endif

                            @php
                            $options = preg_split('/\r\n|[\r\n]/', $field->options);
                            @endphp

                            @if($field->type == 'text')
                                <input
                                    type="text"
                                    id="field-{{$i}}-{{$j}}"
                                    class="form-control"
                                    name="field[{{$pass->id}}][{{$field->id}}]"
                                    value="{{old('field.'.$pass->id.'.'.$field->id)}}"
                                    {{$field->required ? 'required="required"':''}}>
                            @elseif($field->type == 'email')
                                <input
                                    type="email"
                                    id="field-{{$i}}-{{$j}}"
                                    class="form-control"
                                    name="field[{{$pass->id}}][{{$field->id}}]"
                                    value="{{old('field.'.$pass->id.'.'.$field->id)}}"
                                    {{$field->required ? 'required="required"':''}}>
                            @elseif($field->type == 'paragraph')
                                <textarea
                                    name="field[{{$pass->id}}][{{$field->id}}]"
                                    id="field-{{$i}}-{{$j}}"
                                    class="form-control"
                                    {{$field->required ? 'required="required"':''}}>{{old('field.'.$pass->id.'.'.$field->id)}}</textarea>
                            @elseif($field->type == 'select')
                                <select
                                    name="field[{{$pass->id}}][{{$field->id}}]"
                                    id="field-{{$i}}-{{$j}}"
                                    class="form-control"
                                    {{$field->required ? 'required="required"':''}}>
                                    @foreach($options as $option)
                                        <option
                                            value="{{$option}}"
                                            {{old('field.'.$pass->id.'.'.$field->id) == $option ? 'selected="selected"': ''}}>{{$option}}</option>
                                    @endforeach
                                </select>
                            @elseif($field->type == 'radio')
                                @foreach($options as $option)
                                    <div class="radio">
                                        <label>
                                            <input type="radio"
                                                value="{{$option}}"
                                                name="field[{{$pass->id}}][{{$field->id}}]"
                                                {{old('field.'.$pass->id.'.'.$field->id) == $option ? 'checked="checked"': ''}}
                                                {{$field->required ? 'required="required"':''}}> {{$option}}
                                        </label>
                                    </div>
                                @endforeach
                            @elseif($field->type == 'checkbox')
                                @foreach($options as $option)
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"
                                                value="{{$option}}"
                                                name="field[{{$pass->id}}][{{$field->id}}][]"
                                                {{in_array($option, (array)old('field.'.$pass->id.'.'.$field->id)) ? 'checked="checked"': ''}}> {{$option}}
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endforeach
                </div>
            @endforeach

            <div class="text-center">
                <button class="btn btn-primary" id="submit-button" type="submit">Continue</button>
                {{csrf_field()}}
            </div>
        </form>
    </section>
@endsection
