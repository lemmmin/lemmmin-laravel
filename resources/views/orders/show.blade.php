@extends('layouts.dashboard')

@section('title', "Order# $order->id")

@section('content')
    @if($order->status == 'complete')
    <script>
    fbq('track', 'Purchase');
    </script>
    @endif
    <section class="container">
        <div class="row">
            <div class="col-sm-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $message)
                            <li>{{$message}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if($order->status == 'pending')
                <p class="alert alert-warning">
                    This order will expire on
                    {{$order->created_at
                        ->addMinutes(LemmminHelper::$order_reserve_duration)
                        ->timezone('Asia/Dhaka')
                        ->toDayDateTimeString()}}
                    unless paid.
                </p>
                @elseif($order->status == 'cancelled')
                <p class="alert alert-danger">
                    This order has been cancelled.
                </p>
                @elseif($order->status == 'expired')
                <p class="alert alert-danger">
                    This order has expired. Please place another order to continue.
                </p>
                @else
                <p class="alert alert-info">
                    Your order information is listed below
                </p>
                @endif
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table order-table">
                        <thead>
                            <tr>
                                <th class="ticket-name">Ticket</th>
                                <th class="ticket-fee">Fee</th>
                                <th class="ticket-quantity">Quantity</th>
                                <th class="ticket-total">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($order->quantities as $row)
                            <tr>
                                <td class="ticket-name">
                                    {{$row->ticket_name}}
                                    @if(!is_null($row->ticket))
                                    <br>
                                    <small>{{$row->ticket->events[0]->title}}</small>
                                    @endif
                                </td>
                                <td class="ticket-fee">৳ {{number_format($row->price, 2, '.', ',')}}</td>
                                <td class="ticket-quantity">{{$row->quantity}}</td>
                                <td class="ticket-total">৳ {{number_format($row->price * $row->quantity, 2, '.', ',')}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot class="order-totals">
                            <tr>
                                <td colspan="2" class="invisible"></td>
                                <td class="order-total">Total</td>
                                <td class="ticket-total">৳ {{number_format($order->meta('total'), 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="invisible"></td>
                                <td class="order-fee">Processing Fee ({{$order->meta('fee_percentage')}}%)</td>
                                <td class="ticket-total">৳ {{number_format($order->meta('fees'), 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="invisible"></td>
                                <td class="order-subtotal">Subtotal</td>
                                <td class="ticket-total">৳ {{number_format($order->meta('total') + $order->meta('fees'), 2, '.', ',')}}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

        @if($order->status == 'complete')
        <div class="text-center">
            <a href="{{route('passes.index', ['order' => $order->id])}}" class="btn btn-primary pull-right">View your passes</a>
        </div>
        @elseif($order->status == 'pending')
            <form action="{{route('orders.payment', $order)}}" method="post">
                @if($order->user->email == '')
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control" required>
                    </div>
                </div>
                @endif
                @if($order->user->meta('phone') == '')
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 form-group">
                        <label for="phone">Phone</label>
                        <input type="text" name="phone" id="phone" class="form-control" required>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-sm-12 text-center"><button type="submit" class="btn btn-primary">Pay &amp; Confirm</button></div>
                </div>
                {{csrf_field()}}
            </form>
        @endif

    </section>
@endsection
