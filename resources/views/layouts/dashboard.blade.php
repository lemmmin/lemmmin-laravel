@extends('layouts.app')

@section('title')
    @yield('title')
@overwrite

@section('content')

<section class="container-fluid @yield('hidden-element') nav-gap">
    <div class="row">
        <div class="col-sm-12 default-header">
            <h1>@yield('title')</h1>
        </div>
    </div>
</section>

@if(Session::has('message'))
<section class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-info">{{session('message')}}</div>
        </div>
    </div>
</section>
@endif

@yield('content')


@overwrite
