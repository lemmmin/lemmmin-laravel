<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', Config::get('app.name'))@hasSection('title') - {{Config::get('app.name')}}@endif</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kreon:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">


    <!-- Styles -->
    <link href="{{ elixir('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ elixir('css/all.css') }}" rel="stylesheet">
    @stack('styles')
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    <link href="{{ elixir('css/print.css') }}" rel="stylesheet" media="print">

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1433646753320585'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1433646753320585&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->

</head>

<body id="app-layout">
    <nav class="navbar navbar-default navbar-fixed-top main-nav-font">
        <div class="container navbar-container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar top-bar"></span>
                    <span class="icon-bar middle-bar"></span>
                    <span class="icon-bar bottom-bar"></span>
                </button>

                <!-- Branding Image -->
                <a href="{{ url('/') }}">
                    <img class="main-logo" src="{{ asset('images/brand.png') }}" alt="Lemmmin">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <!-- <ul class="nav navbar-nav text-center">
                    @if (Auth::check())
                    <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ route('events.index') }}">Events</a></li>
                    <li><a href="{{ route('organizers.index') }}">Organizers</a></li>
                    @endif
                </ul> -->

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right text-center lem-navbar">
                    <!-- Authentication Links -->
                        <li><a href="{{ route('browse') }}">Browse Events</a></li>
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Sign Up</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu lem-dropdown-menu" role="menu">
                                <li>
                                    <a href="{{route('profiles.self.edit')}}" class="profile-link">
                                        <div class="media">
                                            <div class="media-left">
                                                <img class="media-object img-circle" src="{{Auth::user()->avatar(40)}}" alt="...">
                                            </div>
                                            <div class="media-body">
                                                <p>{{ Auth::user()->name }}</p>
                                                <p class="auth-email">{{ Auth::user()->email }}</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="{{ route('events.index') }}">My Events</a></li>
                                <li><a href="{{ route('passes.index') }}">My Passes</a></li>
                                <li><a href="{{ route('events.saved') }}">Saved Events</a></li>
                                <li><a href="{{ route('organizers.index') }}">Manage Organizers</a></li>
                                <li>
                                    <form action="{{ url('/logout') }}" method="post">
                                        <button type="submit" Class="lem-logout-button" >Logout</button>
                                        {{csrf_field()}}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                        <li><a role="button" class="create-event-option" href="{{ route('events.create') }}">Create Event</a></li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- Footer Section  -->
    <div class="container-fluid  lem-footer">
        <div class="row">
            <div class="col-sm-12">
                <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <h5>About Us</h5>
                        <p>
                            Lemmmin is a event and ticket management platform
                            which makes it easy for hosts to sell tickets online
                            and for guests to collect tickets without hassle.
                        </p>
                    </div>

                    <div class="col-sm-3 col-sm-offset-1">
                        <h5>Info</h5>
                        <ul>
                            <li><a href="{{route('about')}}">About Us</a></li>
                            <li><a href="{{route('tos')}}">Terms of Service</a></li>
                            <li><a href="{{route('pp')}}">Privacy Policy</a></li>
                        </ul>
                    </div>

                    <div class="col-sm-3 col-sm-offset-1">
                        <h5>Help</h5>
                        <p>
                            If you are in need of assistance, email us
                            <a href="mailto:hello@lemmmin.com">hello@lemmmin.com</a>
                            or call us <br><a href="tel:+8801771299299">01771-299 299</a>.
                        </p>
                    </div>
                </div>

                <!-- Copyright Row -->
                <div class="row footer-copyright">
                    <hr>
                    <div class="col-sm-12">
                        <p class="text-center">&copy; 2017 Lemmmin, All rights Reserved.</p>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <!-- JavaScripts -->

    <script src="{{ elixir('js/jquery.min.js') }}"></script>
    <script src="{{ elixir('js/bootstrap.min.js') }}"></script>
    @stack('scripts')
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5881d03e46120fcd"></script>
    <script src="{{ elixir('js/app.js') }}"></script>
    @stack('scripts-after')
</body>
</html>
