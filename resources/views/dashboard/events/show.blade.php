@extends('layouts.dashboard')

@section('hidden-element', 'hidden')
@section('title', $event->title)

@section('content')

@php

// Define Variable

// Event start and end time format
$eventStartDate = $event->start_time_object->format('jS F Y');
$eventEndDate = $event->end_time_object->format('jS F Y');

// About section related variable
$length = mb_strlen($event->description,'UTF-8');
$shortNote = true;
$longNote = false;

// Location section related variable
$locationAddress =  $event->meta('location_address');
$locationLatitude =  $event->meta('location_latitude');
$locationLongitude =  $event->meta('location_longitude');

$contactHome = (!is_null($event->organizer)) ?  $event->organizer->meta('contact_home') : '';
$contactWork = (!is_null($event->organizer)) ? $event->organizer->meta('contact_work') : '';
$contactEmail = (!is_null($event->organizer)) ?  $event->organizer->meta('email_address') : '';


//Construct short description logic

if($length<=250){
    $shortDescription = $event->description;
    $shortNote = true;
    $longNote = false;
}
else {
    $fullStop = " ";
    $shortDescription = mb_substr($event->description,0,250,'UTF-8');
    if(false !== ($breakpoint = mb_strrpos($shortDescription, $fullStop, 'UTF-8'))) {
        $shortDescription = mb_substr($shortDescription, 0, $breakpoint+1,'UTF-8');
    }
    $remainDescription = mb_substr($event->description,$breakpoint+1,$length,'UTF-8');
    $longNote = true;
    $shortNote = false;
}
// End

@endphp

<!-- Single Event General Info Show Section -->
<section class="container-fluid nav-gap">
    <div class="row" >
        <div class="sin-evn-img-background" style="background-image:url('{{$event->cover_image}}');">
            <div class="sin-evn-head-container overlay-container-lite">
                <div class="col-sm-12">
                    <div class="sin-evn-head-content">

                        <h1>{{$event->title}}</h1>
                        <p class="sin-evn-head-date">
                            @if($eventStartDate == $eventEndDate)
                            {{$event->start_time_object->format('l')}}, {{$eventStartDate}} <br />
                            {{$event->start_time_object->format('h:i A')}} To {{$event->end_time_object->format('h:i A')}}
                            @else
                            {{$event->start_time_object->format('l')}}, {{$eventStartDate}} {{$event->start_time_object->format('h:i A')}}
                            <br /> To <br />
                            {{$event->end_time_object->format('l')}}, {{$eventEndDate}} {{$event->end_time_object->format('h:i A')}}
                            @endif
                        </p>
                        <a href="#get-tickets" role="button" class="btn sin-evn-head-button scroll">Get tickets</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="container">

    <div class="row">
        <div class="col-sm-12 sin-evn-h3">
            <h3>About Event</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-offset-2 col-sm-8">
            @if($shortNote)
            <div class="sin-evn-des-body">
                {{$shortDescription}}
            </div>
            @elseif($longNote)
            <div class="sin-evn-des-body">
                {{$shortDescription}}
            </div>
            <div class="collapse" id="sin-evn-des-collapse">
                <div class="sin-evn-des-body">
                    {{$remainDescription}}
                </div>
            </div>
            <div class="data-link-a">
                <a class="link-to-expand" data-toggle="collapse" href="#sin-evn-des-collapse" aria-expanded="false" aria-controls="sin-evn-des-collapse"></a>
            </div>
            @endif
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
        </div>
    </div>

</section>


<section class="container">

    <div class="row">
        <div class="col-sm-12 sin-evn-h3">
            <h3>Share Event</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="text-center">
                <div class="addthis_inline_share_toolbox"></div>
            </div>
        </div>
    </div>

</section>

<section class="container-fluid">
    @if(!($event->online_event) && ($locationLatitude != 0 || $locationLongitude != 0))
    <div class="row">
        <div class="col-sm-12 sin-evn-h3">
            <h3>Location</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 sin-evn-loc-info-wrapper" >
            <div class="sin-evn-loc-info-container">
                <div class="sin-evn-loc-info-close">
                    <i class="fa fa-chevron-right"></i>
                </div>
                <div class="sin-evn-loc-info-box" style="display:none">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Venue</h4>
                            <ul>
                                <li>{{$event->meta('location_name')}}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Address</h4>
                            <ul>
                                @if($locationAddress != '')
                                    <li> {!! $locationAddress !!} </li>
                                @else
                                    <li>No address available!</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input id="map-Lat" type="hidden" name="" value="{{$locationLatitude}}">
        <input id="map-Lng" type="hidden" name="" value="{{$locationLongitude}}">
            <div id="show-map"></div>
    </div>
    @elseif(($event->online_event))
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info">
                <p class="sin-evn-online-event-text text-center">This is an <strong >Online</strong> Event</p>
            </div>
        </div>
    </div>
    @endif
</section>

@php
$tickets = $event->tickets()
    ->where([
        ['start_time', '<=', date('Y-m-d H:i:s')],
        ['end_time', '>=', date('Y-m-d H:i:s')],
    ])
    ->whereColumn([
        ['quantity', '>', 'issued']
    ]);
@endphp

<section id="get-tickets" class="container">
    <form action="{{route('events.tickets', $event)}}" method="post">
        <div class="row">
            <div class="col-sm-12">
                <div class="sin-evn-h3">
                    <h3>Buy tickets</h3>
                </div>
            </div>
        </div>
        @if($tickets->count())
        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table lem-table purchase-tickets-table">
                        <thead>
                            <tr>
                                <th>Ticket Name</th>
                                <th>Remaining</th>
                                <th>Price</th>
                                <th>Quantity</th>
                            </tr>
                        </thead>
                        @foreach($tickets->get() as $ticket)
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="ticket-name">
                                            {{$ticket->name}}
                                        </div>
                                    </td>
                                    <td >
                                        <div class="ticket-remaining">
                                            {{$ticket->remaining}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="ticket-price">
                                            @if($ticket->price == 0)
                                                Free
                                            @else
                                                {{$ticket->price}}
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        <div class="ticket-quantity">
                                            <div class="ticket-quantity-change ticket-quantity-decrease"><i class="fa fa-minus-square"></i></div>
                                            <div >
                                                <input class="ticket-quantity-value" type="text" name="tickets[{{$ticket->id}}]" value="0">
                                                <input class="max-purchase" type="hidden" value="{{$ticket->user_remaining}}">
                                            </div>
                                            <div class="ticket-quantity-change ticket-quantity-increase"><i class="fa fa-plus-square"></i></div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        @endforeach
                    </table>
                    <input class="max-total-purchase-quantity" type="hidden" value="{{$event->user_max_ticket_purchase}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">

                    <button class="btn btn-primary sin-evn-order-button" id="order-button" disabled="disabled" type="submit" name="button">Order Now</button>
                    {{csrf_field()}}
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-info">
                    <p class="text-center sin-evn-all-ticket-purchase-text">No tickets are available at this moment</p>
                </div>
            </div>
        </div>
        @endif
    </form>
</section>

<section class="container section-gap">
    @if(isset($event->organizer->name))
    <div class="row">
        <div class="col-sm-12">
            <div class="sin-evn-h3">
                <h3>Organizer Information</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-offset-3 col-sm-6">
            <div class="organizer-name">
                <a href="{{route('organizers.show', $event->organizer)}}">
                    {{$event->organizer->name}}
                </a>
            </div>
            <div class="organizer-about-panel">
                <p class="organizer-about">{{$event->organizer->about}}</p>
            </div>
            <div class="social-address-link">
                <h5 class="social-address-headline">Social Address</h5>
                @if(($event->organizer->meta('social_facebook')))
                <a href="{{$event->organizer->meta('social_facebook')}}"><i class="fa fa-facebook"></i></a>
                @endif
                @if(($event->organizer->meta('social_twitter')))
                <a href="{{$event->organizer->meta('social_twitter')}}"><i class="fa fa-twitter"></i></a>
                @endif
                @if(($event->organizer->meta('email_address')))
                <a href="{{$event->organizer->meta('email_address')}}"><i class="fa fa-envelope"></i></a>
                @endif
                @if(($event->organizer->meta('website')))
                <a href="{{$event->organizer->meta('website')}}"><i class="fa fa-globe"></i> </a>
                @endif
            </div>
        </div>
    </div>
    @endif
</section>

<section class="container-fluid sin-evn-sub-event-schedule">
    <div class="row">
        <div class="col-sm-12 sin-evn-h3">
            <h3>Events Schedule</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                @foreach( $event->children as $subevent)

                    @php
                        $offset = '';
                        $startTimeDateStr = $subevent->start_time_object->format('jS F Y');
                        $endTimeDateStr = $subevent->end_time_object->format('jS F Y');

                        if (($loop->count - $loop->iteration == 0)  && ($loop->count)%3 === 1)
                        $offset = 'col-sm-offset-4';
                        elseif (($loop->count - $loop->iteration == 1) && ($loop->count)%3 === 2)
                        $offset = 'col-sm-offset-2';
                    @endphp

                    <div class="{{$offset}} col-sm-4">
                        <div class="sin-evn-sub-event-card-container">
                            <div class="sin-evn-sub-event-card">
                                <h3>{{$subevent->title}}</h3>
                                @if($startTimeDateStr == $endTimeDateStr)
                                <p class="sin-evn-sub-event-card-schedule">Schedule </p>
                                <p>{{$subevent->start_time_object->format('l')}}, {{ $startTimeDateStr }} </p>
                                <p> At </p>
                                <p> {{ $subevent->start_time_object->format('h:i A')}} To {{$subevent->end_time_object->format('h:i A') }}</p>
                                @else
                                <p class="sin-evn-sub-event-card-schedule">Schedule </p>
                                <p> {{$subevent->start_time_object->format('l')}}, {{ $startTimeDateStr }} at {{ $subevent->start_time_object->format('h:i A')}} </p>
                                <p>To</p>
                                <p> {{$subevent->end_time_object->format('l')}}, {{ $endTimeDateStr }} at {{ $subevent->end_time_object->format('h:i A')}} </p>
                                @endif
                            </div>
                            <div class="sin-evn-sub-event-card-overlay">
                                <p>{{$subevent->description}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>


@if(Gate::check('update', $event) || Gate::check('delete', $event) || Gate::check('see_guests', $event))
<div class="lemmmin-float-bar">
    <a class="active lem-back" href="{{ route('events.index')}}"><i class="fa fa-chevron-left"></i></a>
    @can('see_guests', $event)
    <a class="lem-guest" href="{{ route('events.guests', $event)}}"><i class="fa fa-users"></i></a>
    @endcan
    @can('update', $event)
    <a class="lem-edit" href="{{ route('events.edit', $event)}}"><i class="fa fa-pencil"></i></a>
    @endcan
    @can('delete', $event)
    <form  action="{{route('events.destroy', $event->id)}}" method="post">
        <button type="submit" class="lem-trash">
            <i class="fa fa-trash"></i>
        </button>
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    </form>
    @endcan
</div>
@endcan

@endsection

@push('scripts-after')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_5pvfvn7OmtCQJifqvHo6y3uyzziHV5Y&libraries=places&callback=showMapSingle"></script>
@endpush
