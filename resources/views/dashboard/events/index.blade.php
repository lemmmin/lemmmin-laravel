@extends('layouts.dashboard')

@section('title', 'All My Events')

@section('content')

<section class="container section-gap">
    <div class="row">
        <div class="col-sm-12">
            @if(count($events))
            <div class="row">
                @each('dashboard.events.snippets.event', $events, 'event')
            </div>
            @else
            @include('dashboard.events.snippets.no-events')
            @endif
            <div class="text-center">
                {{ $events->links() }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-offset-4 col-sm-4">
                <a href="{{route('events.create')}}" class="btn btn-primary btn-slim btn-fullwidth">Create New</a>
            </div>
        </div>
    </div>
</section>
@endsection
