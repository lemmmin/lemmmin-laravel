@extends('layouts.dashboard')

@section('title', 'Create Event')

@section('content')

<form class="form" action="{{route('events.store')}}" method="post" enctype="multipart/form-data" >

    @if (count($errors) > 0)
    <section class="container section-gap">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
    @endif

    <!-- General Information about Event -->
    <section class="container section-gap">
        <div class="row">
            <div class="col-sm-12">
                @include('dashboard.events.snippets.general-info')
            </div>
        </div>
    </section>

    <!--  Events Location -->
    <section class="container section-gap">
        <div class="row">
            <div class="col-sm-12">
                @include('dashboard.events.snippets.location')
            </div>
        </div>
    </section>

    <!-- Tickets Create Section -->
    <section class="container section-gap">
        <div class="row">
            <div class="col-sm-12">
                @include('dashboard.events.snippets.ticket-section')
            </div>
        </div>
    </section>

    <!-- Add Organizer Section -->
    <section class="container section-gap">
        <div class="row">
            <div class="col-xs-12">
                @include('dashboard.events.snippets.organizer-list')
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <hr class="org-finish hr-first-child">
                <hr class="org-finish hr-last-child">
            </div>
        </div>
    </section>

    <!-- Form Fields Section -->
    <section class="container section-gap" >
        <div id="form-fields-section" class="row">
            <div class="col-sm-12">
                @include('dashboard.events.snippets.form-fields-section')
            </div>
        </div>
    </section>

    <!-- Add Sub events Section -->
    <section class="container section-gap" >
        <div id="sub-event-section" class="row">
            <div class="col-sm-12">
                @include('dashboard.events.snippets.sub-events')
            </div>
        </div>
    </section>

    <!-- Event Create Button Section-->
    <section class="container section-gap">
        <div class="row">
            <div class="create-event-button">
                <div class="col-sm-offset-2 col-sm-4">
                    <button name="live" value="" type="submit" class="btn btn-secondary">Save Event</button>
                </div>
                <div class="col-sm-4">
                    <button name="live" value="on" class="btn btn-primary">Make Event Live</button>
                    {{ csrf_field() }}
                </div>

            </div>
        </div>
    </section>

</form>
@endsection


@push('scripts')
<script type="text/javascript" src="{{asset('/js/jquery.uploadPreview.min.js')}}"></script>
@endpush

@push('scripts-after')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_5pvfvn7OmtCQJifqvHo6y3uyzziHV5Y&libraries=places&callback=initMap"></script>
@endpush
