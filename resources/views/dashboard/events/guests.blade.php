@extends('layouts.dashboard')

@section('title', $event->title)

@section('content')
    <section class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Pass#</th>
                                <th>User</th>
                                <th>Ticket Type</th>
                                <th>Order#</th>
                                <th>Status</th>
                                <th>Purchased On</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($passes as $pass)
                            <tr>
                                <td><a href="{{route('passes.show', $pass->pass_number)}}">{{$pass->pass_number}}</a></td>
                                <td>{{$pass->user->name}}</td>
                                <td>{{$pass->ticket_name}}</td>
                                <td>{{$pass->order_id}}</td>
                                <td>{{$pass->status}}</td>
                                <td>{{$pass->created_at}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{$passes->links()}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
