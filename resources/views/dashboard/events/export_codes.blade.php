<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Export Pass QR Codes for {{$event->title}}</title>
        <link href="{{ elixir('css/bootstrap.min.css') }}" rel="stylesheet">
    </head>
    <body>

        <div class="container">
            @foreach($passes as $pass)
            <div class="code_unit">
                <img class="img-responsive" src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->color((int) $colors[0], (int) $colors[1], (int) $colors[2])->size(100)->margin(1)->generate($pass->pass_number)) !!} ">
                <br>
                <div class="pass_id">
                    <span>{{$pass->pass_number}}</span>
                </div>
            </div>
            @endforeach

            <div class="clearfix"></div>

            <div class="text-center">
                {{ $passes->links() }}
            </div>
        </div>

        <style>
            .code_unit {
                float:left;
                width:0.9in;
                margin:0.05in;
                position:relative;
            }
            .code_unit img {
                margin: 0;
                max-width: 100%;
                display: inline-block;
            }
            .pass_id {
                text-align:center;
                margin: 0;
                margin-top: -5px;
            }

            .pass_id span {
                background-color: white;
                font-size: 8pt;
            }
        </style>

        <style media="print">
            .container {
                width: 8.5in;
            }
            .pagination {
                display: none;
            }
        </style>

    </body>
</html>
