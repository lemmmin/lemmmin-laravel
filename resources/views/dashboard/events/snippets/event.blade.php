
<div class="col-sm-4">
    <div id="event-{{$event->id}}" class="{{$event->live ? 'live-event' : ''}}">
        <div class="single-event-card">

            <div class="single-event-card-cover-image">
                <a href="{{route('events.edit', $event)}}">
                    <img class="img-responsive" src="{{$event->thumbnail}}" alt="Cover Image">
                </a>
            </div>
            <div class="single-event-card-short-data">
                <div class="row">
                    <div class="col-xs-2">
                        <div class="single-event-card-date-box">
                            {!! $event->start_time_object->format('<p>j <b\r />M</p>') !!}
                        </div>
                    </div>
                    <div class="col-xs-10">
                        <div class="single-event-card-heading">
                            <h4 >
                                <a href="{{route('events.edit', $event->id)}}">
                                    @if(strlen($event->title < 30))
                                    {{$event->title}}
                                    @else
                                    {{substr($event->title,0,30)}} . . .
                                    @endif
                                </a>
                            </h4>
                            @if($event->organizer != null)
                                <p >Organized By <a href="{{route('organizers.show', $event->organizer)}}">{{$event->organizer->name}}</a></p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-offset-2 col-xs-10">
                        <div class="single-event-card-description">
                            <p>
                                @if(strlen($event->description) < 100)
                                    {{$event->description}}
                                @else
                                    {{substr($event->description,0,100)}} . . .
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-event-card-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="single-event-card-footer-privacy">
                            {{$event->privacy}}
                        </div>
                        <div class="single-event-card-footer-show">
                            <a href="{{route('events.show', $event->id)}}">
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="single-event-card-footer-trash">
                            <form  action="{{route('events.destroy', $event->id)}}" method="post">
                                <button type="submit" class="btn btn-link">
                                    <i class="fa fa-trash"></i>
                                </button>
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
