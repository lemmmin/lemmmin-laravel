
<div class="row ">
    <div class="col-xs-12">

        <!-- Organizer Name -->
        <div class="row field-hint">
            <div class="col-xs-12">
                <label for="organizer-name">Organizer Name</label>
                <input type="text" name="organizer_name" id="organizer-name" class="form-control" value="{{isset($event)? $event->organizer_name : ''}}">
            </div>
        </div>

        <!-- About Organizer -->
        <div class="row field-hint">
            <div class="col-xs-12">
                <label for="organizer-about">About Organizer</label>
                <textarea type="text" name="organizer_about" id="organizer-about" class="form-control">{{isset($event)? $event->organizer_about : ''}}</textarea>
                <small>Organizer will not be saved if it's name is skipped.</small>
            </div>
        </div>
    </div>
</div>
