
    <!-- Section Level Row -->
    <div class="row">
        <div class="col-sm-12">
            <h3 class="text-left md-ubuntu-font">Form Fields</h3>
        </div>
    </div>

    <!-- Fields container -->
    <div class="row relation-repeatable form-fields">

        <div class="relation-wrapper">
            <div class="relation-container">
                @php
                $count = 0;
                if(isset($event)) $count = $event->form_fields()->count();
                @endphp
                @for($i = 0; $i < $count; $i++)
                    @include('dashboard.events.snippets.form-field-single', [
                        'deleted' => 0,
                        'key' => $i,
                        'class' => 'form-field-template'
                    ])
                @endfor
                @include('dashboard.events.snippets.form-field-single', [
                    'deleted' => 1,
                    'key' => "@{{key}}",
                    'class' => 'form-field-template relation-template'
                ])
            </div>
        </div>
    </div>
