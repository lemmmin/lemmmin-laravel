@php
if(isset($event) && isset($event->tickets[$key])){
    $data = $event->tickets[$key];
    $data['max_purchase'] = $event->tickets[$key]->max_purchase;
}
else
    $data = [
        'id' => 0,
        'name' => '',
        'quantity' => '',
        'price' => '',
        'max_purchase' => '5',
        'start_time' => '0000-00-00 00:00',
        'end_time' => '9999-12-31 23:59',
        'currency' => '',
        'deleted' => false,
        'privacy' => 'public',
        'description' => ''
    ];
@endphp
<tbody class="{{$class or ''}} ticket relation-row">
    <tr>
        <td>
            <input class="form-control ticket-name ticket-basic" type="text" id="tickets-{{$key}}-name" name="tickets[{{$key}}][name]" value="{{ $data['name'] }}">
        </td>
        <td>
            <input class="form-control ticket-quantity ticket-basic" type="text" id="tickets-{{$key}}-quantity" name="tickets[{{$key}}][quantity]" value="{{ $data['quantity']}}">
        </td>
        <td>
            <input class="form-control ticket-price ticket-basic" type="text" id="tickets-{{$key}}-price" name="tickets[{{$key}}][price]" value="{{ $data['price']}}">
        </td>
        <td>
            <i class="tkt-remove fa fa-trash-o remove-relation"></i>
            <a role="button" data-toggle="collapse" href="#tkt-{{$key}}-additional-field" aria-expanded="false" aria-controls="tkt-additional-field" class="tkt-additional-info-trigger">
                <i class="fa fa-ellipsis-v"></i>
            </a>
            <input class="relation-id" type="hidden" name="tickets[{{$key}}][id]" value="{{ $data['id']}}">
            <input class="relation-deleted" type="hidden" name="tickets[{{$key}}][deleted]" value="{{ $data['deleted']}}">
        </td>
    </tr>
    <tr class="collapse additional-info" id="tkt-{{$key}}-additional-field">
        <td>
            <div class="content-padding">
                <input class="form-control ticket-start-time" placeholder="Starts" type="text" id="tickets-{{$key}}-start-time" name="tickets[{{$key}}][start_time]" value="{{$data['start_time']}}">
            </div>
            <div class="content-padding">
                <select name="tickets[{{$key}}][currency]" class="form-control" id="tickets-{{$key}}-currency">
                    @php
                    $val = isset($ticket->currency) ? $ticket->currency : old('tickets.'.$key.'.currency');
                    @endphp

                    @foreach(LemmminHelper::$currencies as $code => $currency)
                    <option value="{{$code}}" {!! $val == $code ? 'selected="selected"' : '' !!}>{{$currency['name']}} ({{$code}})</option>
                    @endforeach
                </select>
            </div>
        </td>
        <td>
            <div class="content-padding">
                <input class="form-control ticket-end-time" placeholder="Ends" type="text" id="tickets-{{$key}}-end-time" name="tickets[{{$key}}][end_time]" value="{{$data['end_time']}}">
            </div>
            <div class="content-padding">
                <input class="form-control ticket-maximum-purchase" placeholder="Maximum Purchase Quantity" id="tickets-{{$key}}-max-purchase" type="text" name="tickets[{{$key}}][max_purchase]" value="{{$data['max_purchase']}}">
            </div>
            {{--<div class="content-padding">
                <label for="tickets-{{$key}}-private">
                    <input type="checkbox" id="tickets-{{$key}}-private" name="tickets[{{$key}}][private]" {!! ( ( isset($ticket) && $ticket->privacy == 'private' ) || old('tickets.'.$key.'.private') == 'on' ) ? 'checked="checked"':''!!} value="on">
                    Private Event
                </label>
            </div>--}}
        </td>
        <td colspan="2">
            <div class="content-padding">
                <textarea rows="3" class="form-control" placeholder="Ticket Description" id="tickets-{{$key}}-description" name="tickets[{{$key}}][description]">{{$data['description']}}</textarea>
            </div>
        </td>
    </tr>
</tbody>
