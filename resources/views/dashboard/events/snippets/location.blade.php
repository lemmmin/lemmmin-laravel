
    <!-- Section Level Row -->
    <div class="row">
        <div class="col-sm-12">
            <h3>Locations</h3>
        </div>
    </div>

    <!-- Checkbox to declare online or offline -->

    <div class="row">
        <div class="col-sm-10 loc-on-off-checkbox">
            <input type="checkbox" name="online_event" id="online-event" {{isset($event) && $event->online_event ? 'checked="checked"' : ''}}>
            <label for="online-event">Online Event</label>
        </div>
    </div>

    <!-- IF not checked that row will appear -->
    <div class="row">
        <div id="loc-offline" class="col-sm-12">
            <input type="hidden" name="location[name]" id="location-name" value="{{ isset($event)? $event->meta('location_name') : ''}}">
            <input type="hidden" name="location[address]" id="location-address" value="{{ isset($event)? $event->meta('location_address') : ''}}">
            <input type="hidden" name="location[latitude]" id="location-latitude" value="{{ isset($event)? $event->meta('location_latitude') : ''}}">
            <input type="hidden" name="location[longitude]" id="location-longitude" value="{{ isset($event)? $event->meta('location_longitude') : ''}}">
            <input id="pac-input" class="controls" type="text" placeholder="Location Address">
            <div id="map"></div>
        </div>
    </div>
