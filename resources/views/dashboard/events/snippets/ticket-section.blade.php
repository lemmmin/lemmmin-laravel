
    <!-- Section Level Row -->
    <div class="row">
        <div class="col-sm-12">
            <h3 class="text-left md-ubuntu-font">Tickets</h3>
        </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <label for="max-ticket-purchase-quantity">Maximum Purchase Quantity</label>
        <input type="text" class="form-control" name="max_ticket_purchase_quantity" id="max-ticket-purchase-quantity" value="{{isset($event) ? (int) $event->meta('max_ticket_purchase_quantity', 0) : 0}}">
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <small class="help-block">Maximum number of tickets one can purchase. Use 0 for unlimited.</small>
      </div>
    </div>

    <!-- Ticket panel -->
    <div class="row relation-repeatable">
        <div class="col-sm-12 relation-wrapper">
            <table class="table lem-table relation-container">

                <thead>
                    <tr>
                        <th>Ticket Name</th>
                        <th>Quantity Available</th>
                        <th>Price</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                @if(!isset($event) || (isset($event) && $event->tickets()->count()==0))
                    @include('dashboard.events.snippets.tickets', [
                        'key' => 0,
                        'deleted' => "1",
                        'class' => "ticket-template"
                    ])
                @endif
                @php
                $count = 0;
                if(isset($event)) $count = count($event->tickets);
                @endphp
                @for($i = 0; $i < $count; $i++)
                    @include('dashboard.events.snippets.tickets', [
                        'deleted' => 0,
                        'key' => $i,
                        'class' => 'ticket'
                    ])
                @endfor

                @include('dashboard.events.snippets.tickets', [
                    'key' => "@{{key}}",
                    'deleted' => "1",
                    'class' => "ticket-template relation-template"
                ])

                <tfoot>
                    <tr>
                        <td colspan="4">
                            <div class="add-relation add-ticket">
                                <div class="add-option">
                                    <i class="fa fa-plus-square-o"></i>
                                    <span>New</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tfoot>

            </table>

        </div>
    </div>
