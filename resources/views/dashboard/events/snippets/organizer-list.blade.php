
    <div class="row lower-space">
        <div class="col-sm-12">
            <h3 class="org-h3">Organizer</h3>
        </div>
    </div>

    <!-- Orgainzer List -->
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="single-event-page-organizer-select-box">
                <select name="organizer" id="organizer" data-target="#new-organizer">
                    <option value="">Select</option>
                    @foreach($organizers as $organizer)
                    <option value="{{$organizer->id}}" {{isset($event) && $event->organizer != null && $event->organizer->id == $organizer->id ? 'selected="selected"' : ''}}>{{$organizer->name}}</option>
                    @endforeach
                    @can('create', 'App\Organizer')
                    <option value="new">Create New</option>
                    @endcan
                </select>
            </div>
            <div id="new-organizer" style="display:none">
                @include('dashboard.events.snippets.add-organizer')
            </div>
        </div>
    </div>
