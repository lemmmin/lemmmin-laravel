@php

if(isset($event) && isset($event->form_fields[$key]))
    $data = $event->form_fields[$key];
else
    $data = [
        'id' => 0,
        'title' => '',
        'type' => 'text',
        'options' => '',
        'hint' => '',
        'required' => false,
        'deleted' => false,
    ];
@endphp

<!-- Form Field Defination -->
<div class="col-sm-6 {{$class or ''}} form-field relation-row">

    <!-- Title -->
    <div class="row field-hint">
        <div class="col-sm-12">
            <label for="form-fields-{{$key}}-title">Title</label>
            <input
                type="text"
                class="form-control"
                id="form-fields-{{$key}}-title"
                name="form_fields[{{$key}}][title]"
                value="{{ $data['title'] }}">
        </div>
    </div>
    <!-- End Title -->

    <!-- Field Type -->
    <div class="row field-hint">
        <div class="col-sm-12">
            <label for="form-fields-{{$key}}-type">Field Type</label>
            <select
                class="form-control form-field-types"
                id="form-fields-{{$key}}-type"
                name="form_fields[{{$key}}][type]">

                @foreach($field_types as $type => $name)
                <option
                    value="{{$type}}"
                    @if($type == $data['type'])
                    selected="selected"
                    @endif>{{$name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <!-- End Field Type -->

    <!-- Options -->
    <div class="row field-hint form-field-options">
        <div class="col-sm-12">
            <label for="form-fields-{{$key}}-options">Options</label>
            <textarea
                class="form-control"
                id="form-fields-{{$key}}-options"
                name="form_fields[{{$key}}][options]">{{$data['options']}}</textarea>
            <p class="help-block">Specify options one per line.</p>
        </div>
    </div>
    <!-- End Options -->

    <!-- Hint -->
    <div class="row field-hint">
        <div class="col-sm-12">
            <label for="form-fields-{{$key}}-hint">Hint Text</label>
            <textarea
                class="form-control"
                id="form-fields-{{$key}}-hint"
                name="form_fields[{{$key}}][hint]">{{$data['hint']}}</textarea>
        </div>
    </div>
    <!-- End Hint -->

    <!-- Required -->
    <div class="row field-hint">
        <div class="col-sm-12">
            <label for="form-fields-{{$key}}-required">
            <input
                type="checkbox"
                id="form-fields-{{$key}}-required"
                @if($data['required'])checked="checked"@endif
                name="form_fields[{{$key}}][required]">Required Field
            </label>

            <label class="pull-right"><a href="#" class="btn btn-link remove-relation">Remove</a></label>
        </div>
    </div>
    <!-- End Required -->

    <!-- End Event Description -->
    <div class="add-relation add-form-field">
        <div class="add-option">
            <i class="fa fa-plus-square-o"></i>
            <br>
            <span>New</span>
        </div>
    </div>

    <div class="clearfix"></div>
    <input type="hidden" class="relation-id" name="form_fields[{{$key}}][id]" value="{{ $data['id'] }}">
    <input type="hidden" class="relation-deleted" name="form_fields[{{$key}}][deleted]" value="{{ $data['deleted'] }}">
</div>
<!-- End Sub Event Form -->
