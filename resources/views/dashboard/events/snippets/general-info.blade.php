
    <!-- Section Level Row -->
    <div class="row">
        <div class="col-sm-12">
            <h3> General Information </h3>
        </div>
    </div>

    <!-- Setting Box -->
    <div class="row">
        <div class="col-sm-6">

            <!-- Event Title Row -->
            <div class="row field-hint">
                <div class="col-sm-12">
                    <label for="event-title">Add Event Title</label>

                    <input class="form-control" type="text" name="title" id="event-title" value="{{isset($event)? $event->title : ''}}">
                </div>
            </div>
            <!-- End Event Title -->

            <!-- Date & Time Row -->
            <div class="row field-hint">
                <div class="col-sm-6">
                    <label for="start-time">Starts</label>
                    <input class="form-control" type="text" name="start_time" id="start-time" value="{{isset($event)? $event->start_time : ''}}">
                </div>

                <div class="col-sm-6">
                    <label for="end-time">Ends</label>
                    <input class="form-control" type="text" name="end_time" id="end-time" value="{{isset($event)? $event->end_time : ''}}">
                </div>
            </div>
            <!-- End Date & Time -->

            <!-- Event Description Row -->
            <div class="row field-hint">
                <div class="col-sm-12">
                    <label for="description">Description</label>
                    <textarea class="form-control" type="text" name="description" id="description" rows="6">{{isset($event)? $event->description : ''}}</textarea>
                </div>
            </div>
            <!-- End Event Description -->

            <!-- Event Visibility -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="row field-hint">
                        <div class="col-sm-12">
                            <p>Privacy</p>
                        </div>
                    </div>
                    <div class="row">
                        @php
                        $privacy = 'public';
                        if(isset($event)){
                            $privacy = $event->privacy;
                        }
                        @endphp
                        <div class="col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon no-background">
                                    <input type="radio" id="public" name="privacy" value="public" title="Public" {!! $privacy=='public' ? 'checked = "checked"' : '' !!}>
                                </span>
                                <label for="public" class="des">Public</label>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="input-group" id="privacy">
                                <span class="input-group-addon no-background">
                                    <input id="private" type="radio" name="privacy" value="private" title="Private" {!! $privacy=='private' ? 'checked = "checked"' : '' !!}>
                                </span>
                                <label for="private" class="des">Private</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Event Visibility -->
        </div>
        <!-- End Setting Box -->

        <!-- Add Cover Image Box -->
        <div class="col-sm-6" >
            <div class="upload-bg-color" id="cover-image-preview">
                <div class="flex-container">
                    <div class="flex-item" >
                        <img class="img-responsive" src="{{ asset('images/camera.png') }}" alt="add or drag image">
                        <label for="image-upload" id="image-label">
                            @if(isset($event) && $event->hasMedia('cover_image'))
                            Change File
                            @else
                            Choose File
                            @endif
                        </label>
                        <input type="file" name="cover_image" id="image-upload" />
                    </div>
                </div>
            </div>
        </div>
    </div>

@if(isset($event) && $event->hasMedia('cover_image'))
@push('styles')
    <style media="screen">
        #cover-image-preview .flex-container {
            background-image: url({{$event->thumbnail}});
            background-size: cover;
            background-position: center center;
        }
    </style>
@endpush
@endif
