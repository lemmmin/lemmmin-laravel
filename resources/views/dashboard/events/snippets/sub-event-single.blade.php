@php

if(isset($event) && isset($event->children[$key]))
    $data = $event->children[$key];
else
    $data = [
        'id' => 0,
        'title' => '',
        'start_time' => '',
        'end_time' => '',
        'deleted' => false,
        'description' => ''
    ];
@endphp

<!-- Sub Event Form -->
<div class="col-sm-6 {{$class or ''}} subevent relation-row">

    <!-- Name Fields -->
    <div class="row field-hint">
        <div class="col-sm-12">
            <label for="subevents-{{$key}}-title">Sub Event Name</label>
            <input type="text" class="form-control" id="subevents-{{$key}}-title" name="subevents[{{$key}}][title]" value="{{ $data['title'] }}">
        </div>
    </div>
    <!-- End Name Fields -->

    <!-- Date & Time Row -->
    <div class="row field-hint">
        <div class="col-sm-6">
            <label for="subevents-{{$key}}-start-time">Starts</label>
            <input class="form-control sub-event-start-time" type="text" id="subevents-{{$key}}-start-time" name="subevents[{{$key}}][start_time]" value="{{ $data['start_time'] }}">
        </div>

        <div class="col-sm-6">
            <label for="subevents-{{$key}}-end-time">Ends</label>
            <input class="form-control sub-event-end-time" type="text" id="subevents-{{$key}}-end-time" name="subevents[{{$key}}][end_time]" value="{{ $data['end_time'] }}">
        </div>
    </div>
    <!-- End Date & Time -->

    <!-- Sub Event Description Row -->
    <div class="row field-hint">
        <div class="col-sm-12">
            <label for="subevents-{{$key}}-description">Description</label>
            <textarea class="form-control" id="subevents-{{$key}}-description" name="subevents[{{$key}}][description]" rows="6">{{ $data['description'] }}</textarea>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-right">
            <a href="#" class="btn btn-link remove-relation">Remove</a>
        </div>
    </div>

    <!-- End Event Description -->
    <div class="add-relation add-subevent">
        <div class="add-option">
            <i class="fa fa-plus-square-o"></i>
            <br>
            <span>New</span>
        </div>
    </div>

    <div class="clearfix"></div>
    <input type="hidden" class="relation-id" name="subevents[{{$key}}][id]" value="{{ $data['id'] }}">
    <input type="hidden" class="relation-deleted" name="subevents[{{$key}}][deleted]" value="{{ $data['deleted'] }}">
</div>
<!-- End Sub Event Form -->
