<section class="container">
    <div class="row">
        <div class="col-sm-offset-3 col-sm-6">
            <div class="no-organizer-text alert alert-warning">Currently you have <span>no event </span>.<br>Please create it by clicking <br><span>CREATE NEW</span> <br>button if you want to.</div>
        </div>
    </div>
</section>
