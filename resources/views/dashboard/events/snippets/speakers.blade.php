
    <!-- Section Level Row -->

    <div class="row">
        <div class="col-sm-12">
            <h3 class="text-left md-ubuntu-font">Speakers</h3>
        </div>
    </div>

    <!-- Setting Box -->
    <div class="row">
        <div class="col-sm-6">

            <!-- Event Title Row -->
            <div class="row field-hint">
                <div class="col-sm-12">
                    <label for="subevents-{{$key}}-title">Speaker Name</label>
                    <input type="text" class="form-control" id="subevents-{{$key}}-title" name="subevents[{{$key}}][title]" value="{{$subevent->title or old('subevents.'.$key.'.title')}}">
                </div>
            </div>
            <!-- End Event Title -->

            <!-- Designation Row -->
            <div class="row field-hint">
                <div class="col-sm-12">
                    <label for="subevents-{{$key}}-title">Speaker Role</label>
                    <input type="text" class="form-control" id="subevents-{{$key}}-title" name="subevents[{{$key}}][title]" value="{{$subevent->title or old('subevents.'.$key.'.title')}}">
                </div>
            </div>
            <!-- End Event Title -->

            <!-- Event Description Row -->
            <div class="row field-hint">
                <div class="col-sm-12">
                    <label for="subevents-{{$key}}-description">About Speaker</label>
                    <textarea class="form-control" id="subevents-{{$key}}-description" name="subevents[{{$key}}][description]" rows="6">{{$subevent->description or old('subevents.'.$key.'.description')}}</textarea>
                </div>
            </div>
            <!-- End Event Description -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="row field-hint">
                        <div class="col-sm-12">
                            <label for="subevents-{{$key}}-title">Facebook</label>
                            <input type="text" class="form-control" id="subevents-{{$key}}-title" name="subevents[{{$key}}][title]" value="{{$subevent->title or old('subevents.'.$key.'.title')}}">
                        </div>
                    </div>

                    <div class="row field-hint">
                        <div class="col-sm-12">
                            <label for="subevents-{{$key}}-title">Twitter</label>
                            <input type="text" class="form-control" id="subevents-{{$key}}-title" name="subevents[{{$key}}][title]" value="{{$subevent->title or old('subevents.'.$key.'.title')}}">
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div id="dropzonePreview" class="image-holder dz-click">
                        <a href="#" class="field-hint">
                            <div class="camera-shape">
                                <img src="{{ asset('images/camera.png') }}" alt="cover-image">
                            </div>
                            <p class="text-center blackish">Add image</p>
                        </a>
                    </div>
                </div>
            </div>


        </div>
        <!-- End Setting Box -->

        <div class="col-sm-6">

            <!-- Event Title Row -->
            <div class="row field-hint">
                <div class="col-sm-12">
                    <label for="subevents-{{$key}}-title">Speaker Name</label>
                    <input type="text" class="form-control" id="subevents-{{$key}}-title" name="subevents[{{$key}}][title]" value="{{$subevent->title or old('subevents.'.$key.'.title')}}">
                </div>
            </div>
            <!-- End Event Title -->

            <!-- Designation Row -->
            <div class="row field-hint">
                <div class="col-sm-12">
                    <label for="subevents-{{$key}}-title">Speaker Role</label>
                    <input type="text" class="form-control" id="subevents-{{$key}}-title" name="subevents[{{$key}}][title]" value="{{$subevent->title or old('subevents.'.$key.'.title')}}">
                </div>
            </div>
            <!-- End Event Title -->

            <!-- Event Description Row -->
            <div class="row field-hint">
                <div class="col-sm-12">
                    <label for="subevents-{{$key}}-description">About Speaker</label>
                    <textarea class="form-control" id="subevents-{{$key}}-description" name="subevents[{{$key}}][description]" rows="6">{{$subevent->description or old('subevents.'.$key.'.description')}}</textarea>
                </div>
            </div>
            <!-- End Event Description -->
        </div>
        <div class="clearfix"></div>
        <input type="hidden" name="subevents[{{$key}}][id]" value="{{$subevent->id or 0}}">
        <input type="hidden" name="subevents[{{$key}}][deleted]" value="{{$deleted or 0}}">
    </div>
