
    <!-- Section Level Row -->
    <div class="row">
        <div class="col-sm-12">
            <h3 class="text-left md-ubuntu-font">Event Schedule Plan</h3>
        </div>
    </div>

    <!-- Sub event row -->
    <div class="row relation-repeatable">

        <div class="relation-wrapper">
            <div class="relation-container">
                @php
                $count = 0;
                if(isset($event)) $count = count($event->children);
                @endphp
                @for($i = 0; $i < $count; $i++)
                    @include('dashboard.events.snippets.sub-event-single', [
                        'deleted' => 0,
                        'key' => $i,
                        'class' => 'subevent-template'
                    ])
                @endfor
                @include('dashboard.events.snippets.sub-event-single', [
                    'deleted' => 1,
                    'key' => "@{{key}}",
                    'class' => 'subevent-template relation-template'
                ])
            </div>
        </div>
    </div>
