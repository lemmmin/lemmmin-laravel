<section class="container section-gap">
    <div class="row">
        <div class="col-sm-offset-3 col-sm-6">
            <div class="no-pass-text alert alert-warning">Currently you have <span>no pass </span>.
                <br>You can look it from here. To buy ticket please browse through events and order tickets from there.
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-offset-4 col-sm-4">
            <a href="{{ route('browse') }}" role="button" class="btn btn-primary btn-slim btn-fullwidth">Browse events</a>
        </div>
    </div>
</section>
