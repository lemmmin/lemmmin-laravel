@extends('layouts.dashboard')

@section('title', 'My Passes')

@section('content')
    <section class="container">
        <div class="row">

            @if(count($passes))
            <div class="pass-list container-fluid">
                @foreach($passes as $pass)
                <div class="pass row">
                    <div class="col-sm-2">
                        <div class="pass-qr-code">
                            <img class="img-responsive" src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(100)->margin(1)->generate($pass->pass_number)) !!} ">
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <div class="pass-content-group">
                            <h2 class="pass-no">
                                <a href="{{route('passes.show', $pass->pass_number)}}">
                                    Pass#: {{$pass->pass_number}}
                                </a>
                            </h2>
                        </div>

                        <div class="pass-content-group">
                            <div class="ticket-info">
                                <p>
                                    <span class="hilight-text">Order# :</span>
                                    <strong>
                                        <a href="{{route('orders.show', $pass->order)}}">
                                            {{$pass->order->id}}
                                        </a>
                                    </strong>
                                </p>
                                @if(!is_null($pass->event))
                                    @php
                                    $eventStartDate = $pass->event->start_time_object->format('jS F Y');
                                    $eventEndDate = $pass->event->end_time_object->format('jS F Y');
                                    @endphp
                                    <p>
                                        <span class="hilight-text">Event: </span>
                                        <strong>
                                            <a href="{{route('events.show', $pass->event)}}">
                                                {{$pass->event->title}}
                                            </a>
                                        </strong>
                                    </p>
                                @endif
                                <p>
                                    <span class="hilight-text">Type: </span>
                                    <strong>{{$pass->ticket_name}}</strong>
                                </p>
                                <p>
                                    <span class="hilight-text">Issued: </span>
                                    <strong>{{$pass->created_at->timezone('Asia/Dhaka')->toDayDateTimeString()}}</strong>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                @endforeach
            </div>
            <div class="text-center">
                {{ $passes->links() }}
            </div>
            @else
            @include('dashboard.passes.snippets.no-pass')
            @endif
        </div>
    </section>
@endsection
