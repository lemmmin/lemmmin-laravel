@extends('layouts.dashboard')

@section('title', !is_null($event) ? $event->title . ' - ' . $pass->ticket_name : $pass->ticket_name)

@section('content')
@php
if(!is_null($event)){
    // Event start and end time format
    $eventStartDate = $event->start_time_object->format('jS F Y');
    $eventEndDate = $event->end_time_object->format('jS F Y');
}
else {
    $eventStartDate = '';
    $eventEndDate = '';
}
@endphp

<section class="container pass-container">
    <div class="row">
        <div class="col-sm-12">
            <div class="pass-info">

                <div class="pass-content-group">
                    <label class="pass-label">Name</label>
                    <div class="pass-user-name">
                        {{$pass->user->name}}
                    </div>
                </div>

                <div class="pass-content-group">
                    <label class="pass-label">Email</label>
                    <div class="pass-user-email">
                        {{$pass->user->email}}
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-7">
            <div class="pass-info">

                @if(!is_null($event))
                <div class="pass-content-group">
                    <label class="pass-label">Event Name</label>
                    <div class="pass-event-title">
                        <a href="{{route('events.show', $event)}}">
                            {{$event->title}}
                        </a>
                    </div>
                    @if(!is_null($event->organizer))
                    <div class="pass-event-organizer">
                        <strong class="hilight-text">Organized By, </strong>
                        <a href="{{route('organizers.show',$pass->event->organizer)}}">
                            {{$pass->event->organizer->name}}
                        </a>
                    </div>
                    @endif
                </div>
                <div class="pass-content-group">
                    <label class="pass-label">Event Information</label>
                    <div class="event-info">

                        <p >{!! $event->meta('location_address') !!}</p>
                        <p>
                            @if($eventStartDate == $eventEndDate)
                            {{$event->start_time_object->format('l')}}, {{$eventStartDate}} <br />
                            {{$event->start_time_object->format('h:i A')}} To {{$event->end_time_object->format('h:i A')}}
                            @else
                            {{$event->start_time_object->format('l')}}, {{$eventStartDate}} {{$event->start_time_object->format('h:i A')}}<br />
                            To,
                            {{$event->end_time_object->format('l')}}, {{$eventEndDate}} {{$event->end_time_object->format('h:i A')}}
                            @endif
                        </p>
                    </div>
                </div>
                @endif
                <div class="pass-content-group">
                    <label class="pass-label">Ticket Information</label>
                    <div class="ticket-info">
                        <p>
                            <span class="hilight-text">Order# :</span>
                            <a href="{{route('orders.show', $pass->order)}}">
                                {{$pass->order->id}}
                            </a>
                        </p>
                        <p><span class="hilight-text">Ticket: </span><strong>{{$pass->ticket_name}}</strong></p>
                        <p><span class="hilight-text">Issued: </span><strong>{{$pass->created_at->timezone('Asia/Dhaka')->toDayDateTimeString()}}</strong></p>
                    </div>
                </div>
                
                @php
                $pass_details = unserialize($pass->meta('form_data'));
                @endphp
                @if(!empty($pass_details))
                <div class="pass-content-group">
                    <label class="pass-label">Pass Details</label>
                    <div class="pass-details">
                        @foreach($pass_details as $name => $value)
                            <p>
                                <span class="hilight-text">{{$name}}: </span>
                                <strong>{{$value}}</strong>
                            </p>
                        @endforeach
                    </div>
                </div>
                @endif

            </div>
        </div>
        <div class="col-sm-5">
            <div class="pass-qr-code">
                <img class="img-responsive" src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(200)->margin(1)->generate($pass->pass_number)) !!} ">
            </div>
            <div class="pass-id">Pass#: {{$pass->pass_number}}</div>
        </div>
    </div>
</section>

@endsection
