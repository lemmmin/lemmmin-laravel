<tr id="organizer-{{$organizer->id}}">
        <td class="organizer-name-list col-xs-10">
            <a href="{{route('organizers.show', $organizer->id)}}">
                {{$organizer->name}}
            </a>
        </td>
        <td class="organizer-edit col-xs-1">
            <a role="button" href="{{route('organizers.edit', $organizer->id)}}">
                <i class="fa fa-pencil"></i>
            </a>
        </td>
        @can('delete', $organizer)
        <td class="organizer-delete col-xs-1">
            <form  action="{{route('organizers.destroy', $organizer->id)}}" method="post">
                <button type="submit">
                    <i class="fa fa-trash-o"></i>
                </button>
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
            </form>
        </td>
        @endcan
    </tr>
