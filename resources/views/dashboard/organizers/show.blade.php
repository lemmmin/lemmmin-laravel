@extends('layouts.dashboard')

@section('title', 'About Organizer')

@section('content')

<section class="container section-gap">
    <div class="row">
        <div class="col-sm-12">
            <div class="sin-evn-h3">
                <h3>Organizer Information</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-offset-3 col-sm-6">
            <div class="organizer-name">
                <a href="{{route('organizers.show', $organizer)}}">
                    {{$organizer->name}}
                </a>
            </div>
            <div class="organizer-about-panel">
                <p class="organizer-about">{{$organizer->about}}</p>
            </div>
            <div class="social-address-link">
                <h5 class="social-address-headline">Social Address</h5>
                @if(($organizer->meta('social_facebook')))
                <a href="{{$organizer->meta('social_facebook')}}" data-toggle="tooltip" data-placement="top" title="{{$organizer->meta('social_facebook')}}"><i class="fa fa-facebook"></i></a>
                @endif
                @if(($organizer->meta('social_twitter')))
                <a href="{{$organizer->meta('social_twitter')}}" data-toggle="tooltip" data-placement="top" title="{{$organizer->meta('social_twitter')}}"><i class="fa fa-twitter"></i></a>
                @endif
                @if(($organizer->meta('website')))
                <a href="{{$organizer->meta('website')}}" data-toggle="tooltip" data-placement="top" title="{{$organizer->meta('website')}}"><i class="fa fa-globe"></i></a>
                @endif
                @if(($organizer->meta('email_address')))
                <a href="{{$organizer->meta('email_address')}}" data-toggle="tooltip" data-placement="top" title="{{$organizer->meta('email_address')}}"><i class="fa fa-envelope"></i></a>
                @endif
                @if(($organizer->meta('contact_home')))
                <a href="#" data-toggle="popover" data-placement="bottom" data-content="{{$organizer->meta('contact_home')}}"><i class="fa fa-mobile"></i></a>
                @endif
                @if(($organizer->meta('contact_work')))
                <a href="#" data-toggle="popover" data-placement="bottom" data-content="{{$organizer->meta('contact_work')}}"><i class="fa fa-phone"></i></a>
                @endif
            </div>
        </div>
    </div>
</section>

<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="sin-evn-h3">
                <h3>Event List</h3>
            </div>
        </div>
    </div>

    @if(count($events))
    <div class="row">
        @each('frontend.snippets.event', $events, 'event')
    </div>

    <div class="text-center">
    {{ $events->links() }}
    </div>
    @else
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-warning text-center">
                Not enough data available at this moment!
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-sm-offset-4 col-sm-4">
            <a href="{{route('organizers.create')}}" class="btn btn-slim btn-primary btn-fullwidth">Create New</a>
        </div>
    </div>
</section>
@endsection
