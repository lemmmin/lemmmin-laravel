@extends('layouts.dashboard')

@section('title', 'Edit Organizer')

@section('content')

<form class="form" action="{{route('organizers.update', $organizer->id)}}" method="post">

    @if (count($errors) > 0)
    <section class="container section-gap">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
    @endif
    <!-- General Information about Event -->
    <section class="container section-gap">
        <div class="row">
            <div class="col-sm-12">

                    <!-- General Information about Organizer -->
                    <div class="row">
                        <div class="col-md-4">

                            <!-- Heading -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="organizer-info-heading">
                                        General Information
                                    </div>
                                </div>
                            </div>

                            <!-- Name -->
                            <div class="row field-hint">
                                <div class="col-sm-12">
                                    <label for="name">Organizer Name</label>
                                    <input type="text" class="form-control" name="name" id="name" value="{{ isset($organizer)? $organizer->name : ''}}">
                                </div>
                            </div>
                            <!-- End -->

                            <!-- About -->
                            <div class="row field-hint">
                                <div class="col-sm-12">
                                    <label for="about">About Organizer</label>
                                    <textarea rows="6" type="text" class="form-control" name="about" id="about">{{isset($organizer)? $organizer->about : ''}}</textarea>
                                </div>
                            </div>
                            <!-- End-->
                        </div>

                        <!-- Social link -->
                        <div class="col-sm-6 col-md-4" >

                            <!--Heading -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="organizer-info-heading-small">
                                        Social Network links
                                    </div>
                                </div>
                            </div>
                            <!-- End -->

                            <!-- Website Link -->
                            <div class="row field-hint">
                                <div class="col-sm-12">
                                    <label for="website">Website</label>
                                    <input type="text" class="form-control" name="meta[website]" id="website" value="{{ isset($organizer)? $organizer->meta('website') : ''}}">
                                </div>
                            </div>
                            <!-- End -->

                            <!-- Facebook Link -->
                            <div class="row field-hint">
                                <div class="col-sm-12">
                                    <label for="facebook">Facebook</label>
                                    <input type="text" class="form-control" name="meta[social_facebook]" id="facebook" value="{{isset($organizer)? $organizer->meta('social_facebook') : ''}}">
                                </div>
                            </div>
                            <!-- End -->

                            <!-- Twitter -->
                            <div class="row field-hint">
                                <div class="col-sm-12">
                                    <label for="twitter">Twitter</label>
                                    <input type="text" class="form-control" name="meta[social_twitter]" id="twitter" value="{{isset($organizer)? $organizer->meta('social_twitter') : ''}}">
                                </div>
                            </div>
                            <!-- End -->
                        </div>

                        <!-- Other Settings -->
                        <div class="col-sm-6 col-md-4">

                            <!-- Colors -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="organizer-info-heading-small">
                                        Contact Info
                                    </div>
                                </div>
                            </div>

                            <!-- Contact No Home-->
                            <div class="row field-hint">
                                <div class="col-sm-12">
                                    <label for="text">Home</label>
                                    <input placeholder="format:+880-1XXX-XXX-XXX" type="text" class="form-control" name="meta[contact_home]" id="contact_home" value="{{isset($organizer)? $organizer->meta('contact_home') : ''}}">
                                </div>
                            </div>
                            <!-- End -->

                            <!-- Contact No -->
                            <div class="row field-hint">
                                <div class="col-sm-12">
                                    <label for="text">Work</label>
                                    <input placeholder="format:+880-1XXX-XXX-XXX" type="text" class="form-control" name="meta[contact_work]" id="contact_work" value="{{isset($organizer)? $organizer->meta('contact_work') : ''}}">
                                </div>
                            </div>
                            <!-- End -->

                            <!-- Email Address -->
                            <div class="row field-hint">
                                <div class="col-sm-12">
                                    <label for="background">Email Address</label>
                                    <input type="text" class="form-control" name="meta[email_address]" id="email_address" value="{{isset($organizer)? $organizer->meta('email_address') : ''}}">
                                </div>
                            </div>
                            <!-- End -->

                        </div>
                    </div>

            </div>
        </div>
    </section>

    <section class="container section-gap">
        <div class="row">
            <div class="create-event-button">
                <div class="col-sm-offset-2 col-sm-4">
                    <button name="update_organizer" type="submit" class="btn btn-primary">Update</button>
                </div>
                <div class="col-sm-4">
                    <a role="button" href="{{route('organizers.index')}}" class="btn btn-default">Cancel</a>
                </div>

            </div>
        </div>
    </section>
    {{ csrf_field() }}
    {{ method_field('PUT') }}
</form>
@endsection
