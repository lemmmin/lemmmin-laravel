@extends('layouts.dashboard')

@section('title', 'All My Organizers')

@section('content')



<section class="container section-gap">
    <div class="row">
        <div class="col-sm-12">

            @cannot('create', \App\Organizer::class)
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-warning">You have created maximum number of organizers allowed.</div>
                </div>
            </div>
            @endcannot

            <div class="row">
                <div class="col-sm-12">
                    @if(count($organizers))
                    <div class="single-org-list">
                        <table class="table" id="organizers">
                            <thead>
                                <tr>
                                    <th class="col-xs-10">Organizer</th>
                                    <th class="col-xs-1">Edit</th>
                                    <th class="col-xs-1">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @each('dashboard.organizers.snippets.organizer', $organizers, 'organizer')
                            </tbody>
                        </table>
                        @else
                        @include('dashboard.organizers.snippets.no-organizers')
                        @endif
                    </div>
                </div>
            </div>

            @can('create', \App\Organizer::class)
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                    <a href="{{route('organizers.create')}}" class="btn btn-primary btn-slim btn-fullwidth">Create New</a>
                </div>
            </div>
            @endcan

            <div class="clearfix"></div>
            {{ $organizers->links() }}

        </div>
    </div>
</section>

@endsection
