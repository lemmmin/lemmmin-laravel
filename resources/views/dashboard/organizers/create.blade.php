@extends('layouts.dashboard')

@section('title', 'Create Organizer')

@section('content')

    <form class="form" action="{{route('organizers.store')}}" method="post">

        @if (count($errors) > 0)
        <section class="container section-gap">
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $message)
                            <li>{{$message}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        @endif
        <!-- General Information about Event -->
        <section class="container section-gap">
            <div class="row">
                <div class="col-sm-12">
                    @include('dashboard.organizers.snippets.org-general-info')
                </div>
            </div>
        </section>

        <section class="container section-gap">
            <div class="row">
                <div class="create-event-button">
                    <div class="col-sm-offset-2 col-sm-4 ">
                        <button name="create-organizer"type="submit" class="btn btn-primary">Create</button>
                    </div>
                    <div class="col-sm-4">
                        <a role="button" href="{{route('organizers.index')}}" class="btn btn-default">Cancel</a>
                        {{ csrf_field() }}
                    </div>

                </div>
            </div>
        </section>
        {{ csrf_field() }}
    </form>
@endsection
