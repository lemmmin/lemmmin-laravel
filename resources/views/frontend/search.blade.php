@extends('layouts.dashboard')

@section('title', sprintf("Search Results For: %s", Input::get('search')))

@section('content')

<section class="container section-gap">
    <div class="row">
        <div class="col-sm-12">
            @if(count($events))
            <div class="row">
                @each('frontend.snippets.event', $events, 'event')
            </div>
            <div class="text-center">
                {{ $events->links() }}
            </div>
            @else
            Not enough data available at this moment!
            @endif
        </div>
        <div class="row">
            <div class="col-sm-offset-4 col-sm-4">
                <a href="{{route('events.create')}}" class="btn btn-slim btn-primary btn-fullwidth">Create New</a>
            </div>
        </div>
    </div>
</section>
@endsection
