@php
$event = (object) $event;
@endphp


<div class="col-sm-4">
    <div id="event-{{$event->id}}">
        <div class="single-event-card">

            <div class="single-event-card-cover-image">
                <a href="{{route('events.show', $event->id)}}">
                    <img class="img-responsive" src="{{$event->thumbnail}}" alt="Cover Image">
                </a>
            </div>
            <div class="single-event-card-short-data">
                <div class="row">
                    <div class="col-xs-2">
                        <div class="single-event-card-date-box">
                            {!! $event->start_time_object->format('<p>j <b\r />M</p>') !!}
                        </div>
                    </div>
                    <div class="col-xs-10">
                        <div class="single-event-card-heading">
                            <h4 >
                                <a href="{{route('events.show', $event->id)}}">
                                    @if(strlen($event->title) < 30)
                                    {{$event->title}}
                                    @else
                                    {{mb_substr($event->title,0,30,'UTF-8')}} . . .
                                    @endif
                                </a>
                            </h4>
                            @if($event->organizer != null)
                                <p >Organized By <a href="{{route('organizers.show', $event->organizer)}}">{{$event->organizer->name}}</a></p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-offset-2 col-xs-10">
                        <div class="single-event-card-description">
                            <p>
                                @if(strlen($event->description) < 100)
                                    {{$event->description}}
                                @else
                                    {{mb_substr($event->description,0,100,'UTF-8')}} . . .
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-event-card-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="single-event-card-footer-privacy">
                            {{$event->privacy}}
                        </div>

                        <div class="single-event-card-footer-share">
                            <a class="fa fa-share-alt addthis_button_compact"
                            addthis:url="{{route('events.show', $event)}}"
                            addthis:title="{{$event->title}}"></a>
                        </div>

                        <div class="single-event-card-footer-bookmark">
                            <a href="{{route('events.bookmark', $event)}}" class="fa {{$event->is_bookmarked ? 'fa-bookmark' : 'fa-bookmark-o'}} save_event"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
