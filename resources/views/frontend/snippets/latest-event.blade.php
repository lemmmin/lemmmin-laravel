@php
$event = (object) $event;

$eventStartDate = $event->start_time_object->format('jS F Y');

@endphp

<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-5">
            <img class="img-responsive" src="{{$event->cover_image}}" alt="cover image">
        </div>
        <div class="col-sm-7">
            <h4>$event->title</h4>
            <div class="row">
                <div class="col-xs-4">
                    <p>$eventStartDate</p>
                </div>
                <div class="col-xs-4">
                    <p>$event->$ticket->price</p>
                </div>
                <div class="col-xs-4">
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="{{route('events.bookmark', $event)}}" class="fa {{$event->is_bookmarked ? 'fa-heart' : 'fa-heart-o'}} save_event"></a>
                        </div>
                        <div class="col-xs-6">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="single-event-card-description">
                        <p>
                            @if(strlen($event->description) < 300)
                                {{$event->description}}
                            @else
                                {{substr($event->description,0,300)}} . . .
                            @endif
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <button class="btn btn-primary btn-slim btn-fullwidth" type="submit" name="button">Buy Tickets</button>

                </div>
                <div class="col-sm-6">
                    <button class="btn btn-primary btn-slim btn-fullwidth" type="submit" name="button">View Details</button>
                </div>
            </div>
        </div>
    </div>
</div>
