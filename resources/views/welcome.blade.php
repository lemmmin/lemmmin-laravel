@extends('layouts.app')

@section('content')


<!-- Heading Section -->
<section class="container-fluid nav-gap home-head-bg">
    <div class="row overlay-container">
        <div class="container">
            <div class="home-head-container">
                <div class="home-head-content">
                    <div class="row">
                <div class="col-sm-8">
                    <h1>Lemmmin, An Event And Ticket Management Platform</h1>
                    <p>It's easy and free to host an event.</p>
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{route('events.create')}}" role="button" class="btn btn-primary btn-fullwidth">Create event</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="mobile-img-content">
                        <div class="img-front">
                            <img src="" alt="">
                        </div>
                        <div class="img-back">
                            <img src="" alt="">
                        </div>
                    </div>
                </div>
            </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Search Section -->
<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <form method="get" action="{{route('search')}}" class="home-search-loc-container">
                <div class="home-search-loc-content">
                    <div class="home-search-box-select">
                        <span class="select-caret"><i class="fa fa-caret-down"></i></span>
                        <select class="" name="sort">
                            <option value="nearby">Nearby Events</option>
                            <option value="latest">Newest Events</option>
                        </select>
                    </div>
                    <div class="home-search-box-input-field">
                        <input placeholder="Search with keywords" type="text" name="q" value="">
                        <span class="home-search-box-icon">
                            <i class="fa fa-search"></i>
                        </span>
                    </div>
                </div>
                <input type="hidden" name="latlng">
            </form>
        </div>
    </div>
</section>

<!-- Latest Event Section -->
<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="featured-event-heading">Latest Events</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            @if(count($recommended_events) > 0)
            <div class="row">
                @each('frontend.snippets.event', $recommended_events, 'event')
            </div>
            @endif
        </div>
    </div>
</section>

<!-- Subscribe Section -->
<section class="container-fluid home-subscribe-bg">
    <div class="row">
        <div class="col-xs-12">
            <div class="container">
                <div class="home-subscribe-container">
                    <div class="home-subscribe-content">

                        <h3>Subscribe for Newsletter</h3>
                        <p>we are always working hard to making best product for you, feel & enjoy</p>

                        <form id="mailchimp-subscribe" class="mailchimp-subscribe">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="home-subscription" style="display:none"></div>
                                </div>
                                <div class="col-sm-6">
                                    <input id="subscriber-name" class="form-control" placeholder="Your Name" type="text" name="NAME" value="">
                                </div>
                                <div class="col-sm-6">
                                    <input id="subscriber-email" class="form-control" placeholder="Your Email" type="text" name="EMAIL" value="">
                                </div>
                            </div>

                            <div class="row ">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <button class="btn btn-primary btn-slim btn-fullwidth" type="submit" name="button">Subscribe</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Home Auth Section -->
<section class="container-fluid home-auth-bg">
    <div class="row">
        <div class="col-xs-12">
            <div class="container">
                <div class="home-auth-container">
                    <div class="home-auth-content">

                        <h3>Are you ready to explore our wonderful things</h3>
                        <p>we are always working hard to making best product for you, feel & enjoy</p>

                        @if (Auth::guest())
                        <div class="row">
                            <div class="home-auth-content-button">
                                <a href="{{ url('/social/facebook') }}" class="btn-slim"><img  src="{{ asset('images/fb-button.png') }}" alt=""></a>
                                <a href="{{ url('/social/google') }}" class="btn-slim"><img  src="{{ asset('images/google-dark.png')}}" alt=""></a>
                            </div>
                        </div>
                        @else
                        <div class="row ">
                            <div class="col-sm-offset-3 col-sm-6">
                                <a href="{{ route('browse') }}" role="button" class="btn btn-default btn-slim btn-fullwidth">Browse events</a>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script type="text/javascript" src="{{asset('/js/jquery.ajaxchimp.min.js')}}"></script>
@endpush
