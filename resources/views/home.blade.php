@extends('layouts.app')

@section('hidden-element', 'hidden')

@section('content')
<section class="container-fluid nav-gap">
    <div class="row">
        <div class="col-sm-12 home-head-container">
            <div class="home-head-content">
                <h1>Lemmmin Ticket</h1>
                <p>Ticket is on your hand now.</p>
                <a href="#get-tickets" role="button" class="btn btn-primary">Create event</a>
            </div>
        </div>
    </div>
</section>

@endsection
