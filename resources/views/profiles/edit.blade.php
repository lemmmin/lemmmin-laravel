@extends('layouts.dashboard')

@section('title', 'User Profile')

@section('content')

<form class="form user-profile" action="{{route('profiles.update', $user->id)}}" method="post">

    @if (count($errors) > 0)

    <section class="container section-gap">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>

    @endif

    @if(session()->has('info'))
    <section class="container section-gap">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-info">
                    {{session('info')}}
                </div>
            </div>
        </div>
    </section>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-sm-2 avatar-container hidden-sm hidden-xs">
                <img src="{{$user->avatar(200)}}" alt="Avatar" class="img-responsive avatar">
            </div>
            <div class="col-sm-12 col-md-10">
                <h2>General Information</h2>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="user-name">Full Name</label>
                        <input class="form-control" type="text" name="name" id="user-name" value="{{old('name', $user->name)}}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="user-email">Email</label>
                        <input class="form-control" type="email" name="email" id="user-email" value="{{old('email', $user->email)}}" {{Gate::denies('change_email', $user) ? 'readonly': ''}} required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="user-phone">Phone</label>
                        <input class="form-control" type="text" name="phone" id="user-phone" value="{{old('phone', $user->meta('phone'))}}" required>
                    </div>
                </div>

                <h2>Shipping Address</h2>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="address-line-1">Address Line 1</label>
                        <input class="form-control" type="text" name="address_line_1" id="address-line-1" value="{{old('address_line_1', $user->meta('address_line_1'))}}">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="address-line-2">Address Line 2</label>
                        <input class="form-control" type="text" name="address_line_2" id="address-line-2" value="{{old('address_line_2', $user->meta('address_line_2'))}}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-4">
                        <label for="city">City</label>
                        <input class="form-control" type="text" name="city" id="city" value="{{old('city', $user->meta('city'))}}">
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="post-code">Post Code</label>
                        <input class="form-control" type="text" name="post_code" id="post-code" value="{{old('post_code', $user->meta('post_code'))}}">
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="district">District</label>
                        <input class="form-control" type="text" name="district" id="district" value="{{old('district', $user->meta('district'))}}">
                    </div>
                </div>

                <h2>Change Password</h2>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="password-current">Current Password</label>
                        <input class="form-control" type="password" name="old_password" id="password-current">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="password-new">New Password</label>
                        <input class="form-control" type="password" name="new_password" id="password-new">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="password-repeat">Repeat New Password</label>
                        <input class="form-control" type="password" name="new_password_confirmation" id="password-repeat">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>

    {{ csrf_field() }}

</form>
@endsection


@push('scripts')
<script type="text/javascript" src="{{asset('/js/jquery.uploadPreview.min.js')}}"></script>
@endpush
