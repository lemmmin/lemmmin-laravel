# TaoTao Ticket Booking
A service to sell event tickets. Create your event, promote it and we will take
care of the rest.

## Specifications
- Laravel Version: **5.3**

## Installation
- Make directory 'public' the webroot.
- Run `npm install`.
- Run `composer install`.
- Edit **.env** and make required changes to configure.
- Import migration tables by running `php artisan migrate`.
- Start the server by `php artisan serve` or any other means.
- Generate API credentials for Facebook and Google and put them in .env for social auth to work.
- Run `php artisan passport:client --password` and configure mobile client to use the newly created client.
- Run `php artisan passport:client --personal` to start generating tokens.
- Start laravel queue worker process by `php artisan queue:work` or configure supervisor on linux.
