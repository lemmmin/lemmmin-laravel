<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Plank\Mediable\Mediable;
use Carbon\Carbon;
use Laravel\Scout\Searchable;
use App\Event;
use App\Permission;
use Auth;
use LemmminHelper;

class Event extends Model
{
    use Mediable, Metabble, Searchable;


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'live' => 'boolean',
    ];


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'location_name',
        'location_address',
        'location_latitude',
        'location_longitude',
        '_geoloc',
        'online_event',
        'cover_image',
        'thumbnail',
        'has_image',
        'is_bookmarked',
        'permissions',
        'max_ticket_purchase',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'start_time',
        'end_time',
        'summary',
        'description',
        'privacy',
        'live',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('root', function(Builder $builder) {
            $builder->whereNull('parent');
        });
    }

    /**
     * Get the owner of the event
     */
    public function owner()
    {
    	return $this->belongsTo('App\User', 'owner_id');
    }

    /**
     * Get the organizer of the event
     */
    public function organizer()
    {
    	return $this->belongsTo('App\Organizer', 'organizer_id');
    }

    /**
     * Get the child events of this event
     */
    public function children()
    {
        return $this->hasMany('App\Event', 'parent')->withoutGlobalScope('root');
    }

    /**
     * Get the parent event of this event
     */
    public function parent()
    {
        return $this->belongsTo('App\Event', 'parent');
    }

    /**
     * Get tickets associated with this event
     */
    public function tickets()
    {
        return $this->belongsToMany('App\Ticket');
    }

    /**
     * Get passes associated with this event
     */
    public function passes()
    {
        return $this->hasMany('App\Pass');
    }

    /**
     * Get guests of this event
     */
    public function guests()
    {
        return $this->belongsToMany('App\User', 'passes');
    }

    /**
     * Get list of form fields for this event
     */
    public function form_fields()
    {
        return $this->hasMany('App\FormField');
    }

    /**
     * List of orders associated with this event.
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    /**
     * Find all users that have any permission on this event.
     */
    public function permittedUsers()
    {
        return $this->morphToMany('App\User', 'permissionable');
    }

    /**
     * Find users who have a particular permission on this event.
     */
    public function findUsersByPermission($permission)
    {
        return $this->permittedUsers()
                    ->wherePivot('permission', $permission)
                    ->wherePivot('granted', 1);
    }

    /**
     * Get latitude of a location.
     */
    public function getLocationNameAttribute()
    {
        return $this->meta('location_name');
    }

    /**
     * Get latitude of a location.
     */
    public function getLocationAddressAttribute()
    {
        return $this->meta('location_address');
    }

    /**
     * Get latitude of a location.
     */
    public function getLocationLatitudeAttribute()
    {
        return $this->meta('location_latitude');
    }

    /**
     * Get longitude of a location.
     */
    public function getLocationLongitudeAttribute()
    {
        return $this->meta('location_longitude');
    }

    /**
     * Get longitude of a location.
     */
    public function getOnlineEventAttribute()
    {
        return $this->meta('online_event') == 1;
    }

    public function getCoverImageAttribute()
    {
        if($this->hasMedia('cover_image')){
            $media = $this->firstMedia('cover_image');

            return asset('/images/original/'.$media->basename);
        }

        return asset('/images/original/placeholder.jpg');
    }

    public function getThumbnailAttribute()
    {
        if($this->hasMedia('cover_image')){
            $media = $this->firstMedia('cover_image');

            return asset('/images/large/'.$media->basename);
        }

        return asset('/images/large/placeholder.jpg');
    }

    public function getHasImageAttribute()
    {
        return $this->hasMedia('cover_image');
    }

    public function getStartTimeObjectAttribute()
    {
        return new Carbon($this->start_time);
    }

    public function getEndTimeObjectAttribute()
    {
        return new Carbon($this->end_time);
    }

    public function getGeolocAttribute()
    {
        return [
            'lat' => (float) $this->location_latitude,
            'lng' => (float) $this->location_longitude,
        ];
    }

    public function getIsBookmarkedAttribute()
    {
        if(!Auth::check() && !Auth::guard('api')->check()) return false;

        if(Auth::check()) $user = Auth::user();
        else $user = Auth::guard('api')->user();

        return $user->saved_events()->where('event_id', $this->id)->count() > 0;
    }

    /**
     * Get a list of permission assigned to current user.
     */
    public function getPermissionsAttribute()
    {
        if(Auth::check()) $user = Auth::user();
        else $user = Auth::guard('api')->user();

        // if(is_null($user)) return [];

        $tasks = [
            'view',
            'see_guests',
            'create',
            'update',
            'delete',
        ];

        $permissions = [];
        foreach($tasks as $task){
            $permissions[$task] = $user->can($task, $this);
        }

        return $permissions;
    }

    /**
     * Get maximum number of tickets that can be purchased by a single user.
     */
    public function getMaxTicketPurchaseAttribute()
    {
      return $this->meta('max_ticket_purchase_quantity', 0) == 0 ? LemmminHelper::$max_ticket_per_order : $this->meta('max_ticket_purchase_quantity');
    }

    /**
     * Get maximum number of tickets that can be purchased by current user.
     */
    public function getUserMaxTicketPurchaseAttribute()
    {
      if(!Auth::check()) return $this->max_ticket_purchase;

      return $this->max_ticket_purchase - $this->passes()->where('user_id', Auth::id())->count();
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        $array['created_at'] = strtotime($array['created_at']);
        $array['updated_at'] = strtotime($array['updated_at']);
        $array['start_time'] = strtotime($array['start_time']);
        $array['end_time'] = strtotime($array['end_time']);

        return $array;
    }

    /**
     * Strip event from all the dependents
     */
    public function deleteRelatives()
    {
        foreach($this->children()->get() as $child){
            $child->deleteRelatives();
            $child->delete();
        }

        $tickets = $this->tickets()->get();

        foreach($tickets as $ticket){
            $this->tickets()->detach($ticket->id);

            if(count($ticket->events()) > 0)
                Ticket::destroy($ticket->id);
        }
    }
}
