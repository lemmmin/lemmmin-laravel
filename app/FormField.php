<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormField extends Model
{
    /**
     * List of columns that are mass assignable.
     */
    public $fillable = [
      'title',
      'type',
      'hint',
      'options',
      'required',
    ];

    /**
     * Find the event associated with this field.
     */
    public function event()
    {
        return $this->belongsTo('App\Event');
    }
}
