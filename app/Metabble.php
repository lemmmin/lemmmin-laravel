<?php

namespace App;

use App\Meta;

trait Metabble
{
    /**
     * Get all of the entity's meta
     */
    public function meta($key = false, $multi = false)
    {
        if($key) {
            $type = get_class($this);
            $meta = Meta::where('entity_type', $type)
                        ->where('entity_id', $this->id)
                        ->where('key', $key);

            if(!$multi) return $meta->value('value');
            else return $meta->pluck('value');
        }

        return $this->morphMany('App\Meta', 'entity');
    }

    /**
     * Add new meta
     */
    public function add_meta($key, $value)
    {
        if(!$value) return;
        $this->meta()->save(new Meta(['key' => $key, 'value' => $value]));
    }

    /**
     * Update meta data for a specific key
     */
    public function update_meta($key, $value)
    {
        $type = get_class($this);
        $meta = Meta::where('entity_type', $type)
                    ->where('entity_id', $this->id)
                    ->where('key', $key)
                    ->first();

        if(is_null($meta)){
            $this->add_meta($key, $value);
        }
        else {
            if($value){
                $meta->value = $value;
                $meta->save();
            }
            else {
                $meta->delete();
            }
        }
    }

    /**
     * Delete a meta data.
     */
    public function delete_meta($key, $value = null)
    {
      $type = get_class($this);

      $meta = Meta::where('entity_type', $type)
                  ->where('entity_id', $this->id)
                  ->where('key', $key);
      if(!is_null($value)) $meta = $meta->where('value', $value);

      $meta->delete();
    }
}
