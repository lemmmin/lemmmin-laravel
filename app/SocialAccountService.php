<?php

namespace App;

use laravel\Socialite\Contracts\User as ProviderUser;

class SocialAccountService
{

    /**
     * Create or get existing user from social auth's $user object.
     *
     * @param ProviderUser $providerUser.
     * @param string $provider Name of provider.
     * @return User $user
     */
    public function getSocialUser(ProviderUser $providerUser, $provider)
    {
        $account  = SocialAccounts::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if($account){
            return $account->user;
        }
        else {
            $account = new SocialAccounts([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider,
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if( !$user ){
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }
}
