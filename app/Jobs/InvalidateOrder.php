<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;

use App\Order;

class InvalidateOrder implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->order->status != 'pending' && $this->order->status != 'pending_form') return;

        foreach($this->order->quantities as $order_row){
            DB::table('tickets')
                ->where([
                    ['id', $order_row['ticket_id']],
                ])
                ->decrement('issued', $order_row['quantity']);
        }

        $this->order->passes()->delete();

        $this->order->status = 'expired';
        $this->order->save();
    }
}
