<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use Metabble;

    /**
     * Fields that are publicly fillable.
     */
    protected $fillable = [
        'referance',
        'amount',
        'currency',
        'gateway',
        'order_id',
        'user_id',
        'note',
        'status',
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
