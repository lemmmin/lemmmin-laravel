<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderQuantity extends Model
{
    /**
     * Fields that are publicly fillable.
     */
    protected $fillable = [
        'order_id',
        'ticket_id',
        'ticket_name',
        'event_id',
        'quantity',
        'price',
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }

    public function event()
    {
        return $this->belongsTo('App\Event');
    }
}
