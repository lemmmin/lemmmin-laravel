<?php

namespace App\Providers;

use App\Event;
use App\Policies\EventPolicy;
use App\User;
use App\Policies\UserPolicy;
use App\Organizer;
use App\Policies\OrganizerPolicy;
use App\Ticket;
use App\Policies\TicketPolicy;
use App\Order;
use App\Policies\OrderPolicy;
use App\Pass;
use App\Policies\PassPolicy;
use App\FormField;
use App\Policies\FormFieldPolicy;

use Laravel\Passport\Passport;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Event::class => EventPolicy::class,
        Organizer::class => OrganizerPolicy::class,
        Organizer::class => OrganizerPolicy::class,
        Ticket::class => TicketPolicy::class,
        Order::class => OrderPolicy::class,
        Pass::class => PassPolicy::class,
        User::class => UserPolicy::class,
        FormField::class => FormFieldPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        Passport::routes();
    }
}
