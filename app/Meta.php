<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
	protected $dates = ['updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['key', 'value'];


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'meta';


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * Get all of the owning imageable models.
     */
    public function entity()
    {
        return $this->morphTo();
    }
}
