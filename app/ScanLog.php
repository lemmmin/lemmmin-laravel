<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScanLog extends Model
{
    public $timestamps = ['created_at'];
    protected $fillable = ['user_id', 'pass_id', 'action'];

    public function setUpdatedAtAttribute($value)
    {
        // to Disable updated_at
    }

}
