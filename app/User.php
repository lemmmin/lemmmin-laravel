<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, Metabble;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get events owned by the user
     */
    public function events()
    {
        return $this->hasMany('App\Event', 'owner_id');
    }

    /**
     * Get organizers owned by the user
     */
    public function organizers()
    {
        return $this->hasMany('App\Organizer', 'owner_id');
    }

    /**
     * Get orders that are placed by this user.
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    /**
     * Get all the passes of this user.
     */
    public function passes()
    {
        return $this->hasMany('App\Pass');
    }

    /**
     * Get all the transactions of this user.
     */
    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    /**
     * Get list of event this use has any permission in.
     */
    public function permittedEvents()
    {
        return $this->morphedByMany('App\Event', 'permissionable');
    }

    public function grantEventPermission($event, $permission)
    {
        $this->permittedEvents()->save($event, ['permission' => $permission]);
    }

    /**
     * Find events where this user has a particular permission.
     */
    public function findEventsByPermission($permission)
    {
        return $this->permittedEvents()
                    ->wherePivot('permission', $permission)
                    ->wherePivot('granted', 1);
    }

    /**
     * Get a users saved events.
     */
    public function saved_events()
    {
        return $this->belongsToMany('App\Event', 'bookmarks');
    }

    /**
     * Get link to the user's avatar.
     */
    public function avatar($size = 300)
    {
        $url = 'https://www.gravatar.com/avatar/';
        $email = $this->email;

        $hash = md5( strtolower( trim( $email ) ) );

        $grav_url = $url . $hash . "?s=" . $size . '&d=mm';

        return $grav_url;
    }
}
