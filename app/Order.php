<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use Metabble;

    /**
     * Fields that are mass fillable.
     */
    protected $fillable = [
        'user_id',
        'event_id',
        'status',
    ];

    /**
     * Get the user who placed the order.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function passes()
    {
        return $this->hasMany('App\Pass');
    }

    public function transactions()
    {
        return $this-hasMany('App\Transaction');
    }

    public function tickets()
    {
        return $this->belongsToMany('App\Ticket', 'order_quantities');
    }

    public function quantities()
    {
        return $this->hasMany('App\OrderQuantity');
    }

    /**
     * The event this order was created from.
     */
    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event', 'order_quantities');
    }
}
