<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pass extends Model
{

    use Metabble;


    protected $appends = [
        'pass_number',
        'event_date',
        'event_name',
        'pass_owner',
    ];
    /**
     * Fields that are mass fillable.
     */
    protected $fillable = [
        'event_id',
        'ticket_id',
        'order_id',
        'user_id',
        'ticket_name',
        'status',
        'salt',
    ];

    /**
     * Get the event this pass applies to.
     */
    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    /**
     * Get the ticket this pass applies to.
     */
    public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }

    /**
     * Get the order this ticket was purchased with.
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * Get the user who owns this pass.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getPassOwnerAttribute()
    {
        return $this->user->name;
    }

    /**
     * Get the pass number.
     */
    public function getPassNumberAttribute()
    {
        $number = sprintf("%2d%d", $this->salt, $this->id);
        $sum = $number;
        do {
            $sum = array_sum(str_split($sum));
        } while(strlen($sum) > 1);

        $check_digit = 9-$sum;

        return $number.$check_digit;
    }

    /**
     * Get the event date.
     */
    public function getEventDateAttribute()
    {
        $event = Event::find($this->event_id);
        return $event->start_time_object->toDayDateTimeString();
    }

    /**
     * Get the event name.
     */
    public function getEventNameAttribute()
    {
        $event = Event::find($this->event_id);
        return $event->title;
    }

    /**
     * Find pass by pass#
     */
    public static function findPass($no)
    {
        $sum = $no;
        do {
            $sum = array_sum(str_split($sum));
        } while($sum > 9);

        if($sum != 9) return null;

        $pass_id = substr($no, 2, strlen($no)-3);
        $salt = substr($no, 0, 2);

        $pass = Pass::find($pass_id);
        if($pass->salt != $salt){
            return null;
        }

        return $pass;
    }
}
