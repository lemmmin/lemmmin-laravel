<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permissionables';

    /**
     * Return the user who has the permission.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function entity()
    {
        return $this->morphTo('permissionable');
    }
}
