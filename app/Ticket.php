<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Ticket extends Model
{
    use Metabble;

    /**
     * Fields that are publicly fillable.
     */
    protected $fillable = [
        'name',
        'description',
        'price',
        'currency',
        'start_time',
        'end_time',
        'quantity',
        'issued',
        'privacy',
    ];

    /**
     * Get events associated with this ticket
     */
    public function events()
    {
        return $this->belongsToMany('App\Event');
    }

    /**
     * Get passes issued to this ticket type.
     */
    public function passes()
    {
        return $this->hasMany('App\Pass');
    }

    /**
     * Get number of tickets left.
     */
    public function getRemainingAttribute()
    {
        return $this->quantity - $this->issued ;
    }

    /**
     * Get number of tickets left for current user.
     */
    public function getUserRemainingAttribute()
    {
      if(!Auth::check()) return $this->max_purchase;

      return $this->max_purchase - $this->passes()->where('user_id', Auth::id())->count();
    }

    public function getMaxPurchaseAttribute()
    {
        $mp = $this->meta('max_purchase');
        return $mp == "" ? 5 : $mp;
    }

    public function orders()
    {
        return $this->belongsToMany('App\Order', 'order_quantities');
    }

}
