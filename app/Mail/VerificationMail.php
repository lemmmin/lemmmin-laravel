<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $confirmation_code = $this->user->meta('confirmation_code');
      $url = route('registration.activate', ['user' => $this->user, 'confirmation_code' => $confirmation_code]);

      return $this->view('vendor.notifications.email')
                  ->text('vendor.notifications.email-plain')
                  ->subject("Verify your account")
                  ->with([
                      'introLines' => [
                          "Your account at Lemmmin has been created. Please activate it by clicking below.",
                      ],
                      'outroLines' => [
                          "Wish you a great journey with Lemmmin!",
                      ],
                      'actionText' => ucwords("Verify Account"),
                      'actionUrl' => $url,
                      'level' => 'info',
                  ]);
    }
}
