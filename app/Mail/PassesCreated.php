<?php

namespace App\Mail;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PassesCreated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->order->passes()->count() > 1){
            $pass =  'passes';
            $is = 'are';
            $it = 'them';
        }
        else {
            $pass = 'pass';
            $is = 'is';
            $it = 'it';
        }

        return $this->view('vendor.notifications.email')
                    ->text('vendor.notifications.email-plain')
                    ->subject("Your $pass $is ready!")
                    ->with([
                        'introLines' => [
                            "Your order has been successfully processed and your $pass $is ready.",
                        ],
                        'actionText' => ucwords("View $pass"),
                        'actionUrl' => route('passes.index', ['order' => $this->order]),
                        'outroLines' => [
                            "Please print the $pass and bring $it or show $it on your phone while attending the event.",
                        ],
                        'level' => 'success',
                    ]);
    }
}
