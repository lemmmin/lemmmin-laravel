<?php

namespace App\Http\Requests;

use App\Organizer;
use App\Http\Requests\Request;

class StoreOrganizer extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'meta.image' => 'url',
            'meta.website' => 'url',
            'meta.social_facebook' => 'url',
            'meta.social_twitter' => 'url',
            'meta.colors_text' => 'regex:/#[0-9a-fA-F]{6}/',
            'meta.colors_background' => 'regex:/#[0-9a-fA-F]{6}/',
            'meta.colors_link' => 'regex:/#[0-9a-fA-F]{6}/',
        ];
    }

    /**
     * Defaine the error messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Organizer name must be specified.',
            'name.min:5' => 'Organizer name must be at least 5 characters long.',
            'meta.image.url' => 'Invalid url supplied for image.',
            'meta.website.url' => 'Invalid url supplied for website.',
            'meta.social_facebook.url' => 'Invalid url supplied for facebook.',
            'meta.social_twitter.url' => 'Invalid url supplied for twitter.',
            'meta.colors_text.regex' => 'Invalid text color format.',
            'meta.colors_background.regex' => 'Invalid background color format.',
            'meta.colors_link.regex' => 'Invalid link color format.',
        ];
    }
}
