<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreEvent extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5',
            'description' => 'required|min:10',
            'privacy' => 'required|in:public,private',
            'end_time' => 'after:start_time',
            'organizer_name' => 'required_if:organizer,new|min:5',
            'tickets.*.name' => 'required_unless:tickets.*.deleted,1',
            'subevents.*.title' => 'required_unless:subevents.*.deleted,1',
            'subevents.*.start_time' => 'required_unless:subevents.*.deleted,1',
            'subevents.*.end_time' => 'required_unless:subevents.*.deleted,1|after:subevents.*.start_time',
        ];
    }
}
