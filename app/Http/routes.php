<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Request;
use App\Event;

Route::group(['middleware' => ['web']], function () {
	Route::auth();
	Route::get('/registration/{user}/{confirmation_code}', 'Auth\RegisterController@activate')
			 ->name('registration.activate');

	Route::group(['prefix' => 'social'], function(){
		Route::get('facebook', 'SocialAuthController@facebook');
		Route::get('facebook/callback', 'SocialAuthController@facebookCallback');
		Route::get('google', 'SocialAuthController@google');
		Route::get('google/callback', 'SocialAuthController@googleCallback');
	});


	// Route for purchase ticket form submission
	Route::post('events/{event}/tickets', 'OrderController@postPurchaseTicket')
	->name('events.tickets');
	Route::get('order/prepare', 'OrderController@getPrepareOrder')
	->name('orders.prepare');
	Route::post('orders/{order}', 'OrderController@form_submit')
	->name('orders.form_submit');
	Route::get('orders/{order}', 'OrderController@show')
	->name('orders.show');
    Route::post('orders/{order}/pay', 'OrderController@postInitPayment')
        ->name('orders.payment');
    Route::get('orders/{order}/resurrect', 'OrderController@resurrectOrder')
        ->name('orders.resurrect');
	Route::post('payment/{order}/failed', 'OrderController@postFailedTransaction')
	->name('orders.failed_transaction');
	Route::post('payment/{order}/success', 'OrderController@postSuccessfulTransaction')
	->name('orders.successful_transaction');

	Route::get('events/{event}/guests', 'EventController@guestList')->name('events.guests');
	Route::get('events/{event}/export_codes', 'EventController@exportCodes')->name('events.export_codes');
	Route::get('events/{event}/export_csv', 'EventController@exportCsv')->name('events.export_csv');
	Route::get('search', 'EventController@search')->name('search');

	Route::resource('organizers', 'OrganizerController');

	Route::get('events/saved', 'EventController@savedEvents')->name('events.saved');
	Route::get('events/{event}/bookmark', 'EventController@bookmark')->name('events.bookmark');
	Route::resource('events', 'EventController');
	Route::resource('passes', 'PassController',
	['only' => ['index', 'show']]);

	Route::get('/', 'HomeController@getIndex')->name('home');
	Route::get('/browse', 'HomeController@getBrowse')->name('browse');

	Route::get('/terms-of-service', function(){
		return view('pages.tos');
	})->name('tos');

	Route::get('/about-us', function(){
		return view('pages.about');
	})->name('about');

	Route::get('/privacy-policy', function(){
		return view('pages.pp');
	})->name('pp');

	Route::get('/my_profile', 'ProfileController@edit')->name('profiles.self.edit');
	Route::get('/profiles/{user}/edit', 'ProfileController@edit')->name('profiles.edit');
	Route::post('/profiles/{user}/edit', 'ProfileController@update')->name('profiles.update');
});

Route::group([
		'middleware' => ['auth:api', 'api'],
		'prefix' => 'api',
		'as' => 'api::',
	], function () {

		Route::get('events/{event}/bookmark', 'Api\EventController@bookmark')->name('events.bookmark');
		Route::post('events/{event}/scan', 'Api\EventController@scan')->name('events.scan');
		Route::resource('events', 'Api\EventController',
			['except' => ['create', 'edit']]);
		Route::resource('passes', 'Api\PassController',
			['only' => ['index', 'show']]);

		Route::post('orders', 'Api\OrderController@placeOrder');
		Route::post('order/pay/{order}', 'Api\OrderController@postConfirmBkashPayment');

		Route::get('userinfo', 'Api\BasicController@userinfo');
});



Route::post('oauth/provider/{provider}', 'SocialAuthController@authorizeProviderToken')
	->middleware('api');

Route::post('api/users', 'Auth\RegisterController@apiCreate')->middleware(['api', 'auth:api']);
