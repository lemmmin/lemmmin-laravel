<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialAccountService;
use Redirect;
use Socialite;
use Input;
use DB;
use User;

class SocialAuthController extends Controller
{

    /**
     * Handle all callback processes after authentication.
     *
     * @param string $provider Name of the provider.
     */
     protected function handleCallback($provider, $service)
     {
        try {
            $user = Socialite::driver($provider)->user();
        } catch( \Exception $e ){
            return redirect('/login')->with('error', 'Access Denied!');
        }

        $user = $service->getSocialUser($user, $provider);
        $user->is_active = true;
        $user->save();

        \Auth::login($user, true);

        return Redirect::intended('/');
     }

    /**
     * Redirect to facebook for authentication.
     *
     * @return Response
     */
    protected function facebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Handle callback to authenticate from facebook.
     *
     * @return User
     */
    protected function facebookCallback(SocialAccountService $service)
    {
        return $this->handleCallback('facebook', $service);
    }

    /**
     * Redirect to google for authentication.
     *
     * @return Response
     */
    protected function google()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Handle callback to authenticate from google.
     *
     * @return User
     */
    protected function googleCallback(SocialAccountService $service)
    {
        return $this->handleCallback('google', $service);
    }

    /**
     * Verify 3rd party api tokens to generate local user.
     *
     * return string token.
     */
    public function authorizeProviderToken($provider)
    {
        $token = Input::get('token');
        $client_id = Input::get('client_id');
        $client_secret = Input::get('client_secret');

        $client_valid = DB::table('oauth_clients')->where([
            ['id', $client_id],
            ['secret', $client_secret],
            ['password_client', 1],
            ['revoked', 0]
        ])->count();

        if($client_valid){

            if('google' == $provider){
                $client = new \GuzzleHttp\Client();
                $res = $client->request('POST',
                'https://www.googleapis.com/oauth2/v4/token',
                [
                    'form_params' => [
                        'grant_type' => 'authorization_code',
                        'client_id' => env('GOOGLE_CLIENT_ID'),
                        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
                        'redirect_uri' => '',
                        'code' => $token,
                    ]
                ]);
                $response_json = json_decode((string)$res->getBody());
                $token = $response_json->access_token;
            }


            try {
                $providerUser = Socialite::driver($provider)->userFromToken($token);
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $errorBody = json_decode($e->getResponse()->getBody())->error;
                if('google' == $provider){
                    return response([
                        'error' => $errorBody->errors[0]->reason,
                        'message' => $errorBody->errors[0]->message,
                    ], 401);
                }
                elseif('facebook' == $provider) {
                    return response([
                        'error' => $errorBody->type,
                        'message' => $errorBody->message,
                    ], 401);
                }
            }



            $service = new SocialAccountService;
            $user = $service->getSocialUser($providerUser, $provider);

            $token = $user->createToken($provider, []);

            return [
                'token_type' => 'Bearer',
                'expires_in' => strtotime($token->token->expires_at) - time(),
                'access_token' => $token->accessToken,
                'refresh_token' => '',
            ];
        }
    }
}
