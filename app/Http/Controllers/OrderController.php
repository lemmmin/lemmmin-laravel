<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\InvalidateOrder;
use App\Event;
use App\Order;
use App\OrderQuantity;
use App\Ticket;
use App\Pass;
use App\Transaction;
use App\FormField;
use Input;
use Auth;
use Redirect;
use Response;
use LemmminHelper;
use Carbon\Carbon;
use Validator;
use Session;
use Mail;
use App\Mail\PassesCreated;


class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['postPurchaseTicket']]);
    }


    /**
     * Handle form submission to purchase ticket after selecting.
     */
    public function postPurchaseTicket($id)
    {
        $event = Event::find($id);
        if(is_null($event) || !$event->live){
            return redirect()->route('events.show', $event);
        }

        $tickets = Input::get('tickets');
        $tickets = array_filter($tickets, function($t){
            return $t > 0;
        });

        $total_tickets_count = array_sum($tickets);

        $users_ticket_count = Auth::check() ? $event->passes()->where('user_id', Auth::id())->count() : 0;
        if($total_tickets_count + $users_ticket_count > $event->max_ticket_purchase ){
            abort(500, 'Too many tickets!');
        }
        elseif($total_tickets_count < 1){
            return back();
        }

        session(['tickets' => $tickets]);
        session(['event_id' => $event->id]);

        return redirect()->route('orders.prepare');
    }

    /**
     * Prepare the order to purchase tickets.
     */
    public function getPrepareOrder(Request $request)
    {
        if(!Session::has('event_id') || !Session::has('tickets')) return redirect()->route('home');

        $event = Event::find(session('event_id'));
        $tickets = session('tickets');
        $request->session()->forget(['event_id', 'tickets']);

        $users_ticket_count = $event->passes()->where('user_id', Auth::id())->count();
        if($users_ticket_count > $event->max_ticket_purchase ){
            abort(500, 'Too many tickets!');
        }

        $order = Order::create([
            'user_id' => Auth::id(),
            'status' => 'pending',
            'event_id' => $event->id,
        ]);


        $rows = [];
        $total = 0;
        foreach($tickets as $ticket_id => $quantity){
            $ticket = Ticket::find($ticket_id);
            if(is_null($ticket)) continue;

            if($ticket->remaining < $quantity){
                abort(500, 'Not enough tickets available.');
            }
            if($ticket->user_remaining < $quantity){
                abort(403, 'Not enough tickets available for you.');
            }

            $rows[] = [
                'order_id' => $order->id,
                'ticket_id' => $ticket->id,
                'ticket_name' => $ticket->name,
                'event_id' => $ticket->events[0]->id,
                'quantity' => $quantity,
                'price' => $ticket->price,
            ];

            $ticket->issued += $quantity;
            $ticket->save();

            $total += $quantity*$ticket->price;
        }


        OrderQuantity::insert($rows);

        $order->update_meta('total', $total);
        $order->update_meta('fees', $total / 100 * LemmminHelper::$processing_fee_percent);
        $order->update_meta('fee_percentage', LemmminHelper::$processing_fee_percent);

        if($event->form_fields()->count() != 0){
            $order->status = 'pending_form';
            $order->save();
        }

        $this->generatePasses($order);

        $invalidateJob = new InvalidateOrder($order);
        $invalidateJob->delay(Carbon::now()->addMinutes(LemmminHelper::$order_reserve_duration));
        dispatch($invalidateJob);

        return redirect()->route('orders.show', $order->id);
    }

    /**
     * Get a link to payment url and forward user to that.
     */
    public function postInitPayment(Request $request, $id)
    {
        $order = Order::find($id);
        $this->authorize('pay', $order);


        $validator = Validator::make($request->all(), [
            'email' => 'email',
            'phone' => 'not_in:0',
        ]);
        $validator->sometimes('email', 'required|email', function() use ($order){
            return $order->user->email == '';
        });

        $validator->sometimes('phone', 'required', function() use ($order){
            return $order->user->meta('phone') == '';
        });

        $validator->validate();

        if(Input::has('email')){
            $order->user->email = Input::get('email');
            $order->user->save();
        }
        if(Input::has('phone')){
            $order->user->update_meta('phone', Input::get('phone'));
        }

        if($order->meta('total') < LemmminHelper::$minimum_payment)
            return redirect()->route('orders.show', $order->id);

        $processing_fee = $order->meta('fees');
        $amount_total = $order->meta('total')+$processing_fee;
        $processing_fee_percent = $order->meta('fee_percentage');


        $url = config('services.epw.url').'payment/request.php';

        $domain = config('app.url'); // or Manually put your domain name
        $ip = $_SERVER["SERVER_ADDR"];

        $fields = [
            'store_id' => config('services.epw.store_id'),
            'signature_key' => config('services.epw.signature_key'),
            'amount' => floatval($amount_total),
            'tran_id' => "Order#$id-".time(),
            'success_url' => route('orders.successful_transaction', $order->id),
            'fail_url' => route('orders.failed_transaction', $order->id),
            'cancel_url' => route('orders.failed_transaction', $order->id),
            'currency' => 'BDT',
            'amount_processingfee' => $processing_fee,
            'amount_processingfee_ratio' => $processing_fee_percent,
            'desc' => "Payment for lemmmin order#$id.",
            'cus_name' => $order->user->name,
            'cus_email' => $order->user->email,
            'cus_add1' => $order->user->meta('address_line_1'),
            'cus_add2' => $order->user->meta('address_line_2'),
            'cus_city' => $order->user->meta('city'),
            'cus_state' => $order->user->meta('district'),
            'cus_postcode' => $order->user->meta('postcode'),
            'cus_country' => 'Bangladesh',
            'cus_phone' => $order->user->meta('phone'),
            'opt_a' => $id,
            'opt_b' => Auth::id(),
        ];

        // $fields_string = '';
        // //url-ify the data for the POST
        // foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        // rtrim($fields_string, '&');

        $ch = curl_init();

        //set the url, number of POST vars, POST data

        curl_setopt( $ch, CURLOPT_HTTPHEADER, array("REMOTE_ADDR: $ip", "HTTP_X_FORWARDED_FOR: $ip"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $domain);
        curl_setopt($ch, CURLOPT_INTERFACE, $ip);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute post

        $result = curl_exec($ch);

        $url = json_decode($result, true);

        if(!filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_QUERY_REQUIRED) === false)
            return Redirect::to($url);

        abort(500, $url);
    }

    /**
     * Handle a failed transaction.
     */
    public function postFailedTransaction($id)
    {
        return redirect()->route('orders.show', $id)->withErrors([Input::get('reason')]);
    }

    /**
     * Handle a successful transaction.
     */
    public function postSuccessfulTransaction($id)
    {
        $order = Order::find($id);
        $this->authorize('pay', $order);

        if(Input::get('pay_status') != "Successful"){
            return print_r(Input::all(), 1);

            return redirect()->route('orders.show', $id);
        }

        $url = config('services.epw.url').'api/v1/trxcheck/request.php';
        $url .= '?';
        $url .= http_build_query([
            'request_id' => Input::get('mer_txnid'),
            'store_id' => config('services.epw.store_id'),
            'signature_key' => config('services.epw.signature_key'),
            'type' => 'json'
        ]);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        $data = json_decode($result);

        if(
            $data->amount < $order->meta('total') + $order->meta('fees') ||
            $data->risk_level != 0 ||
            $data->status_code != 2 ||
            $data->opt_a != $id
        ){
            return redirect()->route('orders.show', $id)
                ->withErrors([
                    "Could not verify transaction. Please contact with referance {$data->mer_txnid}"
                ]);
        }

        $transaction = Transaction::create([
            'referance' => $data->mer_txnid,
            'amount' => $data->amount,
            'currency' => $data->currency,
            'gateway' => $data->payment_type,
            'note' => 'epw_txnid: '.$data->epw_txnid,
            'status' => 'successful'
        ]);

        $order->passes()->update(['status' => 'valid']);
        $transaction->status = 'complete';
        $transaction->order()->associate($order);
        $order->status = 'complete';

        $this->sendPasses($order);

        $order->save();
        $transaction->save();



        return redirect()->route('orders.show', $order);
    }

    /**
     * Show an order page.
     */
    public function show($id){
        $order = Order::find($id);
        $this->authorize('show', $order);

        if($order->status == 'pending_form'){
            return view('orders.form', ['order' => $order]);
        }

        if($order->meta('total', 0) == 0 && $order->status == 'pending'){
            $order->status = 'complete';
            $order->save();

            $order->passes()->update(['status' => 'valid']);

            $this->sendPasses($order);
        }

        return view('orders.show', ['order' => $order]);
    }

    /**
     * Save information from form-fields.
     */
    public function form_submit(Request $request, $id)
    {
        $order = Order::find($id);
        $this->authorize('update', $order);
        $rules = [];
        $messages = [];

        foreach($order->event->form_fields as $field){
            $rules['field.*.'.$field->id] = [];

            if($field->required) {
                $rules['field.*.'.$field->id][] = 'required';
                $messages['field.*.'.$field->id.'.required'] = "$field->title is a required field.";
            }
            if($field->type == 'email') {
                $rules['field.*.'.$field->id][] = 'email';
                $messages['field.*.'.$field->id.'.email'] = "$field->title must be an email address.";
            }

            $rules['field.*.'.$field->id] = implode('|', $rules['field.*.'.$field->id]);
        }

        $this->validate($request, $rules, $messages);

        foreach(Input::get('field') as $pass_id => $fields){
            $pass = Pass::find($pass_id);
            $this->authorize('describe', $pass);

            $form_data = [];
            foreach($fields as $field_id => $value){
                $field = FormField::find($field_id);
                $this->authorize('view', $field);
                $form_data[$field->title] = $value;
                if($field->type == 'checkbox') $form_data[$field->title] = implode(', ', $value);
            }

            $pass->update_meta('form_data', serialize($form_data));
        }

        $order->status = 'pending';
        $order->save();

        return redirect()->route('orders.show', $order);
    }



    /**
     * Send email with link to passes.
     */
    public function sendPasses($order)
    {
        return Mail::to($order->user)->send(new PassesCreated($order));
    }

    /**
     * Generate passes from an order.
     */
    public function generatePasses($order)
    {
        $passes = [];
        foreach($order->quantities as $type){
            for($i = 0; $i < $type->quantity; $i++){
                $passes[] = [
                    'event_id' => $type->event_id,
                    'ticket_id' => $type->ticket_id,
                    'order_id' => $order->id,
                    'user_id' => $order->user_id,
                    'ticket_name' => $type->ticket_name,
                    'status' => 'pending',
                    'salt' => random_int(10, 99),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }
        }

        Pass::insert($passes);

        return true;
    }

    /**
     * Re-prepare an expired order for payment attempt by admin/host.
     */
    public function resurrectOrder($order)
    {
        $order = Order::find($order);
        $event = $order->event;

        $this->authorize('resurrect', $order);


        if($event->form_fields()->count() != 0){
            $order->status = 'pending_form';
            $order->save();
        }
        else {
            $order->status = 'pending';
            $order->save();
        }

        $this->generatePasses($order);

        $invalidateJob = new InvalidateOrder($order);
        $invalidateJob->delay(Carbon::now()->addMinutes(LemmminHelper::$order_reserve_duration));
        dispatch($invalidateJob);

        return redirect()->route('orders.show', $order->id);
    }
}
