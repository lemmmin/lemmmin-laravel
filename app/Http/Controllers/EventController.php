<?php

namespace App\Http\Controllers;

use DateTime;
use App\Event;
use App\Ticket;
use App\Organizer;
use App\FormField;
use Input;
use Auth;
use Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use MediaUploader;
use Response;
use AlgoliaSearch\Client;
use \Illuminate\Pagination\Paginator;

class EventController extends Controller
{

    /**
     * List of form field types.
     */
    public $field_types = [
      'text' => 'Text',
      'paragraph' => 'Paragraph',
      'select' => 'Select',
      'radio' => 'Radio',
      'checkbox' => 'Checkbox',
      'email' => 'Email',
    ];

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show', 'search']]);
    }

    /**
     * Validator for event requests.
     */
    public function validator($request){
        $messages = [
            'title.required' => 'You must specify an event title.',
            'title.min:5' => 'Event title must be at least 5 characters long.',
            'tickets.required' => 'You must set at least one ticket type.',
            'tickets.*.name.required_unless' => 'Ticket name is a required field.',
            'tickets.*.price.required_unless' => 'Ticket price is a required field.',
            'tickets.*.price.numeric' => 'Invalid ticket price given.',
            'tickets.*.quantity.required_unless' => 'Ticket quantity is a required field.',
            'tickets.*.quantity.integer' => 'Ticket quantity given is not valid.',
            'tickets.*.name.required_unless' => 'Ticket name is required.',
            'subevents.*.title.required_unless' => 'The title field of subevent is required.',
            'subevents.*.start_time.required_unless' => 'Subevents must have a starting time.',
            'subevents.*.end_time.required_unless' => 'Subevents must have an ending time.',
            'subevents.*.end_time.required_unless' => 'Subevents must have an ending time.',
            'subevents.*.end_time.after' => 'The end time of subevents must be a time after start time.',
            'form_fields.*.title.required' => 'Form fields must have a name.',
            'form_fields.*.type.in' => 'Invalid field type.',
        ];

        return Validator::make($request->all(), [
            'title' => 'required|min:5',
            'description' => 'required|min:10',
            'privacy' => 'required|in:public,private',
            'end_time' => 'after:start_time',
            'tickets' => 'required',
            'tickets.*.name' => 'required_unless:tickets.*.deleted,1',
            'tickets.*.price' => 'required_unless:tickets.*.deleted,1|numeric',
            'tickets.*.quantity' => 'required_unless:tickets.*.deleted,1|integer',
            'subevents.*.title' => 'required_unless:subevents.*.deleted,1',
            'subevents.*.start_time' => 'required_unless:subevents.*.deleted,1',
            'subevents.*.end_time' => 'required_unless:subevents.*.deleted,1|after:subevents.*.start_time',
            'form_fields.*.title' => 'required',
            'form_fields.*.type' => 'in:'.implode(',', array_keys($this->field_types)),
        ], $messages);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.events.index', [
            'events' => Event::where('owner_id', '=', Auth::id())->paginate(12)
        ]);
    }

    public function savedEvents()
    {
        return view('frontend.browse', [
            'title' => 'Saved Events',
            'events' => Auth::user()->saved_events()->where([
                ['privacy', 'public'],
                ['live', 1]
            ])
            ->paginate(12)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Event::class);
        return view('dashboard.events.create')
             ->with('organizers', Auth::user()->organizers()->get())
             ->with('field_types', $this->field_types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\StoreEvent  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Event::class);
        $event = Event::create(Input::all());

        $event->owner_id = Auth::id();
        $event->start_time = new DateTime(Input::get('start_time'));
        $event->end_time = new DateTime(Input::get('end_time'));

        if($event->slug == '-' || $event->slug == '') $event->slug = $event->title;
        $event->slug = str_slug($event->slug);

        if(Input::get('organizer') == 'new' && trim(Input::get('organizer_name')) != '') {
            $this->authorize('create', Organizer::class);
            $organizer = Organizer::create([
                'name' => Input::get('organizer_name'),
                'about' => Input::get('organizer_about'),
            ]);
            $organizer->owner_id = Auth::id();

            $organizer->save();

            $event->organizer_id = $organizer->id;
        }
        elseif(Input::get('organizer') != '' && Auth::user()->can('attach', Organizer::find(Input::get('organizer')))) {
            $event->organizer_id = Input::get('organizer');
        }

        if(!empty(Input::get('tickets'))):
        foreach(Input::get('tickets') as $ticket){
            if($ticket['deleted'] != 1) {
                $ticket['privacy'] = isset($ticket['private']) && $ticket['private'] == 'on' ? 'private' : 'public';

                $ticket_object = new Ticket($ticket);
                $event->tickets()->save($ticket_object);
                $ticket_object->update_meta('max_purchase', $ticket['max_purchase']);

            }
        }
        endif;

        if(!empty(Input::get('form_fields'))):
        foreach(Input::get('form_fields') as $field){
            if($field['deleted'] != 1) {
                $field['required'] = isset($field['required']) && $field['required'] == 'on' ? true : false;;
                $event->form_fields()->create($field);
            }
        }
        endif;

        Event::withoutSyncingToSearch(function () use ($event){
        if(!empty(Input::get('subevents'))):
            foreach(Input::get('subevents') as $subevent){
                if($subevent['deleted'] != 1) {
                    $subeventModel = new Event($subevent);
                    $subeventModel->owner_id = Auth::id();
                    $event->children()->save($subeventModel);
                }
            }
            endif;
        });

        if(Input::hasFile('cover_image')){
            $media = MediaUploader::fromSource($request->file('cover_image'))
                ->useHashForFilename()
                ->onDuplicateReplace()
                ->toDestination('uploads', 'event/cover_images')
                ->upload();

            $event->attachMedia($media, ['cover_image']);
        }


        $event->update_meta('max_ticket_purchase_quantity', Input::get('max_ticket_purchase_quantity', 0));
        $event->update_meta('online_event', Input::get('online_event') == 'on');
        $event->update_meta('location_name', Input::get('location.name'));
        $event->update_meta('location_address', Input::get('location.address'));
        $event->update_meta('location_latitude', Input::get('location.latitude'));
        $event->update_meta('location_longitude', Input::get('location.longitude'));


        $validator = $this->validator($request);

        $redirect = null;

        if(Input::get('live') == 'on' && $validator->fails()){
            $event->live = false;
            Event::withoutSyncingToSearch(function () use ($event){
                $event->save();
            });
            $redirect = Redirect(route('events.edit', $event->id))
                ->withErrors($validator);
        }
        elseif(Input::get('live') != 'on'){
            $event->live = false;
            Event::withoutSyncingToSearch(function () use ($event){
                $event->save();
            });
            $redirect = Redirect(route('events.edit', $event->id));
        }
        else {
            $event->live = true;
            Event::withoutSyncingToSearch(function () use ($event){
                $event->save();
            });
        }


        if(!is_null($redirect)) return $redirect;

        if($event->privacy == 'public'){
            $event->searchable();
        }
        else {
            $event->unsearchable();
        }


        Session::flash('message', 'Event created!');
        return Redirect(route('events.show', $event->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);

        if(!$event->live) $this->authorize('view', $event);

        return view('dashboard.events.show')->with('event', $event);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);

        $this->authorize('update', $event);

        return view('dashboard.events.edit')
             ->with([
                'event' => $event,
                'organizers' => Auth::user()->organizers()->get(),
                'field_types' => $this->field_types,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Requests\StoreEvent $event
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::find($id);

        $this->authorize('update', $event);

        $event->title = Input::get('title');
        $event->slug = Input::get('slug');

        if($event->slug == '-' || $event->slug == '') $event->slug = $event->title;
        $event->slug = str_slug($event->slug);

        $event->start_time = new DateTime(Input::get('start_time'));
        $event->end_time = new DateTime(Input::get('end_time'));
        $event->summary = Input::get('summary');
        $event->description = Input::get('description');
        $event->privacy = Input::get('privacy');


        if(Input::get('organizer') == 'new' && trim(Input::get('organizer_name')) != '') {
            // $this->authorize('create', Organizer::class);
            $organizer = Organizer::create([
                'name' => Input::get('organizer_name'),
                'about' => Input::get('organizer_about'),
            ]);
            $organizer->owner_id = Auth::id();

            $organizer->save();

            $event->organizer_id = $organizer->id;
        }
        elseif(Input::get('organizer') != '' && Auth::user()->can('attach', Organizer::find(Input::get('organizer')))) {
            $event->organizer_id = Input::get('organizer');
        }

        if(Input::has('tickets')) {
            foreach(Input::get('tickets') as $ticket){
                if($ticket['deleted'] == '1' && $ticket['id'] != '0'){
                    $ticketModel = Ticket::find($ticket['id']);
                    $this->authorize('delete', $ticketModel);
                    $ticketModel->delete();
                }
                else if($ticket['id'] == '0' && $ticket['deleted'] != '1') {
                    $ticket['privacy'] = isset($ticket['private']) && $ticket['private'] == 'on' ? 'private' : 'public';
                    $ticket_e = new Ticket($ticket);
                    $event->tickets()->save($ticket_e);
                    $ticket_e->update_meta("max_purchase", $ticket['max_purchase']);
                }
                else if(!$ticket['deleted']){
                    $ticket_e = Ticket::find($ticket['id']);
                    $this->authorize('update', $ticket_e);
                    $ticket_e->name = $ticket['name'];
                    $ticket_e->description = $ticket['description'];
                    $ticket_e->price = $ticket['price'];
                    $ticket_e->currency = $ticket['currency'];
                    $ticket_e->quantity = $ticket['quantity'];
                    $ticket_e->start_time = $ticket['start_time'];
                    $ticket_e->end_time = $ticket['end_time'];
                    $ticket_e->privacy = isset($ticket['private']) && $ticket['private'] == 'on' ? 'private' : 'public';
                    $ticket_e->update_meta("max_purchase", $ticket['max_purchase']);
                    $ticket_e->save();
                }
            }
        }

        if(Input::has('form_fields')) {
            foreach(Input::get('form_fields') as $field){
                $field['required'] = isset($field['required']) && $field['required'] == 'on' ? true : false;
                if($field['deleted'] == '1' && $field['id'] != '0'){
                    $fieldModel = FormField::find($field['id']);
                    $this->authorize('delete', $fieldModel);
                    $fieldModel->delete();
                }
                else if($field['id'] == '0' && $field['deleted'] != '1') {
                    $this->authorize('create', FormField::class);
                    $event->form_fields()->create($field);
                }
                else if(!$field['deleted']){
                    $fieldModel = FormField::find($field['id']);
                    $this->authorize('update', $fieldModel);
                    $fieldModel->fill($field);
                    $fieldModel->save();
                }
            }
        }

        if(Input::has('subevents')) {
            foreach(Input::get('subevents') as $subevent){
                if($subevent['deleted'] == '1' && $subevent['id'] != '0'){
                    $subeventModel = Event::withoutGlobalScope('root')->find($subevent['id']);
                    $this->authorize('delete', $subeventModel);
                    $subeventModel->delete();
                }
                else if($subevent['id'] == '0' && $subevent['deleted'] != '1') {
                    $subeventModel = new Event($subevent);
                    $subeventModel->owner_id = Auth::id();
                    Event::withoutSyncingToSearch(function () use ($event, $subeventModel){
                        $subeventModel->save();
                        $event->children()->save($subeventModel);
                    });
                }
                else if($subevent['deleted'] != '1') {
                    $subeventModel = Event::withoutGlobalScope('root')->find($subevent['id']);
                    $this->authorize('update', $subeventModel);
                    $subeventModel->title = $subevent['title'];
                    $subeventModel->description = $subevent['description'];
                    $subeventModel->start_time = $subevent['start_time'];
                    $subeventModel->end_time = $subevent['end_time'];
                    Event::withoutSyncingToSearch(function () use ($subeventModel){
                        $subeventModel->save();
                    });
                }
            }
        }

        if(Input::hasFile('cover_image')){

            // Delete any existing cover imag of the same event.
            if($event->hasMedia('cover_image')){
                $event->firstMedia('cover_image')->delete();
            }

            $media = MediaUploader::fromSource($request->file('cover_image'))
                ->useHashForFilename()
                ->onDuplicateReplace()
                ->toDestination('uploads', 'event/cover_images')
                ->upload();

            $event->attachMedia($media, ['cover_image']);
        }


        $event->update_meta('max_ticket_purchase_quantity', Input::get('max_ticket_purchase_quantity', 0));
        $event->update_meta('online_event', Input::get('online_event') == 'on');
        $event->update_meta('location_name', Input::get('location.name'));
        $event->update_meta('location_address', Input::get('location.address'));
        $event->update_meta('location_latitude', Input::get('location.latitude'));
        $event->update_meta('location_longitude', Input::get('location.longitude'));


        $validator = $this->validator($request);

        if(Input::get('live') == 'on' && $validator->fails()){
            $event->live = false;
            Event::withoutSyncingToSearch(function () use ($event){
                $event->save();
            });
            $event->unsearchable();
            return Redirect(route('events.edit', $event->id))
                ->withErrors($validator);
        }
        elseif(Input::get('live') != 'on'){
            $event->live = false;
            Event::withoutSyncingToSearch(function () use ($event){
                $event->save();
            });
            $event->unsearchable();
            return Redirect(route('events.edit', $event->id));
        }
        else {
            $event->live = true;
        }

        Event::withoutSyncingToSearch(function () use ($event){
            $event->save();
        });

        if($event->privacy == 'public'){
            $event->searchable();
        }
        else {
            $event->unsearchable();
        }

        Session::flash('message', 'Updates saved!');
        return Redirect(route('events.edit', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);

        $this->authorize('delete', $event);

        $event->deleteRelatives();
        $event->delete();

        Session::flash('message', 'Event Deleted!');
        return Redirect(route('events.index'));
    }

    /**
     * Route for searching
     */
    public function search(Request $request)
    {
        $client = new Client(config('scout.algolia.id'), config('scout.algolia.secret'));
        $index = $client->initIndex('events');

        $options = [
            'attributesToRetrieve' => ['id'],
            'attributesToHighlight' => [],
            'hitsPerPage' => 12,
            'page' => Input::has('page') ? Input::get('page')-1 : 0
        ];

        $sort = Input::get('sort');

        if($sort == 'nearby'){
            if(Input::has('latlng') && Input::get('latlng') != '')
                $options['aroundLatLng'] = Input::get('latlng');
            else
                $options['aroundLatLngViaIP'] = true;
        }


        $result = $index->search($request->q, $options);

        $events = array_map(function($hit){
            return Event::find($hit['id']);
        }, $result['hits']);

        $events = new \Illuminate\Pagination\LengthAwarePaginator(
            $events,
            $result['nbHits'],
            $result['hitsPerPage']
        );

        return view('frontend.search', [
            'events' => $events->appends(Input::except('page'))->setPath(request()->path()),
        ]);
    }

    public function guestList($id)
    {
        $event = Event::find($id);
        $this->authorize('see_guests', $event);

        $order_by = Input::get('order_by', 'id');
        $order = Input::get('order', 'desc');
        $passes = $event->passes()->orderBy($order_by, $order);

        if(Input::has('order_no'))
            $passes->where('order_id', '=', Input::get('order_no'));

        if(Input::has('user'))
            $passes->where('user_id', '=', Input::get('user'));

        $passes = $passes->paginate(50);

        return view('dashboard.events.guests', [
            'event' => $event,
            'passes' => $passes,
        ]);
    }

    /**
     * Export list of guests in csv format
     */
    public function exportCsv($id)
    {
        $event = Event::find($id);
        $this->authorize('see_guests', $event);
        $passes = $event->passes()
                        ->leftJoin('users', 'users.id', 'passes.user_id')
                        ->leftJoin('meta', 'meta.entity_id', '=', 'passes.id')
                        ->get();

        $fields = $event->form_fields;

        $keys = [
            'Pass#',
            'Order#',
            'Ticket Name',
            'Status',
            'User ID',
            'Name',
            'Email'
        ];

        foreach($fields as $field){
            $keys[] = $field->title;
        }

        $tempfile = tempnam(sys_get_temp_dir(), 'lemmmin_');
        $output = fopen($tempfile, 'w') or die("Can't open tempfile");
        fputcsv($output, $keys);

        foreach($passes as $pass){
            $data = [];
            $form_fields = unserialize($pass->value);

            foreach($keys as $key){
                switch($key){
                    case "Pass#":
                        $data[] = $pass->pass_number;
                        break;
                    case "Order#":
                        $data[] = $pass->order_id;
                        break;
                    case "Ticket Name":
                        $data[] = $pass->ticket_name;
                        break;
                    case "Status":
                        $data[] = $pass->status;
                        break;
                    case "User ID":
                        $data[] = $pass->user_id;
                        break;
                    case "Name":
                        $data[] = $pass->name;
                        break;
                    case "Email":
                        $data[] = $pass->email;
                        break;
                    default:
                        if(isset($form_fields[$key])) $data[] = $form_fields[$key];
                        else $data[] = "--";
                        break;
                }
            }

            fputcsv($output, $data);
        }

        fclose($output);
        return response()->download($tempfile, "lemmmin_export_event_$id-".time().".csv");
    }

    public function exportCodes($id)
    {
        $event = Event::find($id);
        $this->authorize('see_guests', $event);

        $order_by = Input::get('order_by', 'id');
        $order = Input::get('order', 'desc');
        $passes = $event->passes()->orderBy($order_by, $order);

        if(Input::has('order_no'))
            $passes->where('order_id', '=', Input::get('order_no'));

        if(Input::has('ticket'))
            $passes->where('ticket_id', '=', Input::get('ticket'));

        if(Input::has('user'))
            $passes->where('user_id', '=', Input::get('user'));

        // Print 10 pages at a time.
        $passes = $passes->paginate(800);

        if(Input::has('color')){
            $colors = explode(',', Input::get('color'));
        }
        else {
            $colors = [0, 0, 0];
        }

        return view('dashboard.events.export_codes', [
            'event' => $event,
            'passes' => $passes->appends(Input::except('page')),
            'colors' => $colors
        ]);
    }

    public function bookmark($id)
    {
        if(!Auth::check()) return Response::json(['success' => false], 403);

        $event = Event::find($id);
        $this->authorize('view', $event);
        $user = Auth::user();

        $user->saved_events()->toggle($event);

        return response()->json([
            'success' => true,
            'current_status' => $event->is_bookmarked,
        ]);
    }
}
