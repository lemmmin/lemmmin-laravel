<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Input;
use Illuminate\Http\Request;
use Response;
use Mail;
use App\Mail\VerificationMail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Override default user registration.
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->create($request->all());

        $confirmation_code = str_random(30);
        $user->update_meta('confirmation_code', $confirmation_code);

        Mail::to($user)->send(new VerificationMail($user));

        session()->flash('message', 'We sent you an activation code. Check your email.');
        return redirect()->route('login');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Activate a user account from the link.
     */
    public function activate($user, $confirmation_code)
    {
      $user = User::find($user);

      if($user->meta('confirmation_code') == $confirmation_code){
        $user->is_active = 1;
        $user->save();
        $user->delete_meta('confirmation_code');

        session()->flash('message', 'Your account has been activated! Login to continue.');
        return redirect()->route('login');
      }

      abort(403, "Invalid confirmation code!");
    }

    /**
     * Create user through api call.
     */
    protected function apiCreate()
    {
        $validator = $this->validator(Input::all());

        if($validator->fails()){
            return Response::json(implode(",\n", $validator->errors()->all()), 400);
        }

        return Response::json(['success' => true]);
    }

}
