<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Event;
use Response;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(){
        return view('welcome', [
            'recommended_events' => Event::where([
                ['end_time', '>', date("Y-m-d H:i:s")],
                ['privacy', 'public'],
                ['live', 1],
                ['parent', null]
            ])->latest()
            ->take(6)
            ->get(),
        ]);
    }

    public function getBrowse(){
        return view('frontend.browse', [
            'events' => Event::where([
                ['end_time', '>', date("Y-m-d H:i:s")],
                ['privacy', 'public'],
                ['live', 1],
                ['parent', null]
            ])
            ->latest()
            ->paginate(12)
        ]);
    }
}
