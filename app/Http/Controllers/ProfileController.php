<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Redirect;
use Input;
use Gate;
use Hash;
use Illuminate\Validation\Rule;


class ProfileController extends Controller
{

    function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id = 0)
    {
        // If editing own profile, redirect to proper route.
        if($id != 0 && Auth::id() == $id){
            $request->session()->reflash();
            return Redirect::route('profiles.self.edit');
        }

        if($id == 0) $user = Auth::user();
        else $user = User::find($id);

        $this->authorize('update', $user);

        return view('profiles.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($id),
            ],
            'phone' => 'required|not_in:0',
            'new_password' => 'different:old_password|confirmed',
        ]);

        $user = User::find($id);
        $this->authorize('update', $user);

        $user->name = Input::get('name');
        if(Gate::allows('change_email', $user))
            $user->email = Input::get('email');
        $user->update_meta('phone', Input::get('phone'));
        $user->update_meta('address_line_1', Input::get('address_line_1'));
        $user->update_meta('address_line_2', Input::get('address_line_2'));
        $user->update_meta('city', Input::get('city'));
        $user->update_meta('post_code', Input::get('post_code'));
        $user->update_meta('district', Input::get('district'));

        if($user->password == '' || Hash::check(Input::get('old_password'), $user->password)) {
            $user->password = Hash::make(Input::get('new_password'));
        }

        $user->save();

        $request->session()->flash('info', 'Profile updated!');
        return Redirect::route('profiles.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
