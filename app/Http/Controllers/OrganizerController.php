<?php

namespace App\Http\Controllers;

use App\Organizer;
use App\Meta;
use Auth;
use Input;
use Session;

use Illuminate\Http\Request;

use App\Http\Requests;

class OrganizerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.organizers.index', [
            'organizers' => Auth::user()->organizers()->paginate(20)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Organizer::class);

        return view('dashboard.organizers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\StoreOrganizer  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreOrganizer $request)
    {
        $this->authorize('create', Organizer::class);
        $organizer = Organizer::create(Input::all());

        $organizer->owner_id = Auth::id();

        foreach(Input::get('meta') as $key => $value){
            if(trim($value) == '') continue;
            $organizer->meta()->save(new Meta(['key' => $key, 'value' => $value]));
        }
        $organizer->save();

        Session::flash('message', 'Organizer created!');
        return Redirect(route('organizers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organizer = Organizer::find($id);

        return view('dashboard.organizers.show', [
            'organizer' => $organizer,
            'events' => $organizer->events()->where([
                ['privacy', 'public'],
                ['live', 1]
            ])->paginate(12)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organizer = Organizer::find($id);
        $this->authorize('update', $organizer);

        return view('dashboard.organizers.edit')->with('organizer', $organizer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Requests\StoreOrganizer  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\StoreOrganizer $request, $id)
    {
        $organizer = Organizer::find($id);
        $this->authorize('update', $organizer);
        $organizer->name = Input::get('name');
        $organizer->about = Input::get('about');

        foreach(Input::get('meta') as $key => $value){
            $organizer->update_meta($key, $value);
        }
        $organizer->save();

        Session::flash('message', 'Update saved!');
        return Redirect(route('organizers.edit', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $organizer = Organizer::find($id);
        $this->authorize('delete', $organizer);
        Meta::where('entity_type', 'App\Organizer')
            ->where('entity_id', $id)
            ->delete();

        $organizer->delete();

        Session::flash('message', 'Organizer Deleted!');
        return Redirect(route('organizers.index'));
    }
}
