<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Input;
use Response;
use App\Order;
use App\Ticket;
use App\Transaction;
use App\OrderQuantity;
use App\Jobs\InvalidateOrder;
use Carbon\Carbon;
use LemmminHelper;

class OrderController extends Controller
{
    public function placeOrder()
    {
        $tickets = Input::json()->all();
        $tickets = array_filter($tickets, function($t){
            return $t['mCount'] > 0;
        });

        // return var_dump($tickets);

        if(count($tickets) > LemmminHelper::$max_ticket_per_order){
            return Response::json([
                'error' => 'too_many_tickets',
                'message' => sprintf("Total ticket quantity must be under %d.", LemmminHelper::$max_ticket_per_order)], 400);
        }
        elseif(count($tickets) < 1){
            return Response::json([
                'error' => 'no_ticket',
                'message' => 'You did not select any ticket to order.'], 400);
        }

        $order = Order::create([
            'user_id' => Auth::guard('api')->id(),
            'status' => 'pending',
        ]);


        $rows = [];
        $total = 0;
        foreach($tickets as $cart_item){
            $ticket = Ticket::find($cart_item['mTicketType']['id']);
            if(is_null($ticket)) continue;

            if($ticket->remaining < $cart_item['mCount'] || $ticket->max_purchase < $cart_item['mCount']){
                return Response::json([
                    'error' => 'not_available',
                    'message' => 'Looks like someone else already purchased tickets. Try decreasing some.'], 400);
            }

            $rows[] = [
                'order_id' => $order->id,
                'ticket_id' => $ticket->id,
                'ticket_name' => $ticket->name,
                'event_id' => $ticket->events[0]->id,
                'quantity' => $cart_item['mCount'],
                'price' => $ticket->price,
            ];

            $ticket->issued += $cart_item['mCount'];
            $ticket->save();

            $total += $cart_item['mCount']*$ticket->price;
        }


        OrderQuantity::insert($rows);

        $order->update_meta('total', $total);

        if($total == 0 && $order->generatePasses()){
            $order->status = 'complete';
            $order->save();
        }
        else {
            $invalidateJob = new InvalidateOrder($order);
            $invalidateJob->delay(Carbon::now()->addMinutes(LemmminHelper::$order_reserve_duration));
            dispatch($invalidateJob);
        }

        $order->total = $total;
        $order->quantities;
        return $order;
    }

    public function postConfirmBkashPayment($id)
    {
        $order = Order::find($id);
        $this->authorizeForUser(Auth::guard('api')->user(), 'show', $order);

        $transaction = Transaction::where([
            ['status', '=' ,'detached'],
            ['gateway', '=' ,'bkash'],
            ['referance', '=' ,trim(Input::get('bkash_transaction_id'))],
            ['amount', '>=', round($order->meta('total'))],
        ])->first();

        if(is_null($transaction)){
            return Response::json([
                'error' => 'invalid_referance',
                'message' => 'The transaction id is already used or not valid.'], 400);
        }

        if($order->generatePasses()){
            $transaction->status = 'complete';
            $transaction->order()->associate($order);
            $order->status = 'complete';
            $order->save();
            $transaction->save();
        }

        return $order;
    }
}
