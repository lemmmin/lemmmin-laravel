<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Hshn\Base64EncodedFile\HttpFoundation\File\Base64EncodedFile;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Event;
use App\Ticket;
use App\Pass;
use App\Organizer;
use App\ScanLog;
use Auth;
use Input;
use Session;
use \DateTime;
use MediaUploader;
use Validator;
use Response;

class EventController extends Controller
{

    /**
     * Validator for event requests.
     */
    public function validator($request){
        $messages = [
            'title.required' => 'You must specify an event title.',
            'title.min:5' => 'Event title must be at least 5 characters long.',
            'tickets.required' => 'You must set at least one ticket type.',
            'tickets.*.name.required_unless' => 'Ticket name is a required field.',
            'tickets.*.price.required_unless' => 'Ticket price is a required field.',
            'tickets.*.price.numeric' => 'Invalid ticket price given.',
            'tickets.*.quantity.required_unless' => 'Ticket quantity is a required field.',
            'tickets.*.quantity.integer' => 'Ticket quantity given is not valid.',
        ];

        return Validator::make($request->json()->all(), [
            'title' => 'required|min:5',
            'description' => 'required|min:10',
            'privacy' => 'required|in:public,private',
            'end_time' => 'after:start_time',
            'organizer_name' => 'required_if:organizer,new|min:5',
            'tickets' => 'required',
            'tickets.*.name' => 'required_unless:tickets.*.deleted,1',
            'tickets.*.price' => 'required_unless:tickets.*.deleted,1|numeric',
            'tickets.*.quantity' => 'required_unless:tickets.*.deleted,1|integer',
        ], $messages);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = Input::get('type', 'latest');

        switch ($type) {
            case "own":
                $events = Event::where([
                    ['owner_id', Auth::guard('api')->id()],
                    ['parent', null],
                    ])->with('owner', 'organizer', 'children', 'tickets')
                    ->latest();
                break;
            case "saved":
                $events = Auth::guard('api')->user()->saved_events()->where([
                    ['privacy', 'public'],
                    ['live', 1]
                    ])->with('owner', 'organizer', 'children', 'tickets');
                break;
            default:
                $events = Event::where([
                    ['end_time', '>', date("Y-m-d H:i:s")],
                    ['privacy', 'public'],
                    ['live', 1],
                    ['parent', null],
                    ])->with('owner', 'organizer', 'children', 'tickets')
                    ->latest();
                break;
        }

        $count = $events->count();
        return response()->json([
            'events' => $events->take(Input::get("take", 10))->skip(Input::get("skip", 0))->get()->toArray(),
            'count' => $count
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorizeForUser(Auth::guard('api')->user(), 'create', Event::class);

        $event = Event::create(Input::json()->all());


        $event->owner_id = Auth::guard('api')->id();
        $event->start_time = new DateTime(Input::json()->get('start_time'));
        $event->end_time = new DateTime(Input::json()->get('end_time'));

        $event->slug = $event->title;
        $event->slug = str_slug($event->slug);

        foreach(Input::json()->get('tickets') as $ticket){
            $ticket['privacy'] = 'public';
            $ticket['start_time'] = '0000-00-00 00:00:00';
            $ticket['end_time'] = '9999-12-31 23:59:59';
            $event->tickets()->save(new Ticket($ticket));
        }

        $event->update_meta('online_event', Input::json()->get('online_event'));
        $event->update_meta('location_name', Input::json()->get('location_name'));
        $event->update_meta('location_address', Input::json()->get('location_address'));
        $event->update_meta('location_latitude', Input::json()->get('location_latitude'));
        $event->update_meta('location_longitude', Input::json()->get('location_longitude'));


        if(Input::json()->get('has_new_file')){
            $file = new Base64EncodedFile(Input::json()->get('new_cover_image'));
            $filename = storage_path('uploadtemp').'/'.time().'.jpg';
            $file = $file->move(storage_path('uploadtemp'), time().'.jpg');
            $media = MediaUploader::fromSource($file)
                ->useHashForFilename()
                ->onDuplicateReplace()
                ->toDestination('uploads', 'event/cover_images')
                ->upload();

            $event->attachMedia($media, ['cover_image']);

            unlink($filename);
        }

        $validator = $this->validator($request);

        Event::withoutSyncingToSearch(function () use ($event, $validator){
            if(Input::json()->get('live') && $validator->fails()){
                $event->live = false;
                $event->save();
                return Response::json([
                    'error' => 'validation_failed',
                    'message' => implode("\n\r", $validator->errors()->all())], 400);
            }
            elseif(!Input::json()->get('live')){
                $event->live = false;
            }
            else {
                $event->live = true;
            }
            $event->save();
        });


        if($event->privacy == 'public'){
            $event->searchable();
        }
        else {
            $event->unsearchable();
        }

        return response()->json(self::prepareEvent($event));
    }

    private static function prepareEvent($event){
        if(null != $event->start_time && !is_string($event->start_time))
            $event->start_time = $event->start_time->format('Y-m-d H:i:s');
        if(null != $event->end_time && !is_string($event->end_time))
            $event->end_time = $event->end_time->format('Y-m-d H:i:s');

        $event->location_name = $event->meta('location_name');
        $event->location_latitude = $event->meta('location_latitude');
        $event->location_longitude = $event->meta('location_longitude');
        $event->online_event = $event->meta('online_event') == 1;

        // Just load the following properties no need to change.
        $event->tickets;
        $event->owner;

        return $event;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        $this->authorizeForUser(Auth::guard('api')->user(), 'view', $event);
        if( $event == null ) return Response::json([
            'error' => 'not_found',
            'message' => 'Event not found'], 404);



        return response()->json($this->prepareEvent($event));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $event = Event::find($id);

        $this->authorizeForUser(Auth::guard('api')->user(), 'update', $event);

        $event->title = Input::json()->get('title');
        $event->description = Input::json()->get('description');
        $event->privacy = Input::json()->get('privacy');

        if(Input::json()->has('start_time')) $event->start_time = new DateTime(Input::json()->get('start_time'));
        if(Input::json()->has('end_time')) $event->end_time = new DateTime(Input::json()->get('end_time'));

        $event->slug = $event->title;
        $event->slug = str_slug($event->slug);

        foreach(Input::json()->get('tickets') as $ticket){
            if($ticket['deleted'] && $ticket['id'] != 0){
                $ticketModel = Ticket::find($ticket['id']);
                $this->authorizeForUser(Auth::guard('api')->user(), 'delete', $ticketModel);
                $ticketModel->delete();
            }
            else {
                if($ticket['id'] == 0){
                    $ticket['privacy'] = 'public';
                    $ticket['start_time'] = '0000-00-00 00:00:00';
                    $ticket['end_time'] = '9999-12-31 23:59:59';
                    $event->tickets()->save(new Ticket($ticket));
                }
                else {
                    $ticket_e = Ticket::find($ticket['id']);
                    $this->authorizeForUser(Auth::guard('api')->user(), 'update', $ticket_e);
                    $ticket_e->name = $ticket['name'];
                    $ticket_e->description = $ticket['description'];
                    $ticket_e->price = $ticket['price'];
                    $ticket_e->quantity = $ticket['quantity'];
                    $ticket_e->save();
                }
            }
        }

        $event->update_meta('online_event', Input::json()->get('online_event'));
        $event->update_meta('location_name', Input::json()->get('location_name'));
        $event->update_meta('location_address', Input::json()->get('location_address'));
        $event->update_meta('location_latitude', Input::json()->get('location_latitude'));
        $event->update_meta('location_longitude', Input::json()->get('location_longitude'));


        if(Input::json()->get('has_new_file')){
            // Delete any existing cover imag of the same event.
            if($event->hasMedia('cover_image')){
                $event->firstMedia('cover_image')->delete();
            }


            $file = new Base64EncodedFile(Input::json()->get('new_cover_image'));
            $filename = storage_path('uploadtemp').'/'.time().'.jpg';
            $file = $file->move(storage_path('uploadtemp'), time().'.jpg');
            $media = MediaUploader::fromSource($file)
                ->useHashForFilename()
                ->onDuplicateReplace()
                ->toDestination('uploads', 'event/cover_images')
                ->upload();

            $event->attachMedia($media, ['cover_image']);

            unlink($filename);
        }

        $validator = $this->validator($request);

        Event::withoutSyncingToSearch(function () use ($event, $validator){
            if(Input::json()->get('live') && $validator->fails()){
                $event->live = false;
                $event->save();
                $event->unsearchable();
                return Response::json([
                    'error' => 'validation_failed',
                    'message' => implode("\n\r", $validator->errors()->all())], 400);
            }
            elseif(!Input::json()->get('live')){
                $event->live = false;
            }
            else {
                $event->live = true;
            }
            $event->save();
        });

        if($event->privacy == 'public'){
            $event->searchable();
        }
        else {
            $event->unsearchable();
        }
        return response()->json(self::prepareEvent($event));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);

        $this->authorizeForUser(Auth::guard('api')->user(), 'delete', $event);

        $event->deleteRelatives();
        $event->delete();

        return response()->json(['success' => true]);
    }

    public function bookmark($id)
    {
        $event = Event::find($id);
        $user = Auth::guard('api')->user();
        $this->authorizeForUser($user, 'view', $event);

        $user->saved_events()->toggle($event);

        return response()->json([
            'success' => true,
            'data' => $event->is_bookmarked ? 'saved' : 'unsaved',
        ]);
    }

    public function scan($id)
    {
        $event = Event::find($id);
        $user = Auth::guard('api')->user();
        $pass = Pass::findPass(Input::get('pass_no'));

        if(is_null($pass) || $pass->event_id != $id){
            return Response::json([
                'error' => 'Invalid pass',
                'message' => 'Pass is invalid'
            ]);
        }



        $this->authorizeForUser($user, 'see_guests', $event);

        $response = null;

        switch (Input::get('action')) {
            case 'Check In':
                $action = 'check_in';
                break;
            case 'Check Out':
                $action = 'check_out';
                break;
            default:
                $action = 'show_status';
        }

        if(
            ($action == "check_in" && $pass->status == "check_in")
            ||
            ($action == "check_out" && $pass->status == "check_out")
        ){
            $response = Response::json([
                'success' => false,
                'pass' => $pass,
            ]);
        }
        else {
            $response = Response::json([
                'success' => true,
                'pass' => $pass,
            ]);
        }

        ScanLog::create([
            'user_id' => $user->id,
            'pass_id' => $pass->id,
            'action' => $action
        ]);

        if($action != 'show_status'){
            $pass->status = $action;
            $pass->save();
        }

        return $response;
    }
}
