<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class BasicController extends Controller
{
    public function userinfo()
    {
        return response()->json(Auth::guard('api')->user());
    }
}
