<?php

namespace App\Http\Controllers\Api;

use App\Pass;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use Session;

class PassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $passes = Pass::where([
            ['user_id', Auth::guard('api')->id()],
            ])->latest();

        $count = $passes->count();
        return response()->json([
            'passes' => $passes
                ->take(Input::get("take", 10))
                ->skip(Input::get("skip", 0))
                ->get()
                ->toArray(),
            'count' => $count
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pass = Pass::find($id);
        if( $pass == null ) return Response::json([
            'error' => 'not_found',
            'message' => 'Event not found'], 404);


        $this->authorizeForUser(Auth::guard('api')->user(), 'view', $pass);

        return response()->json($pass);
    }
}
