<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pass;
use Auth;
use Input;

class PassController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $passes = Auth::user()->passes()->latest();
        if(Input::has('order'))
            $passes = $passes->where('order_id', Input::get('order'));

        $passes = $passes->paginate(30);

        return view('dashboard.passes.index', [
            'passes' => $passes,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pass = Pass::findPass($id);

        $this->authorize('show', $pass);

        return view('dashboard.passes.show', [
            'pass' => $pass,
            'event' => $pass->event,
        ]);
    }
}
