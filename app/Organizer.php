<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizer extends Model
{
    use Metabble;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'about'];

    /**
     * Get the owner of the event
     */
    public function owner()
    {
    	return $this->belongsTo('App\User', 'owner_id');
    }

    /**
     * Get events owned by the organizer
     */
    public function events()
    {
        return $this->hasMany('App\Event', 'organizer_id');
    }
}
