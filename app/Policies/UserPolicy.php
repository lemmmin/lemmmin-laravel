<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if users can be created by the user.
     *
     * @param  \App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine if users can list all users.
     *
     * @param  \App\User  $user
     * @return bool
     */
    public function index(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine if users can change email address.
     *
     * @param  \App\User  $user
     * @param  \App\User  $profile
     * @return bool
     */
    public function change_email(User $user, User $profile)
    {
        return ($user->id == $profile->id && $profile->email == '') || $user->is_admin;
    }

    /**
     * Determine if the given user can be updated by the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $profile
     * @return bool
     */
    public function update(User $user, User $profile)
    {
        if($user->is_admin) return true;
        return $user->id === $profile->id;
    }

    /**
     * Determine if the given user can be deleted by the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $profile
     * @return bool
     */
    public function delete(User $user, User $profile)
    {
        if($user->is_admin) return true;
        return $user->id === $profile->id;
    }
}
