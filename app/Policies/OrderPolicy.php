<?php

namespace App\Policies;

use App\User;
use App\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the order can be viewed by current user.
     *
     * @param  \App\User  $user
     * @return bool
     */
    public function show(User $user, Order $order)
    {
        if($user->is_admin) return true;
        return $order->events()->where('owner_id', $user->id)->count() || $user->id == $order->user_id;
    }

    /**
     * Determine if the order can be updated by current user.
     *
     * @param  \App\User  $user
     * @return bool
     */
    public function update(User $user, Order $order)
    {
        if($user->is_admin) return true;
        return $order->events()->where('owner_id', $user->id)->count() || $user->id == $order->user_id;
    }


    /**
     * Determine if the order can be paid by current user.
     *
     * @param  \App\User  $user
     * @return bool
     */
    public function pay(User $user, Order $order)
    {
        if($user->is_admin) return true;
        return $order->events()->where('owner_id', $user->id)->count() || $user->id == $order->user_id;
    }

    /**
     * Determine if the order can be resurrected by current user.
     *
     * @param \App\User $user
     * @return bool
     */
    public function resurrect(User $user, Order $order)
    {
        if($user->is_admin) return true;
        return $order->event->owner_id == $user->id;
    }
}
