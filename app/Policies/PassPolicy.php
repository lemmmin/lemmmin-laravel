<?php

namespace App\Policies;

use App\User;
use App\Pass;
use Illuminate\Auth\Access\HandlesAuthorization;

class PassPolicy
{
    use HandlesAuthorization;

    public function show(User $user, Pass $pass)
    {
        if($user->is_admin) return true;
        return $user->id == $pass->user_id
            || $user->id == $pass->event->owner_id
            || $user->can('see_guests', $pass->event);
    }

    /**
     * Wheather the current user can input data into the form fields of the pass.
     */
    public function describe(User $user, Pass $pass)
    {
        if($user->is_admin) return true;
        return $user->id == $pass->user_id
            || $user->id == $pass->event->owner_id
            || $user->can('see_guests', $pass->event);
    }
}
