<?php

namespace App\Policies;

use App\Organizer;
use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class OrganizerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if events can be created by the user.
     *
     * @param  \App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        if($user->is_admin) return true;
        return $user->organizers()->count() < 20;
    }

    /**
     * Determine if the given organizer can be used by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Organizer  $organizer
     * @return bool
     */
    public function attach(User $user, Organizer $organizer)
    {
        if($user->is_admin) return true;
        return $user->id === $organizer->owner_id;
    }

    /**
     * Determine if the given organizer can be updated by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Organizer  $organizer
     * @return bool
     */
    public function update(User $user, Organizer $organizer)
    {
        if($user->is_admin) return true;
        return $user->id === $organizer->owner_id;
    }

    /**
     * Determine if the given organizer can be deleted by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Organizer  $organizer
     * @return bool
     */
    public function delete(User $user, Organizer $organizer)
    {
        if($user->is_admin) return true;
        return $user->id === $organizer->owner_id;
    }
}
