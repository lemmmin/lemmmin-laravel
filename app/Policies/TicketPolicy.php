<?php

namespace App\Policies;

use App\Ticket;
use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class TicketPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given event can be viewed.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return bool
     */
    public function view(User $user, Ticket $ticket)
    {
        if($user->is_admin) return true;
        return true;
    }

    /**
     * Determine if events can be created by the user.
     *
     * @param  \App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        if($user->is_admin) return true;
        return true;
    }

    /**
     * Determine if the given event can be updated by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return bool
     */
    public function update(User $user, Ticket $ticket)
    {
        if($user->is_admin) return true;
        return !!$ticket->events()->where('owner_id', $user->id)->count();
    }

    /**
     * Determine if the given event can be deleted by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return bool
     */
    public function delete(User $user, Ticket $ticket)
    {
        if($user->is_admin) return true;
        return !!$ticket->events()->where('owner_id', $user->id)->count();
    }
}
