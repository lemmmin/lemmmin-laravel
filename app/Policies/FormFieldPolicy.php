<?php

namespace App\Policies;

use App\FormField;
use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class FormFieldPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given event can be viewed.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return bool
     */
    public function view(User $user, FormField $field)
    {
        if($user->is_admin) return true;
        return true;
    }

    /**
     * Determine if events can be created by the user.
     *
     * @param  \App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        if($user->is_admin) return true;
        return true;
    }

    /**
     * Determine if the given event can be updated by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return bool
     */
    public function update(User $user, FormField $field)
    {
        if($user->is_admin) return true;
        return $field->event->owner_id == $user->id;
    }

    /**
     * Determine if the given event can be deleted by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return bool
     */
    public function delete(User $user, FormField $field)
    {
        if($user->is_admin) return true;
        return $field->event->owner_id == $user->id;
    }
}
