<?php

namespace App\Policies;

use App\Event;
use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given event can be viewed.
     *
     * @param  \App\User  $user
     * @param  \App\Event  $event
     * @return bool
     */
    public function view(User $user, Event $event)
    {
        if($user->is_admin) return true;
        return $event->live
            || $user->id === $event->owner_id
            || $user->permittedEvents()
                    ->where('id', $event->id)
                    ->wherePivot('granted', 1)
                    ->wherePivot('permission', 'view')
                    ->exists();
    }

    /**
     * Determine if the given event's guest list can be viewed.
     *
     * @param  \App\User  $user
     * @param  \App\Event  $event
     * @return bool
     */
    public function see_guests(User $user, Event $event)
    {
        if($user->is_admin) return true;
        return $user->id === $event->owner_id
            || $user->permittedEvents()
                    ->where('id', $event->id)
                    ->wherePivot('granted', 1)
                    ->wherePivot('permission', 'see_guests')
                    ->exists();
    }

    /**
     * Determine if events can be created by the user.
     *
     * @param  \App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        if($user->is_admin) return true;
        return true;
    }

    /**
     * Determine if the given event can be updated by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Event  $event
     * @return bool
     */
    public function update(User $user, Event $event)
    {
        if($user->is_admin) return true;
        return $user->id === $event->owner_id
            || $user->permittedEvents()
                    ->where('id', $event->id)
                    ->wherePivot('granted', 1)
                    ->wherePivot('permission', 'update')
                    ->exists();
    }

    /**
     * Determine if the given event can be deleted by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Event  $event
     * @return bool
     */
    public function delete(User $user, Event $event)
    {
        if($user->is_admin) return true;
        return $user->id === $event->owner_id
            || $user->permittedEvents()
                    ->where('id', $event->id)
                    ->wherePivot('granted', 1)
                    ->wherePivot('permission', 'delete')
                    ->exists();
    }
}
