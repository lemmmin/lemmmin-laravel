<?php

use App\User;
use App\Event;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventPolicyTest extends TestCase
{
    use DatabaseTransactions;

    public function testCanEdit()
    {
        $user = factory(User::class)->create();
        $event = factory(Event::class)->create([
            'owner_id' => $user->id,
        ]);

        $this->assertTrue($user->can('update', $event));
    }

    public function testCantEdit()
    {
        $user = factory(User::class)->create();
        $event = factory(Event::class)->create();

        $this->assertFalse($user->can('update', $event));
    }

    public function testCanDelete()
    {
        $user = factory(User::class)->create();
        $event = factory(Event::class)->create([
            'owner_id' => $user->id,
        ]);

        $this->assertTrue($user->can('delete', $event));
    }

    public function testCantDelete()
    {
        $user = factory(User::class)->create();
        $event = factory(Event::class)->create();

        $this->assertFalse($user->can('delete', $event));
    }
}
