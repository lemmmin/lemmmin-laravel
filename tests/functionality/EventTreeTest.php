<?php

use App\Event;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventTreeTest extends TestCase
{
    use DatabaseTransactions;

    protected $event, $childEvent;

    protected function setUp()
    {
        parent::setUp();
        $this->event = factory(Event::class)->create();
        $this->childEvent = factory(Event::class)->create();
        $this->event->children()->save($this->childEvent);

    }

    public function testCanAddChildEvent()
    {

        $this->seeInDatabase('events', [
            'id' => $this->childEvent->id,
            'parent' => $this->event->id,
        ]);
    }

    public function testCanGetChildEvents()
    {
        $this->assertEquals(
            $this->event->children()->first()->id,
            $this->childEvent->id );
    }

    public function testCanGetParentEvent()
    {
        $this->assertEquals(
            $this->childEvent->parent()->first()->id,
            $this->event->id );
    }
}
