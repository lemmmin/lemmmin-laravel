<?php

use App\User;
use App\Event;
use App\Ticket;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TicketPolicyTest extends TestCase
{
    use DatabaseTransactions;

    public function testCanEdit()
    {
        $user = factory(User::class)->create();
        $event = factory(Event::class)->create([
            'owner_id' => $user->id,
        ]);
        $ticket = factory(Ticket::class)->create();
        $ticket->events()->attach($event->id);

        $this->assertTrue($user->can('update', $ticket));
    }

    public function testCantEdit()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $event = factory(Event::class)->create();
        $ticket = factory(Ticket::class)->create();
        $ticket->events()->attach($event->id);

        $this->assertFalse($user->can('update', $ticket));
    }

    public function testCanDelete()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $event = factory(Event::class)->create([
            'owner_id' => $user->id,
        ]);
        $ticket = factory(Ticket::class)->create();
        $ticket->events()->attach($event->id);

        $this->assertTrue($user->can('delete', $ticket));
    }

    public function testCantDelete()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $event = factory(Event::class)->create();
        $ticket = factory(Ticket::class)->create();
        $ticket->events()->attach($event->id);

        $this->assertFalse($user->can('delete', $ticket));
    }
}
