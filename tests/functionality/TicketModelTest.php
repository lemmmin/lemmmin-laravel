<?php

use App\Ticket;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TicketModelTest extends TestCase
{
    use DatabaseTransactions;

    public function testCanAddTicket(){
        $ticket = factory(Ticket::class)->create();

        $this->seeInDatabase('tickets', [
            'id' => $ticket->id,
            'name' => $ticket->name,
        ]);
    }
}
