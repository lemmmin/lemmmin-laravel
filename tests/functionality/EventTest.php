<?php

use App\User;
use App\Event;
use App\Organizer;
use App\Ticket;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventTest extends TestCase
{
    use DatabaseTransactions;

    protected $user, $event;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->event = factory(Event::class)->create([
            'owner_id' => $this->user->id,
        ]);
    }

    public function testDelete()
    {
        $this->actingAs($this->user)
             ->visit(route('events.index'))
             ->press('Delete')
             ->see('Event Deleted!');
    }

    public function testDeleteRemovesRelations()
    {
        $ticket = factory(Ticket::class)->create();
        $ticket->events()->attach($this->event->id);
        $subevent = factory(Event::class)->create([
            'owner_id' => $this->user->id,
        ]);
        $this->event->children()->save($subevent);

        // $this->actingAs($this->user)
        //      ->visit(route('events.show', $this->event->id))
        //      ->see('Delete')
        //      ->press('Delete')
        //      ->see('Event Deleted!')
        //      ->notSeeInDatabase('events', ['id' => $this->event->id])
        //      ->notSeeInDatabase('tickets', ['id' => $ticket->id])
        //      ->notSeeInDatabase('event_ticket', ['event_id' => $this->event->id])
        //      ->notSeeInDatabase('events', ['id' => $subevent->id]);
    }


    public function testCreate()
    {
        $start_time = new DateTime("now");
        $end_time = new DateTime("tomorrow");

        $this->actingAs($this->user)
              ->visit(route('events.create'))
              ->type('Test Event 2', 'title')
              ->type('Bla bla bla...', 'description')

              ->press('Save Event');

        $this->actingAs($this->user)
             ->visit(route('events.create'))
             ->type('Test Event', 'title')
             ->type('Bla bla bla...', 'description')
             ->type('New Organizer', 'organizer_name')
             ->type('DeviserWeb', 'location[name]')
             ->type('24/A, Jalalabad, Sylhet, Bangladesh', 'location[address]')

             ->type('24.9091', 'location[latitude]')
             ->type('91.8640', 'location[longitude]')

             ->press('Save Event')

             ->see('Test Event')
             ->see('DeviserWeb')
             ->see('24/A')
             ->see('Jalalabad')
             ->see('Sylhet')
             ->see('24.9091')
             ->see('91.8640');
    }

    public function testEdit()
    {
        $time = time();
        $start_time = new DateTime("now");
        $end_time = new DateTime("tomorrow");

        $organizer = factory(Organizer::class)->create([
            'owner_id' => $this->user->id,
        ]);

        $this->actingAs($this->user)
             ->visit(route('events.edit', $this->event))
             ->type('New Title', 'title')
             ->type(date('d-m-Y H:i:s', $time), 'start_time')
             ->type(date('d-m-Y H:i:s', $time+100), 'end_time')
             ->type('Bla bla bla', 'summary')
             ->type('Bla bla bla', 'description')
             ->select('private', 'privacy')
             ->select($organizer->id, 'organizer')
             ->type('DeviserWeb', 'location[name]')
             ->type('24/A, Jalalabad, Sylhet City, Bangladesh', 'location[address]')
             ->type('24.9091', 'location[latitude]')
             ->type('91.8640', 'location[longitude]')
             ->press('Save Event')
             ->see('Saved')
             ->see('New Title')
             ->see(date('Y-m-d H:i:s', $time))
             ->see(date('Y-m-d H:i:s', $time+100))
             ->see('value="private" selected')
             ->see('value="'.($organizer->id).'" selected')
             ->see('DeviserWeb')
             ->see('24/A')
             ->see('Jalalabad')
             ->see('Sylhet City')
             ->see('24.9091')
             ->see('91.8640')
             ->type(date('Y-m-d H:i:s', $time), 'end_time')
             ->press('Save Event')
             ->see('The end time must be a date after start time.')
             ->type(date('Y-m-d H:i:s', $time+100), 'end_time')
             ->press('Save Event');
    }
}
