<?php

use App\User;
use App\Organizer;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrganizerPolicyTest extends TestCase
{
    use DatabaseTransactions;

    protected $user, $usersOrganizer, $anotherOrganizer;

    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->usersOrganizer = factory(Organizer::class)->create([
            'owner_id' => $this->user->id,
        ]);
        $this->anotherOrganizer = factory(Organizer::class)->create();
    }

    public function testCanEdit()
    {
        $this->assertTrue($this->user->can('update', $this->usersOrganizer));
    }

    public function testCantEdit()
    {
        $this->assertFalse($this->user->can('update', $this->anotherOrganizer));
    }

    public function testCanDelete()
    {
        $this->assertTrue($this->user->can('delete', $this->usersOrganizer));
    }

    public function testCantDelete()
    {
        $this->assertFalse($this->user->can('delete', $this->anotherOrganizer));
    }

    public function testCanCreate()
    {
        $this->assertTrue($this->user->can('create', Organizer::class));
        factory(Organizer::class, 19)->create([
            'owner_id' => $this->user->id,
        ]);
        $this->assertFalse($this->user->can('create', Organizer::class));
    }
}
