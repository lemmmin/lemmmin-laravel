<?php

use App\Event;
use App\Ticket;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventTicketRelationsTest extends TestCase
{
    use DatabaseTransactions;

    protected $event, $ticket;

    protected function setUp()
    {
        parent::setUp();

        $this->event = factory(Event::class)->create();
        $this->ticket = factory(Ticket::class)->create();
    }

    public function testCanAttachTicket()
    {
        $this->event->tickets()->attach($this->ticket->id);
        $this->assertEquals(
            $this->event->tickets()->first()->id,
            $this->ticket->id );
        $this->assertEquals(
            $this->ticket->events()->first()->id,
            $this->event->id );
    }

    public function testCanDetachTicket()
    {
        $this->event->tickets()->detach($this->ticket->id);
        $this->assertNull( $this->event->tickets()->first() );
        $this->assertNull( $this->ticket->events()->first() );
    }

    public function testCanAttachEvent()
    {
        $this->ticket->events()->attach($this->event->id);
        $this->assertEquals(
            $this->event->tickets()->first()->id,
            $this->ticket->id );
        $this->assertEquals(
            $this->ticket->events()->first()->id,
            $this->event->id );
    }

    public function testCanDetachEvent()
    {
        $this->ticket->events()->detach($this->event->id);
        $this->assertNull( $this->event->tickets()->first() );
        $this->assertNull( $this->ticket->events()->first() );
    }
}
